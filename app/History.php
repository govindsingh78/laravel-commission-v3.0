<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Contracts\Auth\CanResetPassword;

class History extends Authenticatable
{
    use Notifiable,Sortable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'login_history';
    //protected $fillable = ['title','description','image','status'];  

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

}
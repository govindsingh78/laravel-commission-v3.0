<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Emailtemplate extends Authenticatable
{
    use Notifiable,Sortable;

    public $timestamps = true;
    protected $table = 'email_templates';
    protected $guarded = ['id'];
    
}



<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable{

    use Notifiable,Sortable;

    protected $table = 'users';

    protected $fillable = ['first_name','last_name','email','phone','role_id','otp','address','status','password','profile_image','company_name','gender','dob','company_url','facebook_token','twitter_token'];  
    
    public function role(){
        return $this->belongsTo('\App\Role','role_id');
    }

    public function refer(){
        return $this->belongsTo('\App\User','refer_id');
    }
}
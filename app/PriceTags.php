<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class PriceTags extends Model{
   
	use Notifiable,Sortable;

    protected $table = 'price_tags';

    protected $fillable = ['name','price_id'];  
}

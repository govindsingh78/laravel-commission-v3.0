<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Price extends Model{
   
	use Notifiable,Sortable;

    protected $table = 'price';

    protected $fillable = ['name','type', 'per_month', 'per_year'];  

    public function tags(){
		return $this->hasMany('App\PriceTags', 'price_id');
    }
}

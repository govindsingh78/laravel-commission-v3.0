<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class ProductsPhoto extends Model
{
    use Notifiable,Sortable;

    protected $table = 'products_photo';

    protected $fillable = ['product_id', 'product_img']; 
}

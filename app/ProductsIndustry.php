<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class ProductsIndustry extends Model
{
    use Notifiable,Sortable;

    protected $table = 'products_industry';

    protected $fillable = ['product_id', 'industry_id'];
}

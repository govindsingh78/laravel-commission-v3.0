<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Industry extends Model{
   
	use Notifiable,Sortable;

    protected $table = 'industry';

    protected $fillable = ['name','status'];  
}

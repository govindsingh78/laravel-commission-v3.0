<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model{
   
	use Notifiable,Sortable;

    protected $table = 'products';

    protected $fillable = ['name', 'price', 'title', 'description']; 

    public function photos(){
		return $this->hasMany('App\ProductsPhoto', 'product_id');
    }

    public function industries(){
		return $this->belongsToMany('App\Industry', 'products_industry', 'product_id', 'industry_id')->withPivot('product_id', 'industry_id');
    }

}

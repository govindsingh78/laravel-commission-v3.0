<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Session;
use Visitor;

class IsNotAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {  
        // Visitor::recordActivity();
        if (Auth::guard('web')->check()) {
            return redirect()->route('admin.login');
        }
        return $next($request);
    }
}

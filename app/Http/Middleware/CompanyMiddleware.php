<?php
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Session;
use Visitor;


class CompanyMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
   public function handle($request, Closure $next)
    {
     /* dd(Session::has('url.intended'));
       echo $uri = $request->path();
       
       die;*/

    //Visitor::recordActivity();
          $current_path =   $request->path();

         if($current_path=='company/logout'){
            Session::put('url.intended', 'company/dashboard');

         }else{
             Session::put('url.intended', $request->path());
         }
    
    
     
        if (!Auth::guard('company')->check()) {
            return redirect()->route('company.login')->with('alert-error', trans('admin.NOT_LOGIN'));
        }elseif (Auth::guard('company')->user()->role_id != 2){
           
            return redirect()->route('company.login')->with('alert-error', trans('admin.NOT_AUTHORITY'));
        }
        return $next($request);
    }

}

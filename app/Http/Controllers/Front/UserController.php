<?php

//namespace App\Http\Controllers\Front;

//use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
//use App\Http\Requests;
//use App\User;
//use Validator;
//use Artisan;
//use App\Helpers\BasicFunction;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Auth\Middleware\Authenticate;



namespace App\Http\Controllers\Front;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use View;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class UserController extends Controller {

    //protected $guard = 'admin';
   // protected $redirectAfterLogout = '/admin';
   // protected $loginPath = '/login';
    //protected $redirectTo = 'user/dashboard';

//use RegistersUsers, AuthenticatesUsers, ThrottlesLogins;
use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }


    public function getLogin() {
        return view('front.home.index')->with('popup', 'login');;
    }

    public function postLogin(Request $request){
//return $request;
        $validator = validator::make($request->all(),[
            'email' => 'required|email', 'password' => 'required',
        ]);
            
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());             
        }
    
        $credentials = $this->credentials($request);
        $credentials['role_id'] =   2;
        
        if (Auth::guard('admin')->attempt($credentials, $request->has('remember'))) {
            $admin = adminUser();
            return redirect()->intended($this->redirectPath())->with('alert-sucess', trans('admin.LOGIN_SUCCESSFULLY', ['site_tite'=>Configure('CONFIG_SITE_TITLE'),'Name' => $admin->first_name]));
        }
    
        return redirect()->back()->with("error","Invalid Login Credentials.");    
        
    }


    public function signup() {
        return view('front.user.signup');
    }
}

<?php

namespace App\Http\Controllers\Front;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Cms;
use App\User;
use App\Blog;
use \App\Friends;
use DB;
use \App\Voucher;
use \App\UserPreferences as Preference;
use \App\Helpers\BasicFunction;
use Password;
use Socialite;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
class UserLoginController extends Controller
{
    //Constructer
    public function __contruct()
    {
        $this->middleware('auth');
    }
    public function getLatestProducts()
    {
        return Voucher::get();
    }
    //Index
    public function index()
    {
        echo 'hello';die;
    }
    //function to get loggedIn User's Data
    public function getUserData()
    {
        if(Auth::guard('company')->check())
        {
            $user=Auth::guard('company')->user();
        }
        elseif(Auth::guard('user')->check())
        {
            $user=Auth::guard('user')->user();
        }
        elseif(Auth::guard('web')->check())
        {
            $user=Auth::guard('web')->user();
        }
        return $user;
    }
    //Function to register user/company
    public function postRegistration(Request $request)
    {
        $user=$request->all();
        if($user['role_id']=='11')
        {   
            $validator=Validator::make($user,['email'=>'required|email|unique:users','password'=>'required|min:6|confirmed','password_confirmation'=>'required','first_name'=>'required','phone'=>'required|unique:users']);
            
            if($validator->fails())
            {
                return redirect('/signup')
                ->withErrors($validator)
                ->withInput();
            }   
            else
            {
                unset($user['password_confirmation']);
                $name=explode(' ',$user['first_name']);
                $user['first_name']=$name[0];
                unset($name[0]);
                $user['last_name']=implode(" ",$name);
                $user['password']=bcrypt($user['password']);
                $user['status']=1;
                $user['address']='Test Address';
                User::create($user);
                return redirect('/signup')->with("SuccessSignUp",'Registration Successfull, Login To Your Account');
            }
        }
        else if($user['role_id']=='10')
        {
            $validator=Validator::make($user,['company_name'=>'required|unique:users','email'=>'required|email|unique:users','password'=>'required|min:6','first_name'=>'required','phone'=>'required','company_url'=>'required|unique:users']);
            if($validator->fails())
            {
                return redirect('/signup')
                ->withErrors($validator)
                ->withInput();
            }   
            else
            {
                $name=explode(' ',$user['first_name']);
                $user['first_name']=$name[0];
                unset($name[0]);
                $user['last_name']=implode(" ",$name);
                $user['password']=bcrypt($user['password']);
                $user['status']=1;
                $user['address']='Test Address';
                User::create($user);
                return redirect('/signup')->with("SuccessSignUp",'Registration Successfull, Login To Your Account');
            }
        }
        return redirect('/');
        
    }

    //function to display signup form only if the user is not logged in
    public function getSignUp()
    {
        if(Auth::guard('user')->check())
        {
            $user=Auth::guard('user')->user();
            $cms=new Cms;
            $cms_pages=$cms->get();
            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
        }
        elseif(Auth::guard('user')->check())
        {
            $user=Auth::guard('user')->user();
            $cms=new Cms;
            $cms_pages=$cms->get();
            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
        }
        elseif(Auth::guard('admin')->check())
        {
            $user=Auth::guard('admin')->user();
            $cms=new Cms;
            $cms_pages=$cms->get();
            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
        }
        else
        {
            $cms=new Cms;
            $cms_pages=$cms->get();
            $user['first_name']=null;
            return view('front.landing',['cms_pages'=>$cms_pages,'user'=>$user]);
        }
    }
    //Display Login Form
    public function getLogin()
    {
        if(Auth::guard('admin')->check())
        {
            $user=Auth::guard('admin')->user();
            $cms=new Cms;
            $cms_pages=$cms->get();
            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
        }
        else
        {
            $cms=new Cms;
            $cms_pages=$cms->get();
            $user['first_name']=null;
            return view('front.landing',['cms_pages'=>$cms_pages,'user'=>$user]);
        }
    }
    //Login User
    public function postLogin(Request $request)
    {
        $validator=Validator::make($request->all(),['email'=>'required|email','password'=>'required']);
        if($validator->fails())
        {
            return redirect('/login')
                    ->withErrors($validator)
                    ->withInput();
        }
        $credentials = $request->only('email','password');
        $role_id=User::where('email','=',$credentials['email'])->get();
        $role_id=$role_id->all();
        if(!empty($role_id))
        {
            $role_id= $role_id[0]['attributes']['role_id'];
            $credentials['role_id']=intval($role_id);
            if($role_id==10)
            {                                                                                   
                if (Auth::guard('company')->attempt($credentials, true
                  )) {
                
                    return redirect('/');

                }
                else{
                    return redirect('/')->with('login-alert-error','Invalid Login! Password Do Not Match.');
                }
            }
            elseif($role_id==11)
            {
                if (Auth::guard('user')->attempt($credentials, true))
                {
                
                    return redirect('/');

                }
                else{
                    return redirect('/')->with('login-alert-error','Invalid Login! Password Do Not Match.');
                }
            }
            elseif($role_id==1)
            {
                return redirect('/')->with('admin-alert-error','Admin Not Allowed To Login As User');
            }
        }
        else
        {
            return redirect('/')->with('login-alert-error','Email Not Registered With Vouch. Please Register.');
        }
    }
    //Socail login redirect
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    //User data from facebook
    public function handleFacebookProviderCallback()
    {
        $user = Socialite::driver('facebook')->stateless()->user();
        $socialUser=User::where('email','=',$user->email)->first();
        if($socialUser)
        {
            Auth::loginUsingId($socialUser->id);
            return redirect('/');
       }
       else
       {
            $authLogin=$this->createOrUpdateUser($user,'facebook');
            Auth::loginUsingId($authLogin->id);
            return redirect('/');
       }
    }
    //Function to send reset password link
    public function resetPassword()
    {
        Password::sendResetLink(['email'=>'friendly.anubhav@gmail.com']);
    }
    //User data from facebook
    public function handleGoogleProviderCallback()
    {
        $user = Socialite::driver('google')->user();
        $socialUser=User::where('email','=',$user->email)->first();
        if($socialUser)
        {
            Auth::loginUsingId($socialUser->id);
            return redirect('/');
       }
       else
       {
            $authLogin=$this->createOrUpdateUser($user,'twitter');
            Auth::loginUsingId($authLogin->id);
            return redirect('/');
       }
    }

    //Create new user with social media data
    public function createOrUpdateUser($socialData,$provider)
    { 
        if($provider=='facebook')
        {
            $user=array(
                'email'=>$socialData->email,
                'facebook_token'=>$socialData->token,
                'role_id'=>11,
                'password'=>bcrypt('123456'),
                'status'=>1,
                'profile_image'=>$socialData->avatar_original,
                'facebook_url'=>$socialData->profileUrl);
            $name=explode(' ',$socialData->name);
                    $user['first_name']=$name[0];
                    unset($name[0]);
                    $user['last_name']=implode(" ",$name); 
             return User::updateOrCreate(['email'=>$user['email']],$user);
        }
        if($provider=='twitter')
        {
            $user=array(
                'email'=>$socialData->email,
                'twitter_token'=>$socialData->token,
                'role_id'=>11,
                'password'=>bcrypt('123456'),
                'status'=>1,
                'profile_image'=>$socialData->avatar_original);
            $name=explode(' ',$socialData->name);
                    $user['first_name']=$name[0];
                    unset($name[0]);
                    $user['last_name']=implode(" ",$name); 
             return User::updateOrCreate(['email'=>$user['email']],$user);
        }
        //return $user;
    }
    //Load User Profile
    public function getUserProfile($id=null)
    {
        $vouchers=$this->getLatestProducts();
        if($id==null)
        {
            $user=$this->getUserData();
            $user['isfriend']['user_id']=$user['id'];
            $cms=new Cms;
            $cms_pages=$cms->get();
            $friends=Friends::where('user_id','=',$user->id)->where('status','=',1)->with('friends')->get();
            $friends_count=Friends::where('user_id','=',$user->id)->where('status','=',1)->get()->count();
            $posts=Blog::withCount([
                'likes','comments']
            )->with('user')->orderBy('id','desc')->where('category_id','=',6)->where('user_id','=',$user['id'])->get();
            return view('front.user.user_profile',['user_data'=>$user,'user'=>$user,'cms_pages'=>$cms_pages,'posts'=>$posts,'vouchers'=>$vouchers,'friends'=>$friends,'friends_count'=>$friends_count]);
        }
        else
        {
            $user=$this->getUserData();
            $user_data=User::where('id','=',$id)->with('friends')->with(['isfriend'])->get();
            $user_data=$user_data->toArray();
            $cms=new Cms;
            $isfriend=Friends::where('user_id','=',$id)->where('friend_id','=',$user->id)->first();
            $friends=Friends::where('user_id','=',$id)->where('status','=',1)->with('friends')->get();
            $friends_count=Friends::where('user_id','=',$id)->where('status','=',1)->get()->count();
            $cms_pages=$cms->get();
            $posts=Blog::withCount([
                'likes','comments']
            )->with('user')->orderBy('id','desc')->where('category_id','=',6)->where('user_id','=',$id)->get();
        }
        
        return view('front.user.user_profile',['user_data'=>$user_data[0],'user'=>$user,'cms_pages'=>$cms_pages,'posts'=>$posts,'vouchers'=>$vouchers,'friends'=>$friends,'friends_count'=>$friends_count,'isfriend'=>$isfriend]);
    }
    //Logout Function
    public function logout()
    {
        $user=Auth::guard('user')->user();
        $company=Auth::guard('company')->user();
        if($company['role_id']==10)
        {
             Auth::guard('company')->logout();
            return redirect()->route('register');
        }
        if ($user['role_id']==11) 
        {
            Auth::guard('user')->logout();
            return redirect()->route('register');
        }
        Auth::guard('web')->logout();
        return redirect()->route('register');
        
    }
    //function to get user preference/settings page
    public function getUserPreferrences()
    {

        $user=$this->getUserData();
        $cms=new Cms;
        $cms_pages=$cms->get();
        $preferences=Preference::where('user_id','=',$user->id)->get();
        if(empty($preferences[0]))
        {
            $preferences_data['can_find_user']='Public';
            $preferences_data['can_view_profile']='Public';
            $preferences_data['notification_sound']=1;
            $preferences_data['notification_email']=1;
            $preferences_data['birthday_notify']=1;
            $preferences_data['chat_sound']=1;
            $preferences_data['user_id']=$user->id;
            Preference::create($preferences_data);
            $preferences=Preference::where('user_id','=',$user->id)->get();
        }
        return view('front.user.user_preferrence',['user'=>$user,'cms_pages'=>$cms_pages,'preferences'=>$preferences]);
    }
    //function to update password
    public function changePassword(Request $request)
    {
        $pass=$request->all();
        $validator=Validator::make($request->all(),['current_password'=>'required','password'=>'required|confirmed','password_confirmation'=>'required']);   
        if($validator->fails())
        {
            return redirect('/user_prferrences')
                    ->withErrors($validator)
                    ->withInput();
        }
        $loggedInUser=Auth::guard('company')->user();
        $user=User::where('id','=',$loggedInUser->id)->get();
        $password=bcrypt($pass['password']);
        if(Hash::check($pass['current_password'],$user[0]['attributes']['password']))
        {
            $newPassword['password']=$password;
            User::where('id','=',$loggedInUser->id)->update($newPassword);
            return redirect('user_prferrences')->with('passwordMessage','Password Changed Successfull.');
        }
        else
        {
            return redirect('user_prferrences')->with('passwordMessageError','Invalid Current Password.');
        }
    }
    //function to update user profile Picture
    public function updateProfilePicture(Request $request)
    {
        $user=$this->getUserData();
        $userData=User::where('id','=',$user->id)->get();
        $postData=$request->all();
        $image=$request->profile_image;
        $uploadpath=USER_IMAGES;
        $image_prefix="user_";
        $old_image=$userData[0]['attributes']['profile_image'];
        $imageNme=BasicFunction::uploadImage($image,$uploadpath,$image_prefix,false,$old_image,false);
        $userdata['profile_image']=url('/').'/public/user_images/'.$imageNme;
        User::where('id','=',$user->id)->update($userdata);
        return redirect('/user_prferrences');
    }
    //function to update basic profile information of logged in user
    public function updateProfile(Request $request)
    {
        $user=$this->getUserData();
        $userData=$request->all();
        unset($userData['_token']);
        User::where('id','=',$user->id)->update($userData);
        return redirect('user_prferrences')->with('passwordMessage','Profile Updated Successfuly.');
    }
    //function to update the user preferences
    public function updateUserPreferences(Request $request)
    {
        $pref_data=$request->all();
        $user=$this->getUserData();
        $pref_data['user_id']=$user->id;
        if(array_key_exists('chat_sound', $pref_data))
        {
            $pref_data['chat_sound']=1;
        }
        else
        {
            $pref_data['chat_sound']=0;
        }
        if(array_key_exists('birthday_notify', $pref_data))
        {
            $pref_data['birthday_notify']=1;
        }
        else
        {
            $pref_data['birthday_notify']=0;
        }
        if(array_key_exists('notification_email', $pref_data))
        {
            $pref_data['notification_email']=1;
        }
        else
        {
            $pref_data['notification_email']=0;
        }
        if(array_key_exists('notification_sound', $pref_data))
        {
            $pref_data['notification_sound']=1;
        }
        else
        {
            $pref_data['notification_sound']=0;
        }
        unset($pref_data['_token']);
        if(Preference::where('user_id','=',$user->id)->update($pref_data))
        {
            return redirect('/user_prferrences');
        }
        else
        {
            Preference::create($pref_data);
            return redirect('/user_prferrences');
        }
        
    }
}

<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Cms;
use App\User;
use Validator;
use Artisan;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
class CmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*Artisan::call('config:cache');
        Artisan::call('view:clear');*/
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductDetails()
    {

        $user=Auth::guard('web')->user();
        $cms=new Cms;
        $cms_pages=$cms->get();
        return view('front.product.product_detail',['cms_pages'=>$cms_pages,'user'=>$user]);
    }
    public function getCmsPage($page_slug=null)
    {

        if(Auth::guard('admin')->check())
        {
            $user=Auth::guard('admin')->user();
        }
        else
        {
            $user['first_name']=null;
        }
        $cms=new Cms;
        $cms_pages=$cms->get();
        $page_data=$cms->where('slug','=',$page_slug)->get();
        return view('front.page',['page_data'=>$page_data,'cms_pages'=>$cms_pages,'user'=>$user]);
    }
    public function index()
    {
        return view('front.home');
//        if(Auth::guard('company')->check())
//        {
//            $user=Auth::guard('company')->user();
//            $cms=new Cms;
//            $cms_pages=$cms->get();
//            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
//        }
//        elseif(Auth::guard('user')->check())
//        {
//            $user=Auth::guard('user')->user();
//            $cms=new Cms;
//            $cms_pages=$cms->get();
//            $friends_count=Friends::where('user_id','=',$user->id)->where('status','=',1)->get()->count();
//            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
//        }
//        elseif(Auth::guard('web')->check())
//        {
//            $user=Auth::guard('web')->user();
//            $cms=new Cms;
//            $cms_pages=$cms->get();
//            $friends_count=Friends::where('user_id','=',$user->id)->where('status','=',1)->get()->count();
//            return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
//        }
//        elseif(Auth::guard('admin')->check())
//        {
//            $user=Auth::guard('web')->user();
//            $cms=new Cms;
//            $cms_pages=$cms->get();
//            $friends_count=Friends::where('user_id','=',$user->id)->where('status','=',1)->get()->count();
//            return redirect('/')->with('admin-alert-error','Admin Not Allowed To Login As User');
//        }
//        else
//        {
//            $cms=new Cms;
//            $cms_pages=$cms->get();
//            $user['first_name']=null;
//            $posts=null;
//            return view('front.landing',['cms_pages'=>$cms_pages,'user'=>$user]);
//        }
    }

    //Get Logged In User's Posts
    public function getUserPosts()
    {
        if(Auth::guard('web')->check())
            {
                $user=Auth::guard('web')->user();    
                $cms=new Cms;
                $cms_pages=$cms->get();
                return view('front.home',['cms_pages'=>$cms_pages,'user'=>$user]);
            }
    }
}

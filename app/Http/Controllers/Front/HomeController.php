<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Cms;
use App\Slider;
use App\User;
use Validator;
use Artisan;
use App\Helpers\BasicFunction;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        /* Artisan::call('config:cache');
          Artisan::call('view:clear'); */
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function pricing() {
        return view('front.home.price');
    }

    public function index() {

        $slider = new Slider;
        $sliders = $slider::where('status', '=', '1')->get();
        
        $cms = new Cms;
        $why = $cms::with('children')->where([['slug', '=', 'why-us'], ['status', '=', 1]])->first();
        $howitworks = $cms::with('children')->where([['slug', '=', 'how-it-works'], ['status', '=', 1]])->first();
        $blog = $cms::with('children')->where([['slug', '=', 'blog'], ['status', '=', 1]])->first();
        $prem_part = $cms::with('children')->where([['slug', '=', 'premium-partners'], ['status', '=', 1]])->first();
        $contactus = $cms::with('children')->where([['slug', '=', 'contact-us'], ['status', '=', 1]])->first();
        $aboutcompany = $cms::with('children')->where([['slug', '=', 'about-company'], ['status', '=', 1]])->first();
        $pricing = $cms::with('children')->where([['slug', '=', 'pricing'], ['status', '=', 1]])->first();

        return view('front.home.index', ['sliders' => $sliders, 'why' => $why, 'howitworks' => $howitworks, 'blog' => $blog, 'prem_part' => $prem_part, 'contactus' => $contactus, 'aboutcompany' => $aboutcompany, 'pricing' => $pricing]);
    }
    
    public function getProductDetails() {
        $user = Auth::guard('web')->user();
        $cms = new Cms;
        $cms_pages = $cms->get();
        return view('front.product.product_detail', ['cms_pages' => $cms_pages, 'user' => $user]);
    }

    public function getCmsPage($page_slug = null) {
        if (Auth::guard('admin')->check()) {
            $user = Auth::guard('admin')->user();
        } else {
            $user['first_name'] = null;
        }
        $cms = new Cms;
        $cms_pages = $cms->get();
        $page_data = $cms->where('slug', '=', $page_slug)->get();
        return view('front.page', ['page_data' => $page_data, 'cms_pages' => $cms_pages, 'user' => $user]);
    }

    //Get Logged In User's Posts
    public function getUserPosts() {
        if (Auth::guard('web')->check()) {
            $user = Auth::guard('web')->user();
            $cms = new Cms;
            $cms_pages = $cms->get();
            return view('front.home', ['cms_pages' => $cms_pages, 'user' => $user]);
        }
    }

}

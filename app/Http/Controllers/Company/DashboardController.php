<?php
namespace App\Http\Controllers\Company;

use App\User;
use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use Sortable;
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($r_id=NULL, $id=NULL){ 

        $pageTitle = 'Company and Sales Agent Dashboard';
        
        if(isset($r_id) && $r_id ==2 && isset($id) && $id !=''){

            $user = USER::findOrFail($id);
            return view('admin.dashboard.company',compact('pageTitle', 'user'));

        }else if(isset($r_id) && $r_id ==3 && isset($id) && $id !=''){
            $user = USER::findOrFail($id);
            return view('admin.dashboard.sales',compact('pageTitle', 'user'));

        }else if(isset($r_id) && $r_id ==4 && isset($id) && $id !=''){
            $user = USER::findOrFail($id);
            return view('admin.dashboard.buyer',compact('pageTitle', 'user'));

        }else{
            $user = companyUser();
            return view('company.dashboard.index',compact('pageTitle', 'user'));

        }  

    }
}

<?php

namespace App\Http\Controllers\Company;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Emailtemplate;
use App\User;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class EmailtemplatesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $emailtemplates = Emailtemplate::get();
        $pageTitle = 'Email Template';
        $title = "Email Template";
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . 'Email Template'] = 'emailtemplates';

        $breadcrumb = array('pages' => $pages, 'active' => "Email Template");
        setCurrentPage('admin.emailtemplates');
        return view('admin.emailtemplates.index', compact('user', 'pageTitle', 'title', 'breadcrumb','emailtemplates'));
    }

    public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $emailtemplates = Emailtemplate::find($id);
        if (empty($emailtemplates)) {
            return $this->InvalidUrl();
        }

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $pageTitle = 'Emailtemplate';
        $title = 'Emailtemplate';
        $controller = 'emailtemplates';
        $pages["<i class='fa fa-dashboard'></i>DASHBOARD"] = 'admin';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_EMAILTEMPLATE_PAGES'));
        return view('admin.emailtemplates.edit', compact('user','emailtemplates', 'pageTitle', 'title', 'breadcrumb','controller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
       $this->validate($request, [
            'name' => 'required|max:255',
            'subject' => 'required',
            'body' => 'required',
        ]);

        $emailtemplates = EmailTemplate::findOrFail($id);
        //$input = $request->all();
        $input['name'] = $request->name;
        $input['subject'] = $request->subject;
        $input['body'] = $request->body;
        $emailtemplates->fill($input)->save();
        return redirect()->action('Admin\EmailtemplatesController@index')->with('alert-sucess', 'Email template updated successfully.');
    }
    
     public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {
            $new_status = '0';
        } else {
            $new_status = '1';
        }
        $emailtemplate = Emailtemplate::where('id', '=', $id)->first();
        $emailtemplate->status = $new_status;
        $emailtemplate->save();

        return redirect()->action('Admin\EmailtemplatesController@index', getCurrentPage('admin.emailtemplates.index'))->with('alert-sucess', trans('admin.EMAILTEMPLATE_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function show($id)
    {
        //Emailtemplate::findOrFail($id)->delete();   
        //return redirect()->action('Admin\EmailtemplatesController@index', getCurrentPage('admin.emailtemplates.index'))->with('alert-sucess', 'Show Email Template'); 
    }
}

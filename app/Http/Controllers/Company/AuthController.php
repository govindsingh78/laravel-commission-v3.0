<?php

namespace App\Http\Controllers\Company;
use Illuminate\Http\Request;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;   
class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

    protected $guard = 'company';
    protected $redirectAfterLogout = '/';
    protected $loginPath = '/';
    protected $redirectTo = '/company/dashboard';

    //use RegistersUsers, AuthenticatesUsers, ThrottlesLogins;
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    /* public function __construct()
      {
      $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
      } */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:ar_users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }
/**
 * Handle a login request to the application.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function postLogin(Request $request){
    
    $validator = validator::make($request->all(),[
        'email' => 'required|email', 'password' => 'required',
    ]);
        
    if($validator->fails()) {
        //return redirect()->back()->withErrors($validator->errors());

        $errors = $validator->errors();
        $result['status'] = 0;
        $result['errors'] = $errors;
        return response()->json($result);             
    }

     $credentials = $this->credentials($request);
    //$credentials['role_id'] =   2;
    
    if (Auth::guard('company')->attempt($credentials, $request->has('remember'))) {
            $company = companyUser();
            $userType=$company['role_id'];
            switch ($userType) {
                case 2:
                //return redirect()->route('company.dashboard');
                $loginurl = 'company/dashboard';
                $loginMsg = 'Logging In as a Company !!';
                $result['status'] = 1;
                $result['loginMsg'] = $loginMsg;
                $result['loginurl'] = $loginurl;
                return response()->json($result); 
                break;
                case 3:
                $loginurl = 'company/dashboard';
                $loginMsg = 'Logging In as a Sales Agent !!';
                $result['status'] = 1;
                $result['loginMsg'] = $loginMsg;
                $result['loginurl'] = $loginurl;
                return response()->json($result);
                //return redirect()->route('company.dashboard');
                break;
                default:
                //Handle the Error
                $userNotApplicableMsg = 'User not applicable to login !!';
                $result['status'] = 2;
                $result['userNotApplicableMsg'] = $userNotApplicableMsg;
                return response()->json($result);    
                //return redirect('company/login');
                break;
            }

           //return view('front.home.index')->with('signin-success', 'Yes in !!');
           //return redirect()->intended($this->redirectPath())->with('alert-sucess', trans('admin.LOGIN_SUCCESSFULLY', ['site_tite'=>Configure('CONFIG_SITE_TITLE'),'Name' => $company->first_name]));
           //return view('company.dashboard.index');
    }

    //return redirect()->back()->with("signin-error","Invalid Login Credentials.")->with('signintab', 'signin');    
                $userInvalid = 'Invalid Login Credentials.';
                $result['status'] = 3;
                $result['userInvalid'] = $userInvalid;
                return response()->json($result); 
}
    /**
     * Show the application login form.
     *
     * @return Response
     */
    public function getLogin() {
       // return view('admin.auth.login');
       return view('front.home.index');
    }

    public function getLogout() {
       Auth::guard('company')->logout();
        return redirect()->route('home')->with('alert-sucess', trans('admin.LOGOUT_SUCCESSFULLY'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function authenticate() {
        
        if (Auth::guard('company')->attempt(['email' => $email, 'password' => $password, 'role_id' => 2], $remember)) {
            // Authentication passed...
           
             return redirect()->intended($this->redirectTo);
        } else {
            return redirect::to('auth/login');
        }
    }
    
  

}

<?php

namespace App\Http\Controllers\Company;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Price;
use App\PriceTags;
use App\Industry;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class PriceController extends Controller
{
    public function index() {
        
    	$per_page =  configure('CONFIG_PAGE_LIMIT');

        $prices = Price::orderBy('id', 'desc')->paginate($per_page);
        $pageTitle = 'Price';

        $pages['Dashboard'] = 'admin.dashboard';
        $breadcrumb = array('pages' => $pages, 'active' => "Price");

        setCurrentPage('admin.price.index');
        return view('admin.price.index', compact('prices', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function create() {

        $pageTitle = 'Price';
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Price'] = 'admin.price';
        $breadcrumb = array('pages' => $pages, 'active' => 'Add Price');
        return view('admin.price.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {

        $input = $request->all();
        $rules = ['name'=>'required'];
        $msg = ['name.required'=>'name is required'];

        if(isset($request->type) && $request->type == '1'){
            $rules['per_month'] ='required';
            $rules['per_year'] ='required';
            $msg['per_month.required'] ='per month price is required';
            $msg['per_year.required'] ='per year price is required';
        }
        
        $validator = validator::make($request->all(), $rules, $msg);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $priceObj = new Price();
        
        $price = $priceObj->create($input);

        if(isset($request->tags)){
            foreach ($request->tags as $tag) {
                if($tag !=''){
                    PriceTags::create([
                        'name' => $tag,
                        'price_id' => $price->id
                    ]);
                }    
            }
        }

        return redirect()->action('Admin\PriceController@index', getCurrentPage('price.index'))->with('alert-sucess', 'Price added successfully');
    }

    public function edit($id) {
        
        if($id == '') {
            return $this->InvalidUrl();
        }

        $price = Price::where('id', $id)->with('tags')->first();
        
        if(empty($price)){
            return $this->InvalidUrl();
        }

        $pageTitle = 'Price';
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Price'] = 'admin.price';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Price');
        return view('admin.price.edit', compact('price','pageTitle', 'breadcrumb'));
    }


    public function update(Request $request, $id) {

        $input = $request->all();
        $rules = ['name'=>'required'];
        $msg = ['name.required'=>'name is required'];

        if(isset($request->type) && $request->type == '1'){
            $rules['per_month'] ='required';
            $rules['per_year'] ='required';
            $msg['per_month.required'] ='per month price is required';
            $msg['per_year.required'] ='per year price is required';
        }
        
        $validator = validator::make($request->all(), $rules, $msg);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $price = Price::findOrFail($id);
        $price->fill($input)->save();

        PriceTags::where('price_id', $id)->delete();
        
        if(isset($request->tags) && !empty($request->tags)){
            foreach ($request->tags as $tag) {
                if($tag !=''){
                    PriceTags::create([
                        'name' => $tag,
                        'price_id' => $id
                    ]);
                }
            }    
        }

        return redirect()->action('Admin\PriceController@index', getCurrentPage('admin.price.index'))->with('alert-sucess', 'Price updated successfully');

    }

    public function price_tags(Request $request) {
        
        if($request->price_id !=''){
            $price_tags = PriceTags::where('price_id', $request->price_id)->get();
            $data = '';
            $data .= '<td colspan="6"><table class="table table-bordered"><tr><th>S.No.</th><th>Tag Name</th></tr>';
            if($price_tags->count()>0){
                $s=1;
                foreach($price_tags as $tag) {
                    $data .= '<tr><td>'.$s.'</td><td>fsadf</td></tr>';
                    $s++;
                }
            }else{
                $data .= '<tr><td colspan="2">No tags found</td></tr>';
            }

            $data .= '</table></td>';
            $result['status']  = 1;   
            $result['data'] = $data;
            return response()->json($result);
        }
    }
}

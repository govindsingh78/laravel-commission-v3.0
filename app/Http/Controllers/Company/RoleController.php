<?php
namespace App\Http\Controllers\Company;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class RoleController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $roles = Role::get();
        $pageTitle = 'Role';
        $title = "Role";
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . 'Role'] = 'roles';

        $breadcrumb = array('pages' => $pages, 'active' => "Role");
        setCurrentPage('admin.roles');
        return view('admin.roles.index', compact('user', 'pageTitle', 'title', 'breadcrumb','roles'));
    }

    public function create() {
        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $pageTitle = trans('admin.ROLE');
        $title = trans('admin.ROLE');
        $controller = 'roles';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ROLE')] = 'admin.roles.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_ROLE'));
        return view('admin.roles.create', compact('user','controller','pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $roleObj = new Role();
        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\RoleController@create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
        $roles = $roleObj->create($input);
        return redirect()->action('Admin\RoleController@index', getCurrentPage('admin.roles'))->with('alert-sucess', trans('admin.ROLE_ADD_SUCCESSFULLY'));
    }

    public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $roles = Role::find($id);
        if (empty($roles)) {
            return $this->InvalidUrl();
        }

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $pageTitle = trans('admin.ROLE');
        $title = trans('admin.ROLE');
        $controller = 'roles';
        $pages["<i class='fa fa-dashboard'></i>DASHBOARD"] = 'admin';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_ROLE'));
        return view('admin.roles.edit', compact('user','roles', 'pageTitle', 'title', 'breadcrumb','controller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
      ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\RoleController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $roles = Role::findOrFail($id);
        $input = $request->all();
        $roles->fill($input)->save();
        return redirect()->action('Admin\RoleController@index', getCurrentPage('admin.roles'))->with('alert-sucess', trans('admin.ROLE_UPDATE_SUCCESSFULLY'));
    }
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {
            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $roles = Role::where('id', '=', $id)->first();
        $roles->status = $new_status;
        $roles->save();

        return redirect()->action('Admin\RoleController@index', getCurrentPage('admin.roles.index'))->with('alert-sucess', trans('admin.ROLE_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function destroy($id)
    {
        Role::findOrFail($id)->delete();   
        return redirect()->action('Admin\RoleController@index', getCurrentPage('admin.roles.index'))->with('alert-sucess', trans('admin.ROLE_DELETED_SUCCESSFULLY')); 
    }

    public function show($id)
    {
        Role::findOrFail($id)->delete();   
        return redirect()->action('Admin\RoleController@index', getCurrentPage('admin.roles.index'))->with('alert-sucess', trans('admin.ROLE_DELETED_SUCCESSFULLY')); 
    }
}

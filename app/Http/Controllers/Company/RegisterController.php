<?php
namespace App\Http\Controllers\Company;
use Illuminate\Http\Request;
use App\User;
use Validator;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;   

//namespace App\Http\Controllers\Auth;
//use App\User;
//use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Validator;
//use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/company/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|number|max:10',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'role_id' => $data['role_id'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone' => $data['phone'],
            'otp' => $data['otp'],
            
        ]);
    }

    public function getRegister() {
       return view('front.home.index');
     }

     public function postRegister(Request $request){
        $createUser = new User();
        $validator = validator::make($request->all(),[
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required',
        ]);

        if($validator->fails()) {
            //return redirect()->back()->withErrors($validator->errors());    
            $errors = $validator->errors();
            $result['status'] = 0;
            $result['errors'] = $errors;
            return response()->json($result);           
        }
         $userInput = $request->all();
         //$user = $request->all();
         $createdUser = $this->create($userInput);
         $userEmail = $request->input('email');

         //Email Script Goes Here for both Admin and User
         $option = User::where('email', '=', $userEmail)->where('email_admin_status', '=', 0)->where('email_user_status', '=', 0)->first();
         $option->email_admin_status = 1;
         $option->email_user_status = 1;
         $option->save();
         $view = View::make('company.template.account_created', compact('option')); 
         $body = $view->render();
         $subject = 'New Account Created. Please Verify your Email Account';
         $toAdmin = 'govind.singh@shubhashish.in';
         $toUser = $userEmail;
         EmailHelper::sendMail1($toAdmin, '', '', $subject, 'default', $body);
         EmailHelper::sendMail1($toUser, '', '', $subject, 'default', $body);


         //$loginurl = 'sales/dashboard';
         $otpMsg = $userEmail;
         $result['status'] = 1;
         $result['otpMsg'] = $otpMsg;
         //$result['loginurl'] = $loginurl;

         return response()->json($result);
         
        //return redirect()->back()->with("signup-success","Account Created Successfully !!")->with('signuptab', 'signup');    
        
    }


    

    public function postOtpVerification(Request $request){
        $createUser = new User();
        $validator = validator::make($request->all(),[
            'otp_post' => 'required',
        ]);

        if($validator->fails()) {
            //return redirect()->back()->withErrors($validator->errors());    
            $errors = $validator->errors();
            $result['status'] = 0;
            $result['errors'] = $errors;
            return response()->json($result);           
        }

         
        $userEmail = $request->input('user_email');
        $otpPost = $request->input('otp_post');
        //fetch otp from db where email = userEmail
        $verifyOtp = User::where('email', '=', $userEmail)->where('otp', '=', $otpPost)->where('otp_status', '=', 0)->first();
        if(count($verifyOtp) == 0){
             //redirect back with error message that otp not verfied
             $result['status'] = 1;
             $result['otpPost'] = 'OTP Not Verified';
             return response()->json($result); 
         }
         else{
              //update otp_status = 1
                $option = User::where('email', '=', $userEmail)->where('otp', '=', $otpPost)->where('otp_status', '=', 0)->first();
                $option->otp_status = 1;
                $option->save();

              //$verifiedOtp = User::where('email', '=', $userEmail)->where('otp', '=', $otpPost)->where('otp_status', '=', 0)->first()->save(['otp_status' => 1]);
              $otpRedirect = 'company/dashboard';
              $result['status'] = 2;
              $result['otpPost'] = 'Verified OTP Successfully';
              $result['otpRedirect'] = $otpRedirect;
              return response()->json($result); 
         }
        

        $userInput = $request->all();
       // $createdUser = $this->create($userInput);
         
       // return redirect()->back()->with("signup-success","Account Created Successfully !!")->with('signuptab', 'signup');    
        
    }



}

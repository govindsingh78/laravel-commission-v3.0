<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Slider;
use App\User;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class SlidersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $id = Auth::guard('admin')->user()->id;
        
        $user = USER::findOrFail($id);

        $per_page =  configure('CONFIG_PAGE_LIMIT');
        
        $sliderlist = Slider::paginate($per_page); 
        
        $pageTitle = trans('admin.SLIDER_IMAGES');
        
        $title = trans('admin.SLIDER_IMAGES');
        
        $pages["<i class='fa fa-dashboard'></i>"  . trans('admin.DASHBOARD')] = 'admin.dashboard';
        
       // $pages[trans('admin.SLIDER_IMAGES')] = 'admin.sliders';
        
        $breadcrumb = array('pages' => $pages, 'active' => 'Sliders');
        setCurrentPage('admin.sliders');

        //pr($sliderlist); die;
        
        return view('admin.sliders.index', compact('user', 'pageTitle', 'title', 'breadcrumb', 'sliderlist'));
    }

    public function create() {
        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        
        $pageTitle = trans('admin.SLIDER_IMAGES');
        $title = trans('admin.SLIDER_IMAGES');
        $controller = 'sliders';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages[trans('admin.SLIDER_IMAGES')] = 'admin.sliders';
        $breadcrumb = array('pages' => $pages, 'active' => 'Add Slider');
        
        return view('admin.sliders.create', compact('user','controller','pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $sliderObj = new Slider();
        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'image' => 'required|image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size').'|dimensions:min_width='.Config::get('global.min_width').',max_width='.Config::get('global.max_width'),
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\SlidersController@create')
                            ->withErrors($validator)
                            ->withInput();
        }
        //pr($request->hasFile('image'));
        $input = $request->all();
        
        if($request->hasFile('image')){
            $input['image'] = BasicFunction::uploadImage(Input::file('image'), SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH, 'slider_');
        }
        
        $sliders = $sliderObj->create($input);
        return redirect()->action('Admin\SlidersController@index', getCurrentPage('admin.sliders'))->with('alert-sucess', trans('admin.SLIDER_IMAGES_ADD_SUCCESSFULLY'));
    }

    public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $slider = Slider::find($id);
        if (empty($slider)) {
            return $this->InvalidUrl();
        }

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        
        $pageTitle = trans('admin.SLIDER_IMAGES');
        $title = trans('admin.SLIDER_IMAGES');
        $controller = 'sliders';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Sliders'] = 'admin.sliders';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Slider');

        return view('admin.sliders.edit', compact('user', 'pageTitle', 'title', 'breadcrumb', 'controller', 'slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'image' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size').'|dimensions:min_width='.Config::get('global.min_width').',max_width='.Config::get('global.max_width'),
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\SlidersController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $slider = Slider::findOrFail($id);
        $input = $request->all();
        
        if($request->hasFile('image')){
            $input['image'] = BasicFunction::uploadImage(Input::file('image'), SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH, 'slider_', true, $slider->image);
        }
        else {
            $input['image'] = $slider->image;
        }
        
        $slider->fill($input)->save();
        return redirect()->action('Admin\SlidersController@index', getCurrentPage('admin.sliders'))->with('alert-sucess', trans('admin.SLIDER_IMAGES_UPDATE_SUCCESSFULLY'));
    }
    
    public function status_change($id, $status) {
        if (empty($id)) {
            return $this->InvalidUrl();
        }
        
        if ($status == '1') {
            $new_status = '0';
        }
        else {
            $new_status = '1';
        }
        $slider = Slider::where('id', '=', $id)->first();
        $slider->status = $new_status;
        $slider->save();
        return redirect()->action('Admin\SlidersController@index', getCurrentPage('admin.sliders'))->with('alert-sucess', trans('admin.SLIDER_IMAGES_CHANGE_STATUS_SUCCESSFULLY'));
    }
}

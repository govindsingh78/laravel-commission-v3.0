<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Cms;
use App\User;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class CmsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $per_page =  configure('CONFIG_PAGE_LIMIT');
        $cmslist = Cms::paginate($per_page);
        $pageTitle = 'CMS';
        $title = "CMS";
        $pages["<i class='fa fa-dashboard'></i>" . 'Dashboard'] = 'admin.dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => "Cms");
        setCurrentPage('admin.cms.index');
        return view('admin.cms.index', compact('user', 'pageTitle', 'title', 'breadcrumb','cmslist'));
    }

    public function create() {
        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $parent_cms = Cms::whereNull('parent_id')->orderBy('title', 'asc')->get()->toArray();
        $pageTitle = trans('admin.CMS_PAGES');
        $title = trans('admin.CMS_PAGES');
        $controller = 'cms';
        
        $cms = ['' => 'Select Title'];
        if($parent_cms){
            foreach ($parent_cms as $parentcms) {
                $cms[$parentcms['id']] = $parentcms['title'];
            }
        }
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages[trans('admin.CMS_PAGES')] = 'admin.cms';
        $breadcrumb = array('pages' => $pages, 'active' => 'Add Cms');

        return view('admin.cms.create', compact('user','controller','pageTitle', 'title', 'breadcrumb', 'cms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $cmsObj = new Cms();
        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'description' => 'required',
                    'meta_title' => 'required',
                    'meta_keywords' => 'required',
                    'meta_description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\CmsController@create')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($cmsObj, $request->title);
        
        if($request->hasFile('cms_image')){
            $input['cms_image'] = BasicFunction::uploadImage(Input::file('cms_image'), CMS_IMAGES_UPLOAD_DIRECTROY_PATH, 'cms_');
        }
        
        
        $cms = $cmsObj->create($input);
        return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_ADD_SUCCESSFULLY'));
    }

    public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $cms = Cms::find($id);
        if (empty($cms)) {
            return $this->InvalidUrl();
        }

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $parent_cms = Cms::whereNull('parent_id')->orderBy('title', 'asc')->get()->toArray();
        
        $per_cms = ['' => 'Select Title'];
        if($parent_cms){
            foreach ($parent_cms as $parentcms) {
                $per_cms[$parentcms['id']] = $parentcms['title'];
            }
        }
        
        $pageTitle = 'CMS';
        $title = 'CMS';
        $controller = 'cms';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Cms'] = 'admin.cms';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Cms');

        return view('admin.cms.edit', compact('user','cms', 'pageTitle', 'title', 'breadcrumb','controller', 'per_cms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'description' => 'required',
                    'meta_title' => 'required',
                    'meta_keywords' => 'required',
                    'meta_description' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\CmsController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $cms = Cms::findOrFail($id);
        $input = $request->all();
        
        if($request->hasFile('cms_image')){
            $input['cms_image'] = BasicFunction::uploadImage(Input::file('cms_image'), CMS_IMAGES_UPLOAD_DIRECTROY_PATH, 'cms_', true, $input['cms_image']);
        }
        else {
            $input['cms_image'] = $cms->cms_image;
        }
        
        $cms->fill($input)->save();
        return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_UPDATE_SUCCESSFULLY'));
    }
    
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $cms = Cms::where('id', '=', $id)->first();
        $cms->status = $new_status;
        $cms->save();
        return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_CHANGE_STATUS_SUCCESSFULLY'));
    }
}

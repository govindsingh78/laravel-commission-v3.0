<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use View;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        $id = Auth::guard('admin')->user()->id;
        
        $user = USER::findOrFail($id);
        
        $roles = Role::get();
        $allroles = array();
        if($roles){
            $allroles[''] = "Select Role";
            foreach ($roles as $role) {
                if($role->name!='Admin'){
                    $allroles[$role->id] = $role->name;
                }
            }
        }
        
        $form_data ="";
        $params = $request->all();
        $form_data['status']='';
        $form_data['role_id']='';

        $where ="1 = 1 ";
        if(isset($params['status']) && $params['status']!=""){
            $form_data['status'] = $params['status'];
            $where.=" AND status = ".$params['status'];
        } 

        if(isset($params['role_id']) && $params['role_id']!=""){
            $form_data['role_id'] = $params['role_id'];
            $where.=" AND role_id = ".$params['role_id'];
        } 


        $users = User::whereRaw($where)->where('id', '!=', Config::get('global.id.admin'))->where('role_id', '!=', 1)->with('role')->with('refer')->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        $controller = 'users';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_USER'));

        return view('admin.users.index', compact('user','users','allroles','controller','pageTitle', 'title', 'breadcrumb','form_data'));
    }

    public function create() {
        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $roles = Role::get();
        $allroles = array();
        if($roles){
//            $allroles[''] = "Select Role";
            foreach ($roles as $role) {
                if($role->id != 1){
                    $allroles[$role->id] = $role->name;
                }
            }
        }

        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        $controller = 'users';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_USER'));
        return view('admin.users.create', compact('user','allroles','controller','pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $userObj = new User();
        $validator = validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|unique:users,email',
                    'phone' => 'required|digits_between:10,12|numeric',
                    'role_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
        $input['password'] = bcrypt(rand(100000, 999999)); 
        pr($input);die;
        $users = $userObj->create($input);
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_ADD_SUCCESSFULLY'));
    }

    public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $users = User::find($id);
        if (empty($users)) {
            return $this->InvalidUrl();
        }

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $roles = Role::get();
        $allroles = array();
        if($roles){
//            $allroles[''] = "Select Role";
            foreach ($roles as $role) {
                if($role->id != 1){
                    $allroles[$role->id] = $role->name;
                }
            }
        }
        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        $controller = 'users';
        $pages["<i class='fa fa-dashboard'></i>DASHBOARD"] = 'admin';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_USER'));
        return view('admin.users.edit', compact('user','allroles','users', 'pageTitle', 'title', 'breadcrumb','controller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $users = User::findOrFail($id);
        
        $validator = validator::make($request->all(), [
                      'first_name' => 'required|max:255',
                      'last_name' => 'required|max:255',
                      'email' => 'required|email|unique:users,email,'.$users->id,
                      'phone' => 'required|digits_between:10,12|numeric',
  //                    'role_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
        $users->fill($input)->save();
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_UPDATE_SUCCESSFULLY'));
    }
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {
            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $users = User::where('id', '=', $id)->first();
        $users->status = $new_status;
        $users->save();

        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users.index'))->with('alert-sucess', trans('admin.USER_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function destroy($id)
    {
        User::findOrFail($id)->delete();   
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users.index'))->with('alert-sucess', trans('admin.USER_DELETED_SUCCESSFULLY')); 
    }

    public function show($id)
    {
        User::findOrFail($id)->delete();   
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users.index'))->with('alert-sucess', trans('admin.USER_DELETED_SUCCESSFULLY')); 
    }

    public function references(){

        $per_page =  configure('CONFIG_PAGE_LIMIT');
        
        $users = DB::table('users as a')
        ->select(DB::raw('CONCAT(b.first_name," ",b.last_name) AS name'), 'b.role_id', DB::raw('count(a.refer_id) as total'))
        ->join('users as b', 'a.refer_id','=','b.id')
        ->where('a.refer_id', '!=', '0')
        ->groupBy('a.refer_id', 'b.first_name', 'b.last_name', 'b.role_id')
        ->paginate($per_page);

        $title = $pageTitle = 'Top user references';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => 'top user references');
        setCurrentPage('admin.user.references');

       // pr($breadcrumb); die;

        return view('admin.users.references', compact('users', 'pageTitle', 'title', 'breadcrumb'));
    }


    public function reference($id=NULL){
        
        //$user = User::findOrFail($id);

        $user = User::with('role')->where('id', $id)->first();
        
        $per_page =  configure('CONFIG_PAGE_LIMIT');
        $users = User::where('refer_id', $id)->paginate($per_page);
        $title = $pageTitle = 'User reference';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';

        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = array('admin.dashboard', $user->role_id, $id);

        $breadcrumb = array('pages' => $pages, 'active' => 'User Reference');
        setCurrentPage('admin.user.reference');

        return view('admin.users.reference', compact('user', 'users', 'pageTitle', 'title', 'breadcrumb'));
    }




    public function forgotPassword() {
        //$pageTitle = trans('admin.CONFIG_MANAGEMENT');
        //$title = trans('admin.CONFIG_MANAGEMENT');
        $pageTitle = 'forgot password';

        return view('admin.users.forgot_password', compact('pageTitle', 'title'));
    }

    public function sendPasswordLink(Request $request) {

        $validator = validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());                
        }

        $input = $request->all();

        $email = $input['email'];
        $user = User::where('email', '=', $email)->where('role_id', '=', 1)->first();
        
        if(empty($user->id)) {
            return redirect()->back()->with("error","User not exist.please try with another email."); 
        }

        $email_token = md5(uniqid(rand(), true));

        $is_update = User::where('id', $user->id)->update(['email_token' => $email_token]);

        $view = View::make('admin.template.forgot_password', compact('user', 'email_token')); 
        $body = $view->render();
        
        $subject = 'Reset your Commission Adminstrator Password';
        //$to = $user->email;
        $to = 'ganeshkumawat85@gmail.com';
        EmailHelper::sendMail1($to, '', '', $subject, 'default', $body);

        return redirect()->back()->with("success","Password reset link sent successfully on your registered Email address !");

    }


    function resetPassword($email_token = null) {
        if($email_token == null) {
            return redirect()->back()->with("error","Invalid email token."); 
        }

        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 1)->first();

        if(empty($user->id)) {
            return redirect()->back()->with("error","User not exist."); 
        }

        $pageTitle = trans('admin.RESET_PASSWORD');
        $title = trans('admin.RESET_PASSWORD');
        return view('admin.users.reset_password', compact('pageTitle', 'title', 'email_token'));
    }

    public function resetPasswordUpdate(Request $request, $email_token) {

        //$2y$10$AxUW4eO7Gz6TSbU6GcOzCesffo2peQLPBjrPrbzgm1lqqTZFhvAAu

        $inputs = $request->all();
        
        $validator = validator::make($request->all(), [
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()) {
           return redirect()->back()->withErrors($validator->errors());      
        } 

        if($inputs['password']!=$inputs['confirm_password']){
            return redirect()->back()->with("error","New password and Confirm password does not mathched.Please try again.");
        }

        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 1)->first();

        $password = bcrypt($request->get('password'));

        $is_update = User::where('id', $user->id)
                ->update(['email_token' => '', 'password' => $password]);
        //return redirect()->action('Admin\AuthController@getLogin')->with('sucess', 'Account password changed successfully');

        return redirect()->to('admin/login')->with('sucess', 'Account password changed successfully');

    }

    public function changepassword() {

        $pageTitle = trans('admin.CHANGE_PASSWORD');
        $title = trans('admin.CHANGE_PASSWORD');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CHANGE_PASSWORD'));
        //pr($breadcrumb); die;
        return view('admin.users.change_password', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function updatepassword(Request $request) {

        $input = $request->all();
        //pr($input); die;

        $user = Auth::guard('admin')->user();

        $validator = validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|string|min:6',
        ]);

        if($validator->fails()) {
           return redirect()->back()->withErrors($validator->errors());      
        } 

        if(!(Hash::check($request->get('old_password'), $user->password))) {

            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0){
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }


        if(strcmp($request->get('new_password'), $request->get('confirm_password')) == 1){
            return redirect()->back()->with("error","New password and Confirm password does not mathched.Please try again.");
        }
 
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
 
        return redirect()->back()->with("success","Password changed successfully !");

    }

    

}

<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use App\History;
use App\User;

class HistoryController extends Controller {

    public function index(Request $request) {

        $params = $request->all();

        $pageTitle = 'Login history';
        $title = 'Login history';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => 'user login history');
        setCurrentPage('admin.history');

        $per_page =  configure('CONFIG_PAGE_LIMIT');
        
        if(isset($params['search']) && $params['search'] !=''){

            $word = $params['search'];
            
            $where = '1';
            $where .= " or ip_address like '%".$word."%'";

            $where_1 = '1';
            $where_1 .= " or first_name like '%".$word."%' or last_name like '%".$word."%'";
            $histories = History::with('user')->whereHas('user', function($query) use($word) {
            $query->where('first_name', 'like', '%'.$word.'%')->orWhere('last_name','LIKE','%'.$word.'%');
            })->orWhere('ip_address','LIKE','%'.$word.'%')->orderBy('id', 'desc')->paginate($per_page); 

        }else{          
            $histories = History::with('user')->orderBy('id', 'desc')->paginate($per_page);    
        }

        return view('admin.history.index', compact('histories', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function user_history($id=NULL){

        $user = User::findOrFail($id);
        
        $pageTitle = 'Login history';
        $title = 'Login history';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = array('admin.dashboard', $user->role_id, $id);

        $breadcrumb = array('pages' => $pages, 'active' => 'user login history');
        setCurrentPage('admin.user.history');

        $per_page =  configure('CONFIG_PAGE_LIMIT');

        $histories = History::where('user_id', $id)->with('user')->orderBy('id', 'desc')->paginate($per_page);
        
        return view('admin.history.user-history', compact('histories', 'pageTitle', 'title', 'breadcrumb'));
    }

}

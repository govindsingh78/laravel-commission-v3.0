<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Setting;
use App\User;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use File;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class SettingsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index() {

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $setting = Setting::find(1);
        $pageTitle = trans('admin.CONFIG_MANAGEMENT');
        $title = trans('admin.CONFIG_MANAGEMENT');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CONFIG_MANAGEMENT'));
        return view('admin.settings.index', compact('user','setting', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

       $validator = validator::make($request->all(), [
                    'site_title' => 'required|max:255',
                    'copyright' => 'required|max:255',
                    'site_logo' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size').'|dimensions:min_width='.Config::get('global.logo_min_width').',max_width='.Config::get('global.logo_max_width'),
                    'footer_advt_img' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size').'|dimensions:min_width='.Config::get('global.earn_ad_min_width').',max_width='.Config::get('global.earn_ad_max_width'),
                    'site_slider_title' => 'required',
                    'site_slider_description' => 'required',
                    'mobile' => 'required',
                    'email' => 'required',
                    'page_limit' => 'required|numeric',
                    'front_page_limit' => 'required|numeric',
                    'from_name' => 'required',
                    'from_email' => 'required|email|max:255',
                    'reply_to_email' => 'required|email|max:255',
                    'site_emergency_note' => 'required',
                    'meta_title' => 'required',
                    'meta_keywords' => 'required',
                    'meta_description' => 'required',
                    'email_signature' => 'required',
                    'address' => 'required',
                    'contact_address' => 'required',
                    'facebook_icon' => 'required',
                    'twitter_icon' => 'required',
                    'google_plus_icon' => 'required',
                    'linkedin_icon' => 'required',
                    'site_advt_image' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size').'|dimensions:min_width='.Config::get('global.site_ad_min_width').',max_width='.Config::get('global.site_ad_max_width'),
        ]);
       
        if ($validator->fails()) {
            return redirect()->action('Admin\SettingsController@index')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $setting = Setting::findOrFail($id);
        $input = $request->all();
        
        if($request->hasFile('site_logo')){
            $input['site_logo'] = BasicFunction::uploadImage(Input::file('site_logo'), SITE_IMAGES_UPLOAD_DIRECTROY_PATH, 'logo_', true, $setting->site_logo);
        }
        else {
            $input['site_logo'] = $setting->site_logo;
        }
        
        if($request->hasFile('footer_advt_img')){
            $input['footer_advt_img'] = BasicFunction::uploadImage(Input::file('footer_advt_img'), SITE_IMAGES_UPLOAD_DIRECTROY_PATH, 'refer_', true, $setting->footer_advt_img);
        }
        else {
            $input['footer_advt_img'] = $setting->footer_advt_img;
        }
        
        if($request->hasFile('site_advt_image')){
            $input['site_advt_image'] = BasicFunction::uploadImage(Input::file('site_advt_image'), SITE_IMAGES_UPLOAD_DIRECTROY_PATH, 'advt_', true, $setting->site_advt_image);
        }
        else {
            $input['site_advt_image'] = $setting->site_advt_image;
        }
        
        $setting->fill($input)->save();
        $setting = Setting::find(1)->toArray();
        
        $filename = 'f' . gmdate('YmdHis');
        $path = base_path() . '/config/';
        File::put($path . $filename, '<?php ' . "\n");
        File::append($path . $filename, 'return [ ' . "\n");

        foreach ($setting as $key => $value) {
            $constant = "CONFIG_" . strtoupper($key);
            File:: append($path . $filename, '"' . $constant . '"   =>   "' . addslashes($value) . '",' . "\n");
        }
        File:: append($path . $filename, ' ];');
        @rename($path . $filename, $path . 'settings.php');
        return redirect()->action('Admin\SettingsController@index')->with('alert-sucess', 'Site configuration save successfully.');
    }

}

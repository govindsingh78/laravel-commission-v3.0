<?php

namespace App\Http\Controllers\Admin;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Industry;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class IndustryController extends Controller
{
    public function index(Request $request) {

    	$params = $request->all();

    	$per_page =  configure('CONFIG_PAGE_LIMIT');

    	if(isset($params['search']) && $params['search'] !=''){
			$word = $params['search'];
            $industries = Industry::where('name', 'like', '%'.$word.'%')->orderBy('id', 'desc')->paginate($per_page); 
        }else{  
			$industries = Industry::orderBy('id', 'desc')->paginate($per_page); 
		}	

        $pageTitle = 'Industry';
        
        $pages["<i class='fa fa-dashboard'></i>" . 'Dashboard'] = 'admin.dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => "Industry");
        setCurrentPage('industry.index');
        return view('admin.industry.index', compact('industries', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function create() {
        
        $pageTitle = 'Industry';
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Add Industry'] = 'Industry.index';
        $breadcrumb = array('pages' => $pages, 'active' => 'Add Industry');
        return view('admin.industry.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
        
        $indsObj = new Industry();
        $validator = validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        
        $inds = $indsObj->create($input);
        return redirect()->action('Admin\IndustryController@index', getCurrentPage('industry.index'))->with('alert-sucess', 'Industry added successfully');
    }

    public function edit($id) {
        
        if($id == '') {
            return $this->InvalidUrl();
        }

        $industry = Industry::find($id);
        if(empty($industry)){
            return $this->InvalidUrl();
        }

        $pageTitle = 'Industry';
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Add Industry'] = 'Industry.index';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Industry');

        return view('admin.industry.edit', compact('industry','pageTitle', 'breadcrumb'));
    }


    public function update(Request $request, $id) {
      	$validator = validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $industry = Industry::findOrFail($id);
        $input = $request->all();
        $industry->fill($input)->save();
        
        return redirect()->action('Admin\IndustryController@index', getCurrentPage('industry.index'))->with('alert-sucess', 'Industry updated successfully');
    }



    public function status_change($id, $status) {

    	if(empty($id)) {
            return $this->InvalidUrl();
        }

        if($status == '1') {
			$new_status = '0';
        }else {
			$new_status = '1';
        }

        $industry = Industry::where('id', '=', $id)->first();
        $industry->status = $new_status;
        $industry->save();

        return redirect()->action('Admin\IndustryController@index', getCurrentPage('industry.index'))->with('alert-sucess', 'Industry status change successfully');
    }
}

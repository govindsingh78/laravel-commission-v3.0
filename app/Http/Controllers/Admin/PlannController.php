<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Plann;
use App\User;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class PlannController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $per_page =  configure('CONFIG_PAGE_LIMIT');
        $cmslist = Plann::orderBy('sort_order', 'asc')->orderBy('updated_at', 'desc')->paginate($per_page);
        $pageTitle = 'Featured Plann';
        $title = "Featured Plann";
        $pages["<i class='fa fa-dashboard'></i>" . 'Dashboard'] = 'admin.dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => "Plann");
        setCurrentPage('admin.plann.index');
        return view('admin.plann.index', compact('user', 'pageTitle', 'title', 'breadcrumb','cmslist'));
    }

    public function create() {
        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $parent_plann = Plann::orderBy('id', 'desc')->get()->toArray();
       
        $pageTitle = trans('admin.Plann_Feature');
        $title = trans('admin.Plann_Feature');
        $controller = 'plann';
        
        $plann = ['' => 'Select Plann'];
        if($parent_plann){
            foreach ($parent_plann as $parentplann) {
                $plann[$parentplann['id']] = $parentplann['plann_name'];
            }
        }
        //$sort_ord=count($parent_plann);
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages[trans('Plann Feature')] = 'admin.plann';
        $breadcrumb = array('pages' => $pages, 'active' => 'Add Plann');

        return view('admin.plann.create', compact('user','controller', 'pageTitle', 'title', 'breadcrumb', 'plann'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $plannObj = new Plann();
       // return $plannObj;
       $validator = validator::make($request->all(), [
                    'plann_name' => 'required|max:255',
                    'price' => 'required|numeric',
                    'feature_name' => 'required',
                    'status' => 'required'
        ]);
         if ($validator->fails()) {
            return redirect()->action('Admin\PlannController@create')
                            ->withErrors($validator)
                            ->withInput();
        }
        
         $input = $request->all();
        $input['slug_name'] = BasicFunction::getUniqueSlugPlann($plannObj, $request->plann_name);
      
        /*
        
        if($request->hasFile('cms_image')){
            $input['cms_image'] = BasicFunction::uploadImage(Input::file('cms_image'), CMS_IMAGES_UPLOAD_DIRECTROY_PATH, 'cms_');
        }
        */
        
        
         $plann = $plannObj->create($input);
      // exit;    
        return redirect()->action('Admin\PlannController@index', getCurrentPage('admin.plann'))->with('alert-sucess', trans('Featured Plann Added !!'));
    }


    public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
          $countAll = Plann::all();
          $plann = Plann::find($id);
        if (empty($plann)) {
            return $this->InvalidUrl();
        }

          $id = Auth::guard('admin')->user()->id;
          $user = USER::findOrFail($id);
        /*$parent_cms = Plann::orderBy('plann_name', 'asc')->get()->toArray();
        
        $per_cms = ['' => 'Select Title'];
        if($parent_cms){
            foreach ($parent_cms as $parentcms) {
                $per_cms[$parentcms['id']] = $parentcms['title'];
            }
        }
        
        
        */
        $pageTitle = 'Feature Plann';
        $title = 'Feature Plann';
        $controller = 'plann';
         
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Plann'] = 'admin.plann';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Plann');
       // return view('admin.plann.edit')->with('plann', $plann);
        return view('admin.plann.edit', compact('user','plann', 'pageTitle', 'title', 'breadcrumb','controller','countAll'));
       //return view('admin.plann.edit', compact('user','cms','controller', 'per_cms'));
    
    }

    /*public function edit($id) {
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $cms = Cms::find($id);
        if (empty($cms)) {
            return $this->InvalidUrl();
        }

        $id = Auth::guard('admin')->user()->id;
        $user = USER::findOrFail($id);
        $parent_cms = Cms::whereNull('parent_id')->orderBy('title', 'asc')->get()->toArray();
        
        $per_cms = ['' => 'Select Title'];
        if($parent_cms){
            foreach ($parent_cms as $parentcms) {
                $per_cms[$parentcms['id']] = $parentcms['title'];
            }
        }
        
        $pageTitle = 'CMS';
        $title = 'CMS';
        $controller = 'cms';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Cms'] = 'admin.cms';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Cms');

        return view('admin.cms.edit', compact('user','cms', 'pageTitle', 'title', 'breadcrumb','controller', 'per_cms'));
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $validator = validator::make($request->all(), [
        'plann_name' => 'required|max:255',
        'price' => 'required|numeric',
        'feature_name' => 'required',
        'status' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\PlannController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $plann = Plann::findOrFail($id);
        $input = $request->all();
        
        /*if($request->hasFile('cms_image')){
            $input['cms_image'] = BasicFunction::uploadImage(Input::file('cms_image'), CMS_IMAGES_UPLOAD_DIRECTROY_PATH, 'cms_', true, $input['cms_image']);
        }
        else {
            $input['cms_image'] = $cms->cms_image;
        }*/
        
        $plann->fill($input)->save();
        return redirect()->action('Admin\PlannController@index', getCurrentPage('admin.plann'))->with('alert-sucess', trans('Feature Plann Updated Successfully !!'));
    }
    
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $plann = Plann::where('id', '=', $id)->first();
        $plann->status = $new_status;
        $plann->save();
        return redirect()->action('Admin\PlannController@index', getCurrentPage('admin.plann'))->with('alert-sucess', trans('Status Changed Successfully !!'));
    }
}

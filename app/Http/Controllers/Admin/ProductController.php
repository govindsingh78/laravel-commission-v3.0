<?php

namespace App\Http\Controllers\Admin;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Product;
use App\ProductsPhoto;
use App\ProductsIndustry;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
Use DB;
use Session;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;

class ProductController extends Controller
{
    public function index(Request $request) {

    //http://www.expertphp.in/article/search-and-pagination-with-crud-example-in-laravel-php-framework-and-angularjs	

        $params = $request->all();

    	$per_page =  configure('CONFIG_PAGE_LIMIT');

    	$per_page =  2;

    	if(isset($params['search']) && $params['search'] !=''){
			$word = $params['search'];
            $products = Product::where('name', 'like', '%'.$word.'%')->orderBy('id', 'desc')->paginate($per_page); 
        }else{  
			$products = Product::orderBy('id', 'desc')->paginate($per_page); 
		}	

        $pageTitle = 'Product';
        
        $pages["<i class='fa fa-dashboard'></i>" . 'Dashboard'] = 'admin.dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => "Products");
        setCurrentPage('product.index');
        return view('admin.product.index', compact('products', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function create() {

    	$industries = BasicFunction::getIndustries();
        
        $pageTitle = 'Product';
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Products'] = 'product.index';
        $breadcrumb = array('pages' => $pages, 'active' => 'Add Product');
        
        return view('admin.product.create', compact('industries', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
    	$rules = [
            'name' => 'required|max:255',
            'photos' => 'required',
        ];

        $msges = array(
            'name.required'=>'Please enter product name',
            'photos.required'=>'Product photo is required',
        );

        $this->validate($request, $rules, $msges);

    	$product = Product::create($request->all());
    	if(isset($request->photos)){
    		foreach ($request->photos as $photo) {
            	$filename = $photo->store('products_photo');
	            ProductsPhoto::create([
	                'product_id' => $product->id,
	                'product_img' => $filename
	            ]);
        	}
    	}
    	
    	if(isset($request->industries) && !empty($request->industries)){
    		for($i=0; $i < count($request->industries); $i++) { 
    			ProductsIndustry::create([
	                'product_id' => $product->id,
	                'industry_id' => $request->industries[$i]
	            ]);
    		}
    	}
    	return redirect()->action('Admin\ProductController@index', getCurrentPage('product.index'))->with('alert-sucess', 'Product added successfully');
    }

    public function edit($id) {

    	if($id == '') {
			return $this->InvalidUrl();
        }
        
        $with = array('photos', 'industries');
        $product = Product::where('id', $id)->with($with)->first();
        
        if(empty($product)){
            return $this->InvalidUrl();
        }

        $industries = BasicFunction::getIndustries();

        $pageTitle = 'Product';
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'admin.dashboard';
        $pages['Products'] = 'product.index';
        $breadcrumb = array('pages' => $pages, 'active' => 'Edit Product');
        return view('admin.product.edit', compact('product','industries','pageTitle', 'breadcrumb'));
    }


    public function update(Request $request, $id) {

    	$rules = [
            'name' => 'required|max:255',
        ];

        $msges = array(
            'name.required'=>'Please enter product name',
        );

        $this->validate($request, $rules, $msges);

        $product = Product::findOrFail($id);
        $input = $request->all();
        $product->fill($input)->save();

        if($request->img_deleted !=''){
			$imgs = explode(',', $request->img_deleted);
			for($i=0; $i<count($imgs); $i++){
				ProductsPhoto::where('id', $imgs[$i])->delete();	
			}
        }

    	if(isset($request->photos)){
    		foreach ($request->photos as $photo) {
            	$filename = $photo->store('products_photo');
	            ProductsPhoto::create([
	                'product_id' => $product->id,
	                'product_img' => $filename
	            ]);
        	}
    	}
    	
    	ProductsIndustry::where('product_id', $id)->delete();
    	if(isset($request->industries) && !empty($request->industries)){
    		for($i=0; $i < count($request->industries); $i++) { 
    			ProductsIndustry::create([
	                'product_id' => $product->id,
	                'industry_id' => $request->industries[$i]
	            ]);
    		}
    	}
    	return redirect()->action('Admin\ProductController@index', getCurrentPage('product.index'))->with('alert-sucess', 'Product updated successfully');

    }

    public function status_change($id, $status) {

    	if(empty($id)) {
            return $this->InvalidUrl();
        }

        if($status == '1') {
			$new_status = '0';
        }else {
			$new_status = '1';
        }

        $industry = Industry::where('id', '=', $id)->first();
        $industry->status = $new_status;
        $industry->save();

        return redirect()->action('Admin\IndustryController@index', getCurrentPage('industry.index'))->with('alert-sucess', 'Industry status change successfully');
    }
}

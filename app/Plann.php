<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class Plann extends Authenticatable
{
   // use Notifiable,Sortable;

   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plann';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','plann_name','slug_name','price','feature_name','status','sort_order','is_deleted','created_at','updated_at'];  
    
   /* public function parent() {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children() {
        return $this->hasMany(static::class, 'parent_id');
    }*/
}

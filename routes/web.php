<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Front hand routes goes here*/


Route::group(['namespace'=>'Front'],function(){
    Route::get('/',['as'=>'home','uses'=>'HomeController@index']);
    Route::get('/pricing',['as'=>'pricing','uses'=>'HomeController@pricing']);
    Route::get('login',['as'=>'user','uses'=>'UserController@getlogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'UserController@postLogin']);
   /// Route::get('/user',['as'=>'home','uses'=>'UserController@index']);
  
});

/*Company routes goes here*/

Route::group(['namespace' => 'Company', 'prefix' => 'company'], function() {
    


    Route::group(['middleware' => 'IsNotAuthenticated'], function(){
        Route::get('register', ['as' => 'company.register', 'uses' => 'RegisterController@getRegister']);
        Route::post('register', ['as' => 'company.register', 'uses' => 'RegisterController@postRegister']);
         
        //Route::get('login', ['as' => 'company.login', 'uses' => 'AuthController@getlogin']);
        Route::get('login', ['as' => 'company.login', 'uses' => 'AuthController@getlogin']);
        Route::post('login', ['as' => 'company.login', 'uses' => 'AuthController@postLogin']);
        Route::post('logout', ['as' => 'company.logout', 'uses' => 'AuthController@getLogout']);
        
        //Route::get('otp-verification', ['as' => 'company.otp-verification', 'uses' => 'RegisterController@postOtpVerification']);
        Route::post('otp-verification', ['as' => 'company.otp-verification', 'uses' => 'RegisterController@postOtpVerification']);
        
        Route::get('forgot-password', ['as' => 'company.forgot_password', 'uses' => 'UsersController@forgotPassword']);
        Route::post('forgot-password', ['as' => 'company.forgot_password', 'uses' => 'UsersController@sendPasswordLink']);
        
        Route::get('reset-password/{email_token}', ['as' => 'company.reset_password', 'uses' => 'UsersController@resetPassword']);
        Route::post('reset-password/{email_token}', ['as' => 'company.reset_password', 'uses' => 'UsersController@resetPasswordUpdate']);
    });

    Route::group(['middleware' => 'auth.company'], function() {
        
        // Route::get('/dashboard', function(){
        //     return view('company.dashboard.index');
        //   });
        
        //Route::any('dashboard/{r_id?}/{id?}', ['as' => 'company.dashboard', 'uses' => 'DashboardController@index']);
        Route::any('dashboard', ['as' => 'company.dashboard', 'uses' => 'DashboardController@index']);
        Route::get('change-password', ['as' => 'company.changepassword', 'uses' => 'UsersController@changepassword']);
        Route::post('change-password', ['as' => 'company.updatepassword', 'uses' => 'UsersController@updatepassword']);
        
        Route::resource('roles', 'RoleController', ['names' => [
            'index' => 'admin.roles',
            'create' => 'admin.roles.create',
            'update' => 'admin.roles.update',
            'edit' => 'admin.roles.edit',
            'store' => 'admin.roles.store',
            'destroy' => 'admin.roles.destroy',
        ]]);

        Route::get('role-status-change/{id}/{status}', ['as'=>'admin.roles.status_change','uses' => 'RoleController@status_change']);

        Route::resource('users', 'UsersController', ['names' => [
            'index' => 'admin.users',
            'create' => 'admin.users.create',
            'update' => 'admin.users.update',
            'edit' => 'admin.users.edit',
            'store' => 'admin.users.store',
            'show' => 'admin.users.show',
            'destroy' => 'admin.users.destroy',
        ]]);
        Route::post('users/index', ['as' => 'admin.users.index', 'uses' => 'UsersController@index']);
        
        Route::get('references', ['as'=>'admin.user.references','uses' => 'UsersController@references']);
        Route::get('reference/{id}', ['as'=>'admin.user.reference','uses' => 'UsersController@reference']);


        Route::get('user-status-change/{id}/{status}', ['as'=>'admin.users.status_change','uses' => 'UsersController@status_change']);

        Route::resource('contactus', 'ContactusController', ['names' => [
            'index' => 'admin.contactus',
            'create' => 'admin.contactus.create',
            'update' => 'admin.contactus.update',
            'edit' => 'admin.contactus.edit',
            'store' => 'admin.contactus.store',
            'show' => 'admin.contactus.show',
            'destroy' => 'admin.contactus.destroy',
        ]]);

        Route::get('contactus-status-change/{id}/{status}', ['as'=>'admin.contactus.status_change','uses' => 'ContactusController@status_change']);

        Route::resource('emailtemplates', 'EmailtemplatesController', ['names' => [
            'index' => 'admin.emailtemplates',
            'create' => 'admin.emailtemplates.create',
            'update' => 'admin.emailtemplates.update',
            'edit' => 'admin.emailtemplates.edit',
            'store' => 'admin.emailtemplates.store',
            'show' => 'admin.emailtemplates.show',
            'destroy' => 'admin.emailtemplates.destroy',
        ]]);
        Route::get('emailtemplates-status-change/{id}/{status}', ['as'=>'admin.emailtemplates.status_change','uses' => 'EmailtemplatesController@status_change']);

        Route::resource('settings', 'SettingsController', ['names' => [
            'index' => 'admin.settings',
            'create' => 'admin.settings.create',
            'update' => 'admin.settings.update',
            'edit' => 'admin.settings.edit',
            'store' => 'admin.settings.store',
            'show' => 'admin.settings.show',
            'destroy' => 'admin.settings.destroy',
        ]]);

        Route::resource('cms', 'CmsController', ['names' => [
            'index' => 'admin.cms',
            'create' => 'admin.cms.create',
            'update' => 'admin.cms.update',
            'edit' => 'admin.cms.edit',
            'store' => 'admin.cms.store',
            'show' => 'admin.cms.show',
            'destroy' => 'admin.cms.destroy',
        ]]);

        //plann controller
        Route::resource('plann', 'PlannController', ['names' => [
            'index' => 'admin.plann',
            'create' => 'admin.plann.create',
            'update' => 'admin.plann.update',
            'edit' => 'admin.plann.edit',
            'store' => 'admin.plann.store',
            'show' => 'admin.plann.show',
            'destroy' => 'admin.plann.destroy',
        ]]);

        Route::get('plann-status-change/{id}/{status}', ['as'=>'admin.plann.status_change','uses' => 'PlannController@status_change']);
       

        Route::get('cms-status-change/{id}/{status}', ['as'=>'admin.cms.status_change','uses' => 'CmsController@status_change']);
        
        Route::resource('sliders', 'SlidersController', ['names' => [
            'index' => 'admin.sliders',
            'create' => 'admin.sliders.create',
            'update' => 'admin.sliders.update',
            'edit' => 'admin.sliders.edit',
            'store' => 'admin.sliders.store',
            'show' => 'admin.sliders.show',
            'destroy' => 'admin.sliders.destroy',
        ]]);
        Route::get('slider-status-change/{id}/{status}', ['as'=>'admin.sliders.status_change','uses' => 'SlidersController@status_change']);

        Route::resource('history', 'HistoryController', ['names' => [
            'index' => 'admin.history',
        ]]);

        Route::get('user-history/{id}', ['as'=>'admin.user.history','uses' => 'HistoryController@user_history']);

        Route::resource('industry', 'IndustryController');
        Route::get('industry-status-change/{id}/{status}', ['as'=>'admin.industry.status_change','uses' => 'IndustryController@status_change']);

        Route::resource('product', 'ProductController');
 //Route::post('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
   
  // Route::get('forgot-password', ['as' => 'forgot_password', 'uses' => 'UsersController@forgotPassword']);
  // Route::post('forgot-password', ['as' => 'forgot_password', 'uses' => 'UsersController@sendPasswordLink']);
   


        Route::resource('price', 'PriceController', ['names' => [
            'index' => 'admin.price',
            'create' => 'admin.price.create',
            'update' => 'admin.price.update',
            'edit' => 'admin.price.edit',
            'store' => 'admin.price.store',
            'show' => 'admin.price.show',
            'destroy' => 'admin.price.destroy',
        ]]);

        Route::get('price-tags', ['as'=>'admin.price.tags','uses' => 'PriceController@price_tags']);


    });
});


/*Admin routes goes here*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {

    Route::group(['middleware' => 'IsNotAuthenticated'], function(){
        
        Route::get('login', ['as' => 'admin.login', 'uses' => 'AuthController@getlogin']);
        Route::post('login', ['as' => 'admin.login', 'uses' => 'AuthController@postLogin']);
        Route::post('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@getLogout']);
        
        Route::get('forgot-password', ['as' => 'admin.forgot_password', 'uses' => 'UsersController@forgotPassword']);
        Route::post('forgot-password', ['as' => 'admin.forgot_password', 'uses' => 'UsersController@sendPasswordLink']);
        
        Route::get('reset-password/{email_token}', ['as' => 'admin.reset_password', 'uses' => 'UsersController@resetPassword']);
        Route::post('reset-password/{email_token}', ['as' => 'admin.reset_password', 'uses' => 'UsersController@resetPasswordUpdate']);
    });

    Route::group(['middleware' => 'auth.admin'], function() {
        
        Route::any('dashboard/{r_id?}/{id?}', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

         Route::get('change-password', ['as' => 'admin.changepassword', 'uses' => 'UsersController@changepassword']);
        Route::post('change-password', ['as' => 'admin.updatepassword', 'uses' => 'UsersController@updatepassword']);
        
        Route::resource('roles', 'RoleController', ['names' => [
            'index' => 'admin.roles',
            'create' => 'admin.roles.create',
            'update' => 'admin.roles.update',
            'edit' => 'admin.roles.edit',
            'store' => 'admin.roles.store',
            'destroy' => 'admin.roles.destroy',
        ]]);

        Route::get('role-status-change/{id}/{status}', ['as'=>'admin.roles.status_change','uses' => 'RoleController@status_change']);

        Route::resource('users', 'UsersController', ['names' => [
            'index' => 'admin.users',
            'create' => 'admin.users.create',
            'update' => 'admin.users.update',
            'edit' => 'admin.users.edit',
            'store' => 'admin.users.store',
            'show' => 'admin.users.show',
            'destroy' => 'admin.users.destroy',
        ]]);
        Route::post('users/index', ['as' => 'admin.users.index', 'uses' => 'UsersController@index']);
        
        Route::get('references', ['as'=>'admin.user.references','uses' => 'UsersController@references']);
        Route::get('reference/{id}', ['as'=>'admin.user.reference','uses' => 'UsersController@reference']);


        Route::get('user-status-change/{id}/{status}', ['as'=>'admin.users.status_change','uses' => 'UsersController@status_change']);

        Route::resource('contactus', 'ContactusController', ['names' => [
            'index' => 'admin.contactus',
            'create' => 'admin.contactus.create',
            'update' => 'admin.contactus.update',
            'edit' => 'admin.contactus.edit',
            'store' => 'admin.contactus.store',
            'show' => 'admin.contactus.show',
            'destroy' => 'admin.contactus.destroy',
        ]]);

        Route::get('contactus-status-change/{id}/{status}', ['as'=>'admin.contactus.status_change','uses' => 'ContactusController@status_change']);

        Route::resource('emailtemplates', 'EmailtemplatesController', ['names' => [
            'index' => 'admin.emailtemplates',
            'create' => 'admin.emailtemplates.create',
            'update' => 'admin.emailtemplates.update',
            'edit' => 'admin.emailtemplates.edit',
            'store' => 'admin.emailtemplates.store',
            'show' => 'admin.emailtemplates.show',
            'destroy' => 'admin.emailtemplates.destroy',
        ]]);
        Route::get('emailtemplates-status-change/{id}/{status}', ['as'=>'admin.emailtemplates.status_change','uses' => 'EmailtemplatesController@status_change']);

        Route::resource('settings', 'SettingsController', ['names' => [
            'index' => 'admin.settings',
            'create' => 'admin.settings.create',
            'update' => 'admin.settings.update',
            'edit' => 'admin.settings.edit',
            'store' => 'admin.settings.store',
            'show' => 'admin.settings.show',
            'destroy' => 'admin.settings.destroy',
        ]]);

        Route::resource('cms', 'CmsController', ['names' => [
            'index' => 'admin.cms',
            'create' => 'admin.cms.create',
            'update' => 'admin.cms.update',
            'edit' => 'admin.cms.edit',
            'store' => 'admin.cms.store',
            'show' => 'admin.cms.show',
            'destroy' => 'admin.cms.destroy',
        ]]);

        //plann controller
        Route::resource('plann', 'PlannController', ['names' => [
            'index' => 'admin.plann',
            'create' => 'admin.plann.create',
            'update' => 'admin.plann.update',
            'edit' => 'admin.plann.edit',
            'store' => 'admin.plann.store',
            'show' => 'admin.plann.show',
            'destroy' => 'admin.plann.destroy',
        ]]);

        Route::get('plann-status-change/{id}/{status}', ['as'=>'admin.plann.status_change','uses' => 'PlannController@status_change']);
       

        Route::get('cms-status-change/{id}/{status}', ['as'=>'admin.cms.status_change','uses' => 'CmsController@status_change']);
        
        Route::resource('sliders', 'SlidersController', ['names' => [
            'index' => 'admin.sliders',
            'create' => 'admin.sliders.create',
            'update' => 'admin.sliders.update',
            'edit' => 'admin.sliders.edit',
            'store' => 'admin.sliders.store',
            'show' => 'admin.sliders.show',
            'destroy' => 'admin.sliders.destroy',
        ]]);
        Route::get('slider-status-change/{id}/{status}', ['as'=>'admin.sliders.status_change','uses' => 'SlidersController@status_change']);

        Route::resource('history', 'HistoryController', ['names' => [
            'index' => 'admin.history',
        ]]);

        Route::get('user-history/{id}', ['as'=>'admin.user.history','uses' => 'HistoryController@user_history']);

        Route::resource('industry', 'IndustryController');
        Route::get('industry-status-change/{id}/{status}', ['as'=>'admin.industry.status_change','uses' => 'IndustryController@status_change']);

        Route::resource('product', 'ProductController');
 //Route::post('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
   
  // Route::get('forgot-password', ['as' => 'forgot_password', 'uses' => 'UsersController@forgotPassword']);
  // Route::post('forgot-password', ['as' => 'forgot_password', 'uses' => 'UsersController@sendPasswordLink']);
   


        Route::resource('price', 'PriceController', ['names' => [
            'index' => 'admin.price',
            'create' => 'admin.price.create',
            'update' => 'admin.price.update',
            'edit' => 'admin.price.edit',
            'store' => 'admin.price.store',
            'show' => 'admin.price.show',
            'destroy' => 'admin.price.destroy',
        ]]);

        Route::get('price-tags', ['as'=>'admin.price.tags','uses' => 'PriceController@price_tags']);


    });
});

require base_path() . '/global_constants.php';







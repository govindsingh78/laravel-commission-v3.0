@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero"></h3>
                    <div class="example-box-wrapper">
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">User Name</th>
                                    <th width="15%">User Role</th>
                                    <th width="15%">Total References</th>
                                    <th width="20%">Refferal Amount</th>
                                    <th width="15%">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                            @if(!$users->isEmpty())
                                @php 
                                    $i=1;
                                    $amount = 5000; 
                                    $percentage = 20; 
                                @endphp

                                @foreach ($users as $user)
@php
    $roles = array('1'=>'Admin', '2'=>'Company', '3'=>'Sales Agent', '4'=>'Buyer');
    $referal_amt = BasicFunction::user_parent_earned($user->id);
@endphp
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$roles[$user->role_id]}}</td>
                                    <td>{{$user->total}}</td>
                                    <td>{{$referal_amt}}</td>
                                    <td>
                                        
                                    {!!  Html::decode(Html::link(route('admin.user.reference',['id'=>$user->id]),"<i class='glyphicon glyphicon-eye-open'></i>",['class'=>'btn btn-success','data-toggle'=>'tooltip','title'=>'View'])) !!}


                                    </td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>    
                        </table>
                        {!! $users->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
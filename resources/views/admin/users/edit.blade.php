@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
<div id="page-content">
<div class="container">
<section class="content-header with-border">
    @include('includes.admin.breadcrumb')
</section>
<div class="panel">
    <div class="panel-body">
        {!! Form::model($users,['method'=>'patch','route'=>['admin.'.$controller.'.update',$users->id]]) !!} 
                                <div class="form-group clearfix col-sm-6">
                                    {!! Form::label('First Name ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                    <div class="col-sm-12">
                                        {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>'First Name']) !!}    
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('first_name') }}</div>
                                </div>
                                <div class="form-group clearfix col-sm-6">
                                    {!! Form::label('Last Name ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                    <div class="col-sm-12">
                                        {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Last Name']) !!}    
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('last_name') }}</div>
                                </div>
                                <div class="form-group clearfix col-sm-6">
                                    {!! Form::label('Email ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                    <div class="col-sm-12">
                                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email']) !!}    
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('email') }}</div>
                                </div>
                                <div class="form-group clearfix col-sm-6">
                                    {!! Form::label('Phone No.',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                    <div class="col-sm-12">
                                        {!! Form::text('phone',null,['class'=>'form-control','placeholder'=>'Phone No.']) !!}    
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('phone') }}</div>
                                </div>

                                 <div class="form-group clearfix col-sm-6">
                                    {!! Form::label('Address',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                                    <div class="col-sm-12">
                                        {!! Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Address']) !!}    
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('address') }}</div>
                                </div>

                                @if($users['role_id'] != 1)
                                <div class="form-group col-md-6">
                                    {!! Form::label('Role ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::select('role_id', $allroles, null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('role_id') }}</div>
                                </div><!-- /.form-group -->
                                @endif

                                <div class="form-group col-md-6">
                                    {!! Form::label('Status',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!}
                                    <div class="col-sm-12">
                                        <?php $status_list = Config::get('global.status_list'); ?>
                                        {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                                    </div>
                                        <div class="col-sm-12 text-danger">{{ $errors->first('status') }}</div>
                                </div><!-- /.form-group -->
                                <div class="form-actions col-sm-12 center">
                                    <div class="col-sm-12">
                                    {{Form::button('<i class="fa fa-undo"></i> Cancel', array('type' => 'button', 'class'=> 'btn btn-default btn-sm'))}}
                                    {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                            {!! Form::close() !!}
    </div>
</div>
</div>
</div>
</div>

@endsection
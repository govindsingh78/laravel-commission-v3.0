@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">Product List</h3>
                    <div class="example-box-wrapper">
                        <div class="row">
                            <div class="size-md col-user-left">
                                {!!  Html::decode(Html::link(route('product.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
                            </div>
                            <div class="size-md col-user-right">
                            {!! Form::open(['method'=>'get','route'=>['product.index']]) !!}                         
                                <table class="search_table">
                                  <tbody>
                                    <tr>
                                      <td>{!! Form::text('search', '', ['class'=>'form-control']) !!}</td>
                                      <td><button class="btn btn-primary" type="submit">Search</button></td>
                                    </tr>
                                  </tbody>
                                </table>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">@sortablelink('name', 'Name')</th>
                                    <th width="15%">@sortablelink('title', 'Title')</th>
                                    <th width="15%">Price</th>
                                    <th width="15%">Updated At</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!$products->isEmpty())
                                @php $i=1; @endphp
                                @foreach ($products as $product)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{ ucfirst($product->name)}}</td>
                                    <td>{{ ucfirst($product->title)}}</td>
                                    <td>{{ $product->price}}</td>
                                    <td>
                                    {{date('m/d/Y', strtotime($product->updated_at))}}
                                    </td>
                                    <td>
                   
                    {!!  Html::decode(Html::link(route('product.edit', $product->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                    {!!  Html::decode(Html::link(route('product.destroy',['id' => $product->id]),"<i class='glyphicon glyphicon-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>'Delete', "data-alert"=>'Delete Product'])) !!}

                                    </td> 
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>    
                        </table>
                        {!! $products->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }

    .example-box-wrapper .col-user-right{
        float: right;
        margin-right: 10px;
    }
</style>
@endsection
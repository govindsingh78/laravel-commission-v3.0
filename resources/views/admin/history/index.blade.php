@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                    <div class="example-box-wrapper">
                        <div class="size-md">
                            {!! Form::open(['method'=>'get','route'=>['admin.history']]) !!}                         
                            <table class="search_table">
                              <tbody>
                                <tr>
                                  <td>{!! Form::text('search', '', ['class'=>'form-control']) !!}</td>
                                  <td>
                                        <button class="btn btn-primary btn-search" type="submit">Search</button>
                                    </td>
                                </tr>
                              </tbody>
                            </table>
                            {!! Form::close() !!}
                        </div>
                        
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">@sortablelink('first', 'User Name')</th>
                                    <th width="15%">Visited As</th>
                                    <th width="20%">@sortablelink('ip_address', 'IP Address')</th>
                                    <th width="15%">@sortablelink('created_at', 'Login Time')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!$histories->isEmpty())
                                @php $i=1; @endphp
                                @foreach ($histories as $history)
<?php
    $roles = array('1'=>'Admin', '2'=>'Company', '3'=>'Sales Agent', '4'=>'Buyer');
?>
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{ ucfirst($history->user->first_name.' '.$history->user->last_name)}}</td>
                                    <td>{{$roles[$history->user->role_id]}}</td>
                                    <td>{{ $history->ip_address}}</td>
                                    <td>{{ date('m/d/Y h:i A', strtotime($history->created_at))}}</td> 
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>    
                        </table>
                        {!! $histories->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
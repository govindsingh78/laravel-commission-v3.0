    
@extends('layouts.blank')

@section('content')  

<style type="text/css">

    html,body {
        height: 100%;
        background: #fff;
        overflow: hidden;
    }

</style>


{!! Html::script(asset('public/admin/assets/widgets/wow/wow.js')) !!}
<script type="text/javascript">
    /* WOW animations */

    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>


<img src="{{asset('public/admin/assets/image-resources/blurred-bg/blurred-bg-3.jpg')}}" class="login-img wow fadeIn" alt="">

<div class="center-vertical">
    <div class="center-content row">

        <div class="col-md-3 center-margin">

            <form method="post" action="">
                <div class="content-box wow bounceInDown modal-content">
                    <h3 class="content-box-header content-box-header-alt bg-default">
                        <span class="icon-separator">
                            <i class="glyph-icon icon-cog"></i>
                        </span>
                        <span class="header-wrapper">
                            Admin area
                            <small>Login to your account.</small>
                        </span>
                    </h3>
                    
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('error'))
                              <div class="alert alert-danger">
                                  {{ session('error') }}
                              </div>
                            @endif
                              @if (session('success'))
                                  <div class="alert alert-success">
                                      {{ session('success') }}
                                  </div>
                              @endif
                        </div>
                    </div>

                     {!! Form::open(array('url' =>  URL::to('admin/login'))) !!}
                        <div class="content-box-wrapper">
                            <div class="form-group">
                                <div class="input-group">
                                    
                                    {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Enter email']) !!}
                                    <span class="input-group-addon bg-blue">
                                        <i class="glyph-icon icon-envelope-o"></i>
                                    </span>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                             <div class="error">{{ $errors->first('email') }}</div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                    <span class="input-group-addon bg-blue">
                                        <i class="glyph-icon icon-unlock-alt"></i>
                                    </span>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                              <div class="error">{{ $errors->first('password') }}</div>
                            </div>
                            <div class="form-group">
                                {!! Html::link(route('admin.forgot_password'), 'Forgot Password', array('class' => '')) !!}
                            </div>
                           
                             {!! Form::submit('Sign In',['class'=>'btn btn-success btn-block'])!!}
                        </div>
                      {!! Form::close() !!}  
                    </div>
            </form>
        </div>

    </div>
</div>


@stop

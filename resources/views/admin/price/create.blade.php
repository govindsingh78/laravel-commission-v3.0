@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                
                    {!! Form::open(['route'=> 'admin.price.store', 'id'=>'form_price']) !!} 
                        
                        <div class="row">
                            <div class="form-group clearfix col-sm-6">
                                {!! Form::label('Price Name ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Name']) !!}    
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('name') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group clearfix col-sm-6">
                                {!! Form::label('Price Type ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                <div id="pricebox" class="col-sm-12">
                                    <label class="radio-inline">
{!! Form::radio('type', '0', true, ['id'=>'free'])!!}Free
                                    </label>
                                    <label class="radio-inline">
{!! Form::radio('type', '1', false, ['id'=>'paid'])!!}Premium
                                    </label>
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('type') }}</div>
                            </div>
                        </div>

                        <div id="pricepaid" class="row" style="display: none;">
                            <div class="form-group clearfix col-sm-6">
                                {!! Form::label('Price Per Month ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('per_month',null,['id'=>'per_month','class'=>'form-control','placeholder'=>'Per Month']) !!}    
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('per_month') }}</div>
                            </div>
                            <div class="form-group clearfix col-sm-6">
                                {!! Form::label('Price Per Year ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('per_year',null,['id'=>'per_year','class'=>'form-control','placeholder'=>'Per Year']) !!}    
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('per_year') }}</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group clearfix col-sm-6">
                                    <table class="form-group clearfix" id="tbl_tags">
                                        <tr>
                                            <td colspan="2">
                                                {!! Form::label('Price Tags ',null,['class'=>'control-label no-padding-right']) !!} 
                                            </td>
                                        </tr>  
                                        <tr class="input_gap">  
                                            <td>
                                                <input type="text" name="tags[]" placeholder="Enter tag name" class="form-control" />
                                            </td>  
                                            <td>
                                                <button type="button" name="add" id="btn_more" class="btn btn-success btn-add-more">Add More</button>
                                            </td>  
                                        </tr>  
                                    </table>         
                                </div>  
                            </div>  
                        </div>

                        <div class="row">
                            <div class="form-actions col-sm-12 center">
                            <div class="col-sm-12">
                                
                                <a href="{{route('admin.price')}}" class="btn btn-default btn-sm">Cancel</a>
                                
                                {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'button', 'id'=>'btn_save', 'class'=> 'btn btn-success btn-sm' ))}}
                            </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .btn-add-more, .btn_remove{
        display: inline-block;
        margin-left: 6px;
    }
    .input_gap {
        width: 100%;
        display: inline-block;
        margin-bottom: 10px;
    }
    
</style>
<script type="text/javascript">
$(document).ready(function(e){

    $(document).on('click', '#btn_save', function(){
        if(!$('#form_price').valid()){
            return false;
        }else{
            $('#form_price').submit();
        }
    });


    $('#btn_reset').click(function(){
        $('#form_price')[0].reset();
    });

    $(document).on('click', '#pricebox input' , function(){
        var $this = $(this);
        $('#per_month, #per_year').val('');
        if($this.val() == '0'){
          $('#pricepaid').hide();
        }else{
           $('#pricepaid').show();
        }
    });

    var i=1; 

    $('#btn_more').click(function(){  
       i++;  
       $('#tbl_tags').append('<tr id="row'+i+'" class="input_gap"><td><input type="text" name="tags[]" placeholder="Enter tag name" class="form-control" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
    });

    $(document).on('click', '.btn_remove', function(){  
        var button_id = $(this).attr("id");   
        $('#row'+button_id+'').remove();  
    });

    $('#form_price').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error',
        focusInvalid: false,
        rules: {
            name: {
                required: true
            },
            per_month: {
                required: "#paid:checked",
            },
            per_year: {
                required: "#paid:checked",
            },
        },
        messages: {
            name: {
                required: "Name is required"
            },
            per_month: {
                required: "Per month price is required"
            },
            per_year: {
                required: "Per year price is required"
            },
        }
    });  

});
</script>

@endsection
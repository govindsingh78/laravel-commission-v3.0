@extends('layouts.admin')

@section('content') 
 <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

<!-- Ckeditor -->

{!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!}

<script>

    // This code is generally not necessary, but it is here to demonstrate
    // how to customize specific editor instances on the fly. This fits well
    // this demo because we have editable elements (like headers) that
    // require less features.

    // The "instanceCreated" event is fired for every editor instance created.
    CKEDITOR.on( 'instanceCreated', function( event ) {
        var editor = event.editor,
                element = editor.element;

        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                'forms,iframe,image,newpage,removeformat,' +
                'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }
    });
</script>
<section class="content-header with-border">
    @include('includes.admin.breadcrumb')
</section>
<div class="panel">
    <div class="panel-body">
        {!! Form::model($cms,['method'=>'patch', 'files' => true, 'route'=>['admin.'.$controller.'.update',$cms->id]]) !!} 
            <div class="form-group clearfix col-sm-6">
                {!! Form::label('Title ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                <div class="col-sm-12">
                    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Title']) !!}    
                </div>
                <div class="col-sm-12 text-danger">{{ $errors->first('title') }}</div>
            </div>
        
            <div class="form-group clearfix col-sm-6">
                {!! Form::label('Parent CMS',null,['class'=>'col-sm-6 control-label no-padding-right']) !!}
                <div class="col-sm-12">
                    {!! Form::select('parent_id', $per_cms, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        
            <div class="form-group clearfix col-sm-12">
                {!! Form::label('Description ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                <div class="col-sm-12">
                    {!! Form::textarea('description',null,['class'=>'col-xs-12 col-sm-12 ckeditor','placeholder'=>'Description']) !!}   
                </div>
                <div class="col-sm-12 text-danger">{{ $errors->first('description') }}</div>
            </div>      

            <div class="form-group clearfix col-sm-6">
                {!! Form::label('Meta Keywords ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                <div class="col-sm-12">
                    {!! Form::textarea('meta_keywords',null,['class'=>'col-xs-12 col-sm-12 ','placeholder'=>'Description']) !!} 
                </div>
                <div class="col-sm-12 text-danger">{{ $errors->first('meta_keywords') }}</div>
            </div>  

            <div class="form-group clearfix col-sm-6">
                {!! Form::label('Meta Description ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                <div class="col-sm-12">
                    {!! Form::textarea('meta_description',null,['class'=>'col-xs-12 col-sm-12 ','placeholder'=>'Description']) !!}  
                </div>
                <div class="col-sm-12 text-danger">{{ $errors->first('meta_description') }}</div>
            </div>  

            <div class="form-group clearfix col-sm-6">
                {!! Form::label('Meta Title ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                <div class="col-sm-12">
                    {!! Form::text('meta_title',null,['class'=>'col-xs-12 col-sm-12','placeholder'=>'Meta Title']) !!} 
                </div>
                <div class="col-sm-12 text-danger">{{ $errors->first('meta_title') }}</div>
            </div>  
        
            <div class="form-group col-md-6">
                {!! Form::label('Status',null,['class'=>'required_label']) !!}
                <div class="col-sm-12">
                    <?php $status_list = Config::get('global.status_list'); ?>
                    {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-12 text-danger">{{ $errors->first('status') }}</div>
            </div>
        
            <div class="form-group clearfix col-md-6">
                {!! Form::label('Image',null,['class'=>'col-sm-6 control-label no-padding-right']) !!}
                <div class="col-sm-12">
                    {!! Form::file('cms_image', ['class' => 'form-control', 'id' => 'prev_img']) !!}
                    <img id="show_img" rel="{{$cms['cms_image']}}" src="{{asset(CMS_IMAGES_URL . $cms['cms_image'])}}" alt="your image" style="height: 100px; margin: 15px 0 0 10px;"/>
                </div>
            </div>
        
            <!-- /.form-group -->
            <div class="form-actions col-sm-12 center">
                <div class="col-sm-12">
                {{Form::button('<i class="fa fa-undo"></i> Reset', array('type' => 'reset', 'class'=> 'btn btn-default btn-sm'))}}
                {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                </div>
                <div class="clearfix"></div>
            </div>

        {!! Form::close() !!}
    </div>
</div>
</div>
</div>
     
<script>
    $(document).ready(function(){
        var src = $('#show_img').attr('rel');
        if(!src){
            $('#show_img').addClass('hide');
        }
    });
    
    $("#prev_img").change(function() {
        readURL(this);
        $('#show_img').removeClass('hide');
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#show_img').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };
</script>
     
</div>

@endsection
@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">

            <!-- Ckeditor -->
            {!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!}

            <script>
                // This code is generally not necessary, but it is here to demonstrate
                // how to customize specific editor instances on the fly. This fits well
                // this demo because we have editable elements (like headers) that
                // require less features.
                // The "instanceCreated" event is fired for every editor instance created.
                CKEDITOR.on( 'instanceCreated', function( event ) {
                    var editor = event.editor,
                        element = editor.element;

                    // Customize editors for headers and tag list.
                    // These editors don't need features like smileys, templates, iframes etc.
                    if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
                        // Customize the editor configurations on "configLoaded" event,
                        // which is fired after the configuration file loading and
                        // execution. This makes it possible to change the
                        // configurations before the editor initialization takes place.
                        editor.on( 'configLoaded', function() {

                            // Remove unnecessary plugins to make the editor simpler.
                            editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                            'forms,iframe,image,newpage,removeformat,' +
                            'smiley,specialchar,stylescombo,templates';

                            // Rearrange the layout of the toolbar.
                            editor.config.toolbarGroups = [
                                { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                                { name: 'undo' },
                                { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                                { name: 'about' }
                            ];
                        });
                    }
                });
            </script>
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">

 
                    {!! Form::open(['route'=> 'admin.'.$controller.'.store', 'files' => false, 'id'=>'form_plann']) !!} 
                        <div class="form-group clearfix col-sm-6">
                            {!! Form::label('Plann Name',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('plann_name','',['onkeyup'=>"showSlugName(this.value);",'id'=>'slugName','class'=>'form-control','placeholder'=>'Plann Name']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('plann_name') }}</div>
                        </div>

                        <div class="form-group clearfix col-sm-6" style="display: none">
                            {!! Form::label('Slug',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('slug_name',null,['class'=>'form-control','placeholder'=>'Generated Slug', 'readonly'=>'readonly']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('slug_name') }}</div>
                        </div>

                        <div class="form-group clearfix col-sm-6">
                            {!! Form::label('Price',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('price',null,['class'=>'form-control','placeholder'=>'Plann Name']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('price') }}</div>
                        </div>
                        <div class="form-group clearfix col-sm-6">
                            {!! Form::label('Feature Name',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('feature_name',null,['class'=>'form-control','placeholder'=>'Feature Name']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('feature_name') }}</div>
                        </div> 

                        <div class="form-group clearfix col-md-6">
                            {!! Form::label('Status',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!}
                            <div class="col-sm-12">
                                <?php $status_list = Config::get('global.status_list'); ?>
                                {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('status') }}</div>
                        </div>


                        <div class="form-group clearfix col-md-6">
                            {!! Form::label('Sort Order',null,['class'=>'col-sm-6 control-label no-padding-right']) !!}
                            <div class="col-sm-12">
                            <?php
                              $selected_elem=(count($plann) - 1);
                            ?>
                            <select class="form-control" name="sort_order">
                           
                            <?php 
                            $i=1;
                            while($i <= count($plann)){
                            ?>
                            <option value="<?php echo $i; ?>" selected="<?php if($i==$selected_elem){ echo "selected"; } ?>"><?=$i;?></option>
                            <?php
                            $i++;
                            }
                            ?>
                            </select>
                            
                            </div>
                        </div>

                         

                        <!-- /.form-group -->
                        <div class="form-actions col-sm-12 center">
                            <div class="col-sm-12">
                            {{Form::button('<i class="fa fa-undo"></i> Reset', array('type' => 'reset', 'class'=> 'btn btn-default btn-sm'))}}
                            {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ,'id' => 'btn_reset'))}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<script>

$(document).ready(function(e){
    $('#btn_reset').click(function(){
        $('#form_cms')[0].reset();
    });            
});


        $("#prev_img").change(function() {
            readURL(this);
            $('#show_img').removeClass('hide');
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#show_img').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        function showSlugName(slugValue)
        {
            console.log(slugValue);
        }
    </script>
</div>
@endsection


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	@include('includes.front.head')
</head>
<body>
	<div id="page-wraper">
		@include('includes.front.header')
		<div class="container">
            <div class="row">
                <div class="col-lg-8">
                	@foreach($page_data as $page)
                        <h2>{{$page->title}}</h2>
                        <hr></hr>
                        <hr></hr>
                        <p>{!!html_entity_decode($page->description)!!}</p>
                    @endforeach
                </div>
                @include('includes.front.latest_products')
        	</div>
        </div>
    </div>
    	@include('includes.front.footer_scripts')
</body>

</html>
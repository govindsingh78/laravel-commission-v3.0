<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.front.head')

<link type="text/css" href="{{ URL::asset('public/css/front/menu/theme-styles.css')}}" rel="stylesheet">
</head>

<body>
    <div class="header-landing">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5"> Btibcookislands
                </div>
                <div class="col-xl-7 col-lg-7 col-md-7">
                    <div id="site-header-landing">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 col-lg-7 col-md-7">
                                    <div id="site-header-landing" class="header-landing">
                                        <ul class="profile-menu">
                                            @foreach($cms_pages as $page)
                                            <li> <a href="{{route('page_details')}}/{{$page->slug}}">{{$page->title}}</a> </li>
                                            @endforeach
                                            
                                            <li>
                                                <a href="#" class="js-expanded-menu"> <i class="fa fa-bars olymp-menu-icon" aria-hidden="true"></i> <i class="fa fa-times olymp-close-icon" aria-hidden="true"></i></a> </li>
            
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="jumbotron text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-7 text-left">
        <h1 class="jumbotron-heading">Dream click share</h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore 
          magna aliqua. Can consectetur adipi scing sed do eiusmod tempor incididunt ut labore et dolore magna aliqua 
          laboris nisi ut aliquip  ea commodo consequ aute irure dolor.</p>
        <p> <a href="javascript:registerNow();" id="register_now" class="btn register_now">Register Now!</a>
        </p>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4" style="background: #fff ; border-radius: 0 5px 5px 0px ;margin-top: 50px;"> 
        <!--<div class="col-md-5">-->
            @if(Session::has('SuccessSignUp'))
                   Registration Successfull, Login To Your Account.
            @endif
        <div id="panel">
          <div id="tabbox"> <a href="javascript:void(0)" id="signup" class="tab signup "><i class="fa fa-user" aria-hidden="true"></i> </a> <a href="javascript:void(0)" id="login" class="tab select"><i class="fa fa-sign-out" aria-hidden="true"></i> </a> </div>
           
                                    <div class="poz-set">
                                        <div id="signupbox">
                                            {{Form::open(array('url'=>'login','method'=>'post'))}}
                                            <div class="top-row">
                                                <h1>Log In</h1>
                                                <div class="red" style="display:block;">
                                                    <div class="field-wrap">
                                                        <input required="" placeholder="Your Email" name="email" autocomplete="off" type="text">
                                                        <input required="" placeholder="Your Password" autocomplete="off" type="password" name="password">
                                                    </div>
                                                    <div class="rem_for">
                                                        <label>
                                                            <input type="checkbox" name="remember" value="Rememberme"> Remember me</label>
                                                        <label class="text-right">

                                                            <a href="{{route('resetPassword')}}">Forgot my Password?</a></label>
                                                    </div>
                                                    <!-- <label>
                                                        <div class="clearfix"></div>
                                                        <input type="radio" name="colorRadio" value="red" checked> User 1</label>
                                                    <label>
                                                        <input type="radio" name="colorRadio" value="green"> User 2</label> -->


                                                    <button type="submit" class="log_but">Log In</button>
                                                    <div class="orimg"><img src="{{URL::asset('public/img/or_back.png')}}" class="" alt="" />
                                                    </div>
                                                    <a href="{{route('login')}}/facebook"  class="facebook_but btn btn-primary"><i class="fa fa-facebook" aria-hidden="true"></i> Login with Facebook</a>
                                                    <a class="twitter_but btn btn-info btn-lg" href="{{route('login')}}/google"><i class="fa fa-twitter" aria-hidden="true"></i> Login with Twitter</a>
                                                    <p>Don’t have an account? <span><a href="javascript:registerNow();">Register Now!</a></span> it’s really simple and you can start enjoing all the benefits!</p>
                                                </div>
                                            </div>
                                            {{Form::close()}}
                                        </div>
                                        <div id="loginbox">
                                            
                                            <div class="top-row">
                                                <h1>Sign Up</h1>
                                            </div>
                                            <label>
                                                <input type="radio" name="colorRadio" value="red" checked> User
                                            </label>
                                            <label>
                                                <input type="radio" name="colorRadio" value="green"> Company
                                            </label>
                                            {{Form::open(array('url'=>'register','method'=>'post'))}}
                                            <div class="red red_btn" style="display:block;">
                                                <div class="top-row"> </div>
                                                <div class="field-wrap">
                                                    <input required="" placeholder="Full Name" name="first_name" autocomplete="off" type="text">
                                                    <input required="" placeholder="Email ID" tooltip="" name="email" autocomplete="off" type="email"><span>
                                                    @if(isset($errors))
                                                        @if($errors->has('email'))
                                                            {{$errors->first('email')}}
                                                        @endif
                                                    </span>
                                                    <input required="" placeholder="Password" name="password" autocomplete="off" type="password">
                                                    <input required="" placeholder="Retype Password" name="password_confirmation" autocomplete="off" type="password">
                                                    <span>
                                                        @if($errors->has('password'))
                                                            {{$errors->first('password')}}
                                                        @endif
                                                    </span>
                                                    @endif
                                                    <div class="input-group">
                                                        <div class="input-group-addon"> <i class="fa fa-calendar"> </i> </div>
                                                        <input class="form-control" id="date" name="dob" required onkeypress="return false;" format="DD/MM/YYYY" type="date" />
                                                    </div>
                                                    <div class="gender">
                                                        <h1>Gender</h1>

                                                        <label> Male
                                                            <input type="radio" value='male' name="gender" />
                                                        </label>
                                                        <label>Female
                                                            <input type="radio" value='female' name="gender" />
                                                        </label>
                                                        <label>Other
                                                            <input type="radio" value='other' name="gender" />
                                                        </label>

                                                    </div>
                                                    <input required="" placeholder="Phone number" onkeypress="return isNumberKey(event);" name="phone" autocomplete="off" type="text" maxlength="10" minlength="10">
                                                    <div class="form-group">
                                                        <select class="form-control" id="exampleFormControlSelect1">
                                                            <option>Location</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                    <input type="hidden" name="role_id" value="11"/>
                                                    <input required="" placeholder="Location" autocomplete="off" type="text">
                                                    <button type="submit" class="log_but">Sign Up</button>
                                                </div>
                                            </div>
                                            {{Form::close()}}
                                            <div class="green red_btn" style="display:none;">
                                            {{Form::open(array('url'=>'register','method'=>'post'))}}
                                                <div class="top-row">
                                                <input type="hidden" name="role_id" value="10"/>
                                                    <div class="field-wrap">
                                                        <input required="" placeholder="Company Name" name="company_name" autocomplete="off" type="text">
                                                    </div>
                                                    <div class="field-wrap">
                                                        <input required="" placeholder="Full Name" name="first_name" autocomplete="off" type="text">
                                                    </div>
                                                    <div class="field-wrap">
                                                        <input required="" placeholder="Company website URL" name="company_url" autocomplete="off" type="text">
                                                    </div>
                                                    <div class="field-wrap">
                                                        <input required="" placeholder="Email ID" name="email" autocomplete="off" type="email">
                                                    </div>
                                                    <div class="field-wrap"> 
                                                        <input required="" placeholder="Password" name="password" autocomplete="off" type="password">
                                                    </div>
                                                    <div class="field-wrap">
                                                        <input required="" placeholder="Phone number" maxlength="10" minlength='10' name="phone" onkeypress="return isNumberKey(event);" autocomplete="off" type="tel">
                                                    </div>
                                                </div>
                                                <button type="submit" class="log_but">Sign Up</button>
                                            
                                            
                                            <p>Already have an account? <span><a href="javascript:loginNow();">Log In Now!</a></span>
                                            </p>
                                            {{Form::close()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </section>
                    @include('includes.front.landing_features_users')
                    <footer class="text-muted">
                        <div class="container">
                            <p class="text-center">Copyright © 2016 Vouch. All rights reserved.</p>
                        </div>
                    </footer>
                    <!-- Bootstrap core JavaScript
    ================================================== -->
                    <!-- Placed at the end of the document so the pages load faster -->
                    <script src="{{ URL::asset('public/js/jquery-1.11.2.min.js')}}"></script>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
                    <script>
                        $(function() {
                            Holder.addTheme("thumb", {
                                background: "#55595c",
                                foreground: "#eceeef",
                                text: "Thumbnail"
                            });
                        });
                    </script>
                    @if(Session::has('admin-alert-error'))
                    <script type="text/javascript">
                        toastr.warning('Admin Can Not Login As User!','Login Error');
                    </script>
                    @endif
                     @if(Session::has('alert-error'))
                    <script type="text/javascript">
                        toastr.error("{{Session::get('alert-error')}}",'Login Error');
                    </script>
                    @endif
                    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

                    <!-- jQuery first, then Other JS. -->
                    <!-- Js effects for material design. + Tooltips -->
                    <!-- Helper scripts (Tabs, Equal height, Scrollbar, etc) -->
                    <script src="{{ URL::asset('public/js/menu/theme-plugins.js')}}"></script>
                    <!-- Init functions -->
                    <script src="{{ URL::asset('public/js/menu/main.js')}}"></script>
                    <script src="{{ URL::asset('public/js/bootstrap.min.js')}}"></script>
                    <script src="{{ URL::asset('public/js/ie10-viewport-bug-workaround.js')}}"></script>
                    <script type="text/javascript" src="{{ URL::asset('public/js/date_pick/bootstrap-datepicker.min.js')}}"></script>
                    <script type="application/javascript">
                        
                             $(document).on('click','.tab',function(){
                                var X = $(this).attr('id');
                                if (X == 'signup') {
                                    $("#login").removeClass('select');
                                    $("#signup").addClass('select');
                                    $("#loginbox").slideUp();
                                    $("#signupbox").slideDown();
                                } else {
                                    $("#signup").removeClass('select');
                                    $("#login").addClass('select');
                                    $("#signupbox").slideUp();
                                    $("#loginbox").slideDown();
                                }
                            });
                            function registerNow()
                            {
                                    $("#signup").removeClass('select');
                                    $("#login").addClass('select');
                                    $("#signupbox").slideUp();
                                    $("#loginbox").slideDown();
                            }
                            function loginNow()
                            {
                                    $("#login").removeClass('select');
                                    $("#signup").addClass('select');
                                    $("#loginbox").slideUp();
                                    $("#signupbox").slideDown();
                            }
                    </script>
                    <script type="text/javascript">
                         (function($)  {
                            //$('input[type="radio"]').click(function(){
                            $("input:radio[name=colorRadio]").click(function() {
                                var inputValue = $(this).attr("value");
                                var targetBox = $("." + inputValue);
                                $(".red_btn").not(targetBox).hide();
                                $(targetBox).show();
                            });
                            $()
                        })(jQuery);

                        (function($) {
                            var date_input = $('input[name="dob"]'); //our date input has the name "date"
                            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                            date_input.datepicker({
                                format: 'dd/mm/yyyy',
                                container: container,
                                todayHighlight: true,
                                autoclose: true,
                            })
                        })(jQuery);
                    </script>

<script>
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

</body>

</html>
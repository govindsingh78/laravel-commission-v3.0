@extends('layouts.default')
@section('content')
<div class="clearfix"></div>
   <div class="chating_cover">
  <div class="container">
  <div class="row">
  <div class="col-md-2">
  <div class="profile_pick"><img src="{{ URL::asset('public/img/profile_pick.png')}}" alt=""></div></div>
  <div class="col-md-10">
  <div class="user_details"><h1>Kathryn Rodriguez</h1>
  <h3><i class="fa fa-map-marker" aria-hidden="true"></i>Canada</h3>
  <h3><i class="fa fa-envelope" aria-hidden="true"></i>patricia@example.com</h3>
  <h2><i class="fa fa-phone" aria-hidden="true"></i>+91-+91-0987654321</h2>
  </div>
  <div class="icon_button"><i class="fa fa-envelope" aria-hidden="true"></i>Message (16)</div>
  </div>
  </div>
  </div>
  <div class="clearfix"></div>
  </div>
 <div class="main_section_1">
   <div class="container">
      <div class="row">
         <div class="col-lg-3 ">
<div class="friends_chat">
<form id="custom-search-form" class="form-search search_box form-horizontal pull-right">
                    <div class="border-se"><input type="text" class="search-query" placeholder="Search"><button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i>
</button></div>
               
            </form>
            <div class="clearfix"></div>
<div class="heding-de">Friends (206)
</div>
<div class="panel-group">
          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_1.jpg')}}')}}" alt=""></i>
Ariana Romanescu <div class="online"></div></a>
                        </h4>

 
          
            </div>
<div class="panel-group">
                <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_2.jpg')}}" alt=""></i>
Amit Pawar <!--<div class="online"></div>--></a>
                        </h4>
            </div>
<div class="panel-group">
  <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_3.jpg')}}" alt=""></i>
Zoe Weber <div class="online"></div></a>
                        </h4>
            </div>
<div class="panel-group">
  <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_4.jpg')}}" alt=""></i>
Rasmus Sild <div class="online"></div></a>
                        </h4>
            </div>
<div class="panel-group">
                    <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_5.jpg')}}" alt=""></i>
Semenica Onciu<!--<div class="online"></div>--></a>
                        </h4>
            </div>
<div class="panel-group">
<h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_6.jpg')}}" alt=""></i>
Catherine Burton <div class="online"></div></a>
                        </h4>
            </div>
<div class="panel-group">
   <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_7.jpg')}}" alt=""></i>
Carla Vianu <div class="online"></div></a>
                        </h4>
            </div>
<div class="panel-group">
      <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_1.jpg')}}" alt=""></i>
Ariana Romanescu <div class="online"></div></a>
                        </h4>
            </div>
<div class="panel-group">
       <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{ URL::asset('public/img/friend_2.jpg')}}" alt=""></i>
Amit Pawar <!--<div class="online"></div>--></a>
                        </h4>
          
            </div>

            
            
            
            
            </div></div>
         <!--chat_sidebar-->
         <div class="col-sm-9 message_section">
		 <div class="row">
		 <div class="new_message_head">
		 Patricia Busta
         </div><!--new_message_head-->
		 
		 <div class="chat_area">
		 <ul class="list-unstyled">
		 <li class="left clearfix">
                     <span class="chat-img1 pull-left">
                     <img src="{{ URL::asset('public/img/friend_2.jpg')}}" alt=""/> </span>
                     <div class="chat-body1 clearfix">
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
						<div class="chat_time pull-right">09:40PM</div>
                     </div>
                  </li>
                  <li class="left clearfix admin_chat">
                     <span class="chat-img1 pull-right">
                     <img src="{{ URL::asset('public/img/friend_3.jpg')}}" alt=""/> </span>
                     <div class="chat-body1 clearfix">
                        <p class="chat_col">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
    					<div class="chat_time pull-left">09:40PM</div>
                     </div>
                  </li>
				  
				
		 
		 
		 </ul>
		 </div><!--chat_area-->
          <div class="message_write">
    	 <textarea class="form-control" placeholder="type a message"></textarea>
		 <div class="clearfix"></div>
		 <div class="chat_bottom"><a href="#" class="pull-left upload_btn"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
 Add Files</a>
 <a href="#" class="pull-right btn btn-success">
 Send</a></div>
		 </div>
		 </div>
         </div>
         <!--message_section-->
      </div>
   </div>
</div>
@stop
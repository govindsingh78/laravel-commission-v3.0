@extends('layouts.default')
@section('content')
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
  <div class="slider_image"><img src="{{URL::asset('public/img/wish_list.png')}}" alt="image"/></div>
 
<div class="container">
  <div class="row">

        <div class="col-lg-12">
        <aside class="wish_list_back">
          <div class="wishlist_hedding">My Wish List</div>
          <div class="inner_product pad_div"><div class="product wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
            @foreach($wishlist as $product)
            <div class="brand"> <img src="{{URL::asset('public/stuff/voucher_image')}}/{{$product['product']['image']}}" alt="image">
              <div class="clearfix"></div>
              <a href="javascript:void(0);" class="addToCart" product_type="{{$product['product_type']}}" product_id="{{$product['product_id']}}"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="javascript:void(0);"  product_id="{{$product['product_id']}}" class="removeWishlist"><i class="fa fa-times" aria-hidden="true"></i> </a> </div>
            @endforeach
            <div class="clearfix"></div>
          </div></div>
        </aside></div>
    </div>
 
 </div>
@stop
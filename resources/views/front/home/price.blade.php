<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Pricing </title>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/custom-style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/side-navbar.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap-select.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/font-awesome.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/normalize.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/normalize.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery.magicsearch.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/responsive.css')}}">
        <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script> -->
            
     </head>
    <body>
        <header>
            <div class="header_part">
               <div class="header_down">
                    <div class="container-fluid">
                           <div class="k-new-navbar-left">
                                <a class="k-new-navbar-logo" href="#">
                                    <img src="images/logo.jpg" alt="logo" class="img-responsive">
                                </a>
                            </div>
 
                        <div class="k-new-navbar " data-behavior="LoggedInNavbar ToggleClassWhenScrolling " data-login-details="#" data-scroll-class="k-new-navbar--scrolled" data-scroll-class-behavior="add" data-scroll-offset="5">
                            <div class="k-new-navbar-right" >
                                <div class="other_nav">
                                        <div class="k-new-navbar-item-container dropdown men">
                                            <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">
                                                Companies<span class="caret"></span>
                                            </a>
                                               <ul class="dropdown-menu">
                                                    <li class="men"><a href="">Pricing</a></li>
                                                    <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">xx</a>
                                                        <ul class="dropdown-menu">
                                                            <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" -toggle="dropdown">XX.1</a>
                                                            </li><li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">XX.2</a>
                                                                <ul class="dropdown-menu">
                                                                <li class="men"><a href="">XX.2.1</a></li>
                                                                <li class="men"><a href="">XX.2.2</a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    
                                                </ul>
                                        </div> 
                                        <div class="k-new-navbar-item-container dropdown men">
                                            <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">Sales Agents
                                                <span class="caret"></span>
                                            </a>
                                               <ul class="dropdown-menu">
                                                  <li class="men"><a href="#">Opportunities</a></li>
                                                  <li class="men"><a href="#">Training</a></li>
                                               </ul>
                                        </div>
                                         <div class="k-new-navbar-item-container dropdown men">
                                            <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">How it works
                                             <span class="caret"></span></a>
                                               <ul class="dropdown-menu">
                                                  <li class="men"><a href="#">Companies</a></li>
                                                  <li class="men"><a href="#">Sales Agents</a></li>
                                                  <li class="men"><a href="#">Buy Easy</a></li>
                                                  <li class="men"><a href="#">Refer and Earn</a></li>
                                                  <li class="men"><a href="#">Loyalty Points</a></li>
                                               </ul>
                                        </div>
                                        <div class="k-new-navbar-item-container"><a class="k-new-navbar-item" href="#">Contact Us</a></div>

                                        <div class="k-new-navbar-item-container">
                                            <a class="k-new-navbar-item-login" href="javascript:void(0)" data-toggle="modal" data-target="#loginmodel">Login</a>
                                        </div>
                                      

                                        <div class="k-new-navbar-item-container">
                                            <a class="k-new-navbar-item-store" href="javascript:void(0)" data-toggle="modal" data-target="#loginmodel">Sign Up</a>
                                        </div>
                                </div>
                                <div class="k-new-navbar-item-container k-new-navbar-item-container--last">
                                    <a class="k-new-navbar-item-menu" data-behavior="ExpandNewNavbar" href="#"></a>
                                    <div class="k-new-navbar-item-stack" data-behavior="ExpandNewNavbar">
                                        <div class="k-new-navbar-item-stack-top"></div>
                                        <div class="k-new-navbar-item-stack-middle"></div>
                                        <div class="k-new-navbar-item-stack-bottom"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="k-new-navbar-side-bar">
                                    <div class="upper_nav">
                                        <div class="k-new-navbar-right">
                                                <div class="k-new-navbar-item-container dropdown men">
                                                    <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">
                                                        Companies<span class="caret"></span>
                                                    </a>
                                                       <ul class="dropdown-menu">
                                                            <li class="men"><a href="/pricing">Pricing</a></li>
                                                            <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">xx</a>
                                                                <ul class="dropdown-menu">
                                                                    <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" -toggle="dropdown">XX.1</a>
                                                                    </li><li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">XX.2</a>
                                                                        <ul class="dropdown-menu">
                                                                        <li class="men"><a href="">XX.2.1</a></li>
                                                                        <li class="men"><a href="">XX.2.2</a></li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                </div> 
                                                <div class="k-new-navbar-item-container dropdown men">
                                                    <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">Sales Agents
                                                        <span class="caret"></span>
                                                    </a>
                                                       <ul class="dropdown-menu">
                                                          <li class="men"><a href="#">Opportunities</a></li>
                                                          <li class="men"><a href="#">Training</a></li>
                                                       </ul>
                                                </div>
                                                 <div class="k-new-navbar-item-container dropdown men">
                                                    <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">How it works
                                                     <span class="caret"></span></a>
                                                       <ul class="dropdown-menu">
                                                          <li class="men"><a href="#">Companies</a></li>
                                                          <li class="men"><a href="#">Sales Agents</a></li>
                                                          <li class="men"><a href="#">Buy Easy</a></li>
                                                          <li class="men"><a href="#">Refer and Earn</a></li>
                                                          <li class="men"><a href="#">Loyalty Points</a></li>
                                                       </ul>
                                                </div>
                                                <div class="k-new-navbar-item-container"><a class="k-new-navbar-item" href="#">Contact Us</a></div>
                                                <div class="k-new-navbar-item-container "><a class="k-new-navbar-item-login" data-logged-in="false" href="#">Login</a>
                                                </div>
                                                <!-- <div class="k-new-navbar-item-container k-new-navbar-item-container--hidden">
                                                    <a class="k-new-navbar-item-login" data-logged-in="true" href="#">login</a>
                                                </div> -->

                                                <div class="k-new-navbar-item-container">
                                                    <a class="k-new-navbar-item-store" href="#">Sign Up</a>
                                                </div>
                                
                                        </div>
                                    </div>
                                    <a class="k-new-navbar-side-bar-item" href="#">How It Works</a><a class="k-new-navbar-side-bar-item" href="#">Coverage</a>
                                    <a class="k-new-navbar-side-bar-item" href="#">Blog</a>
                                    <a class="k-new-navbar-side-bar-item" href="#">Get Started</a><a class="k-new-navbar-side-bar-item" href="#">About Us</a>
                                    <a class="k-new-navbar-side-bar-item" href="#">Jobs</a>
                                    <div class="k-new-navbar-item-container--hidden">
                                        <h3 class="k-header--xx-small k-spacing-top--x-large k-spacing-bottom--base k-color--blue">My Account</h3><a class="#" data-logged-in="true" href="#">Sign Out</a>
                                    </div>
                                    <h3 class="k-header--xx-small k-spacing-top--x-large k-spacing-bottom--base k-color--blue">Get Support</h3>
                                    <a class="k-new-navbar-side-bar-item" href="#">Help Center</a>
                                    <a class="k-new-navbar-side-bar-item" href="#">Contact Us</a>
                                    <a class="k-new-navbar-side-bar-item" href="#">Return</a>
                                        <a class="k-new-navbar-side-bar-item k-new-navbar-side-bar-item-legal" href="#">Privacy</a><a class="k-new-navbar-side-bar-item k-new-navbar-side-bar-item-legal" href="#">Terms</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </header>
        <?php 
        // $email_token=Route::currentRouteName();
        $email_token=request()->route('email_token');
        ?>
 @include('front.home.login')
        <div class="Get_started">
            <h3>Get started now with Pro for Free.</h3>
            <p class="get_para">
            <span>No credit cards.Upgrade or Cancel subscription at any time.

            </span></p>
            <div class="toggle_year">
                <a class="first_box tablinks" href="javascript:void(0)" onClick="price(event, 'firstyear')" id="defaultOpen">
                    Monthly
                </a>
                <a class="second_box tablinks" href="javascript:void(0)" onClick="price(event, 'twoyear')">
                    Yearly
                </a>
                <!-- <a class="third_box tablinks" herf="" onclick="price(event, 'threeyear')">
                    Choose billing currency
                </a> -->
            </div>
            <div class="container">
                <div class="yearly_montly">
                	<div id="firstyear" class="tabcontent">
	                	<div class="col-md-3">
	                		<div class="starter">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name"> Starter</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $0 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Sign Up <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="starter1">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name"> Pro</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $0 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Try Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="starter2">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name">Business</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $0 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Try Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="starter3">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name"> Premium Plus</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $0 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Get In Touch <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
                	</div> 
                	<div id="twoyear" class="tabcontent" style="display: none;">
                		<div class="col-md-3">
	                		<div class="starter">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name"> Starter</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $1 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Sign Up <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="starter1">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name"> Pro</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $1 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Try Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="starter2">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name">Business</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $1 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Try Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="starter3">
	                			<div class="top_starter">
	                				<div class="top_previous">
	                					<div class="plan-name"> Premium Plus</div>
	                					<div class="person_name"><img src="images/noimage.png"/></div>
	                				</div>
	                				<div class="top_secondry">
	                					<div class="price_value">
	                						<span class="cycle-price"> $1 </span> 
	                						<span class="per-cycle"> / mo </span>
	                					</div>
		                				<div class="seats">
		                					<div class="additional-seats"></div>
		                					<div class="included"> 1 included</div>
		                				</div>

	                				</div>
	                			</div>
	                			<div class="bottom_starter">
	                				<div class="explainer"> Our document editor and powerful quoting tools</div>
	                				<a class="left_input" href="">Get In Touch <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	                			</div>
	                		</div>
	                	</div>
	                </div> 

	                 <script>
                            function price(evt, stepNum) {
                                var i, tabcontent, tablinks;
                                tabcontent = document.getElementsByClassName("tabcontent");
                                for (i = 0; i < tabcontent.length; i++) {
                                    tabcontent[i].style.display = "none";
                                }
                                tablinks = document.getElementsByClassName("tablinks");
                                for (i = 0; i < tablinks.length; i++) {
                                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                                }
                                document.getElementById(stepNum).style.display = "block";
                                evt.currentTarget.className += " active";
                            }
                            // Get the element with id="defaultOpen" and click on it
                            document.getElementById("defaultOpen").click();
                    </script>   
	               
	            </div>   
            </div>

        </div>
        
       
        <section class="premium-partners">
            <div class="">
               <!--  <row>
                    <h3 class="h3-heading"> premium partners </h3> 
                    <span class="h3-subheading">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and type setting industry. 
                            Lorem Ipsum has been the industry's standard dummy text
                        </p>
                    </span>
                </row> -->

                <row>
                    <div class="our-partners">
                        
                        <div class="owl-carousel owl-theme">
                           
                            <div class="item"><img class="img-responsive" src="images/free-logo-11.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-12.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-13.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-14.1.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-15.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-16.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-17.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-18.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-19.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-110.png" ></div>
                        </div>
                    </div>
                </row>

            </div>
        </section>
        <section class="premium-partners">
            <div class="container">
                <row>
                    <div class="Get_started">
                    <h3 width="70%;"> Compare Features </h3> 
					</div>
                    <!-- <p>Best-in-class influencer insights and reporting platform</p> -->
                    <span class="h3-subheading">
                     
                    </span>
                </row>
                <div class="row">
                    <div class="col-md-12">
                                <!-- <div class="compare_feature">
                                   
                                    <div class="table-responsive">
                                        <table  class="table">
                                            <tr>
                                                <td></td>
                                                <td class="center_matter">Recruiting commission-only sales reps with CommissionCrowd</td>
                                                <td class="center_matter"> Hiring a sales employee</td>
                                            </tr>
                                            <tr class="highlist_list">
                                                <td><h4>Cost</h4></td>
                                                <td class="center_matter"><h4>£995 (Premium VIP Membership) + 1% commission</td>
                                                <td class="center_matter"><h4>£65,200 per employee (plus commissions)</td>
                                            </tr>
                                            <tr>
                                                <td><h4>Salary</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"> <h4>£40K + (commission)</h4></td>
                                            </tr>
                                            <tr class="highlist_list">
                                                <td><h4>Training</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£4k (time inbetween training and starting to make sales)</h4></td>
                                            </tr>
                                            <tr>
                                                <td><h4>Expenses</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£1.5k+ (including travel, telephone, internet etc)</td>
                                            </tr>
                                            <tr class="highlist_list">
                                                <td><h4>Equipment</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£2k+ (computers, telephone, stationary)</h4></td>
                                            </tr>
                                            <tr>
                                                <td><h4>Benefits and contributions</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£2k</h4></td>
                                            </tr>
                                            <tr class="highlist_list">
                                                <td><h4>Office space/rates</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£6k+ (Based on Co Working Space)</h4></td>
                                            </tr>
                                            <tr>
                                                <td><h4>Holidays/Sick Days + Other paid leave:</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£4k (based on one month paid leave per annum)</h4></td>
                                            </tr>
                                            <tr class="highlist_list">
                                                <td><h4>Company Car + fuel allowance</h4></td>
                                                <td class="center_matter"><h4>£0</h4></td>
                                                <td class="center_matter"><h4>£5k+</h4></td>
                                            </tr>
                                            <tr>
                                                <td><h4>CRM</h4></td>
                                                <td class="center_matter"><h4>Included in each plan</h4></td>
                                                <td class="center_matter"><h4>£700+</h4></td>
                                            </tr>
                                            <tr class="highlist_list">
                                                <td><h4></h4></td>
                                                <td class="center_matter"><h4>Cost for unlimited sales recruitment via CommissionCrowd</h4></td>
                                                <td class="center_matter"><h4>Basic total Cost per employee/year</h4></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="center_matter"><h4>£995 (Premium VIP Membership) + 1% commission*</h4></td>
                                                <td class="center_matter"><h4>£65,200 (plus sales commissions)**</h4></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div> -->
                               <div class="compare_feature">
                                    
                                    <div class="table-responsive">
                                        <table  class="table">
                                        	<tr>
                                        		<td>
                                        			<div class="compare_head">
                                        				<img src="images/sele_logo.png" class="img-responsive">
                                        				<h3>Selecoms</h3>
                                    				</div>
                                    			</td>
                                    			<td>STARTED</td>
                                    			<td>PRO</td>
                                    			<td>BUSINESS</td>
                                    			<td>ENTERPRISE</td>
                                        	</tr>
                                            <tr>
                                                <td>Google Fonts</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Template Library</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>View Notifications</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Time Limits</td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Interactive Quoting</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Accept Button</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                        		<td>
                                        			<div class="compare_head1">
                                        				<img src="images/sele_logo.png" class="img-responsive">
                                        				<h3>Security & Analytics</h3>
                                    				</div>
                                    			</td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                        	</tr>
                                            <tr>
                                                <td>Google Fonts</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Template Library</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>View Notifications</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Time Limits</td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Interactive Quoting</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Accept Button</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>

                                            <tr>
                                        		<td>
                                        			<div class="compare_head1">
                                        				<img src="images/sele_logo.png" class="img-responsive">
                                        				<h3>Sales Tools</h3>
                                    				</div>
                                    			</td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                        	</tr>
                                            <tr>
                                                <td>Google Fonts</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Template Library</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>View Notifications</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Time Limits</td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Interactive Quoting</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Accept Button</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>

                                            <tr>
                                        		<td>
                                        			<div class="compare_head1">
                                        				<img src="images/sele_logo.png" class="img-responsive">
                                        				<h3>Integrations</h3>
                                    				</div>
                                    			</td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                        	</tr>
                                            <tr>
                                                <td>Google Fonts</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Template Library</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>View Notifications</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Time Limits</td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Interactive Quoting</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Accept Button</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                        		<td>
                                        			<div class="compare_head1">
                                        				<img src="images/sele_logo.png" class="img-responsive">
                                        				<h3>Support</h3>
                                    				</div>
                                    			</td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                        	</tr>
                                            <tr>
                                                <td>Google Fonts</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Template Library</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>View Notifications</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Time Limits</td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Interactive Quoting</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Accept Button</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- <div class="compare_feature">
                                  
                                    <div class="table-responsive">
                                        <table  class="table">
                                        	<tr>
                                        		<td >
                                        			<div class="compare_head">
                                        				<img src="images/sele_logo.png" class="img-responsive">
                                        				<h3>Selecoms</h3>
                                    				</div>
                                    			</td>
                                    			 <td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                    			<td> </td>
                                        	</tr>
                                            <tr>
                                                <td>Google Fonts</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Template Library</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>View Notifications</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Time Limits</td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Interactive Quoting</td>
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/right_green1.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td>Accept Button</td>
                                                
                                                <td><img src="images/right_yellow.png" class="img-responsive"></td>
                                                <td><img src="images/cross.png" class="img-responsive"></td>
                                                 <td><img src="images/right_green.png" class="img-responsive"></td>
                                                <td><img src="images/right_red.png" class="img-responsive"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div> --> 
                    </div>
                </div>

            </div>
        </section>

        <section class="choose_plan">
        	<h2>Choose the right Selecoms plan for you</h2>
        	<p>Experience the best of Selecoms for free forever, or start with a 14-day trial of the Business plan. No credit card or contracts.</p>
        	<a class="left_input" href="">Try It Free <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        </section>
        <section class="description_selecoms">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>WHAT ARE 'Selecoms'?</h4>
        					<p>Selecoms lets you replace clunky PDF proposals, quotes and presentations with mobile-friendly webpages that are interactive and mobile-friendly, offer actionable insights and connect with your other cloud apps to save you time and win you more work. We call these webpage-documents (which can be easily accessed through URLs) ‘<a href="">Selecoms</a>’.</p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>HOW DOES ANNUAL PRICING WORK?</h4>
        					<p>Very simply. We bill you once for a full 12 months of access to Selecoms — at a discount of up to $9 or 14% on the per-month price.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>ARE ANY FEES PAYABLE IN RELATION TO THE PAYMENTS FEATURE?</h4>
        					<p>Yes, but only if you’re on the free, Starter plan — in which case you will pay (to our payments integration partner, Stripe) a 1% transaction fee on payments accepted through your selecoms.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>WHAT ARE THE VARIOUS BRANDING OPTIONS?</h4>
        					<p>The Selecoms branding visible to viewers of your Selecomss depends on your plan. On the Starter and Pro plans, there is a modest Selecoms badge on your Selecomss and the emails your clients receive when they accept your Selecomss. On the Business plan, the Selecoms badge is removed from your Selecomss, and you can use Typekit fonts and a custom subdomain (e.g., acme.Selecoms.com). On Enterprise, we remove all references to Selecoms (except as required for technical or legal reasons) on your Selecomss and the emails your clients receive when they accept your Selecomss, and you can use Custom fonts and a custom domain (e.g., proposals.acme.com).</p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>DO YOU CHARGE FOR EACH USER ON AN ACCOUNT?</h4>
        					<p>Each of our plans has a default number of included seats. Additional seats can be added to your account if you’re subscribed to one of Selecoms paid plans.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>HOW DOES THE 14-DAY TRIAL OF THE BUSINESS PLAN WORK AND WHAT HAPPENS WHEN IT EXPIRES?</h4>
        					<p>When you sign up to Selecoms, you’ll have the option of moving directly to our free Starter plan or trialing our Business plan for free for 14 days. Once the trial expires, you can downgrade to the Free or Pro plans, subscribe to the Business plan, or upgrade to the Enterprise plan. Either way, you’ll continue to have access to your Selecoms account and the features of whatever plan you move to.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>WHAT HAPPENS IF I CLOSE MY ACCOUNT?</h4>
        					<p>If your trial expires, your Selecomss will remain accessible to (and acceptable by) your clients, but your account will be inaccessible to you. If your account is closed (e.g., on your instruction or following the cancellation of your subscription), then your account and your Selecoms become inaccessible.</p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>WHAT IS A 'LIVE PROJECT'?</h4>
        					<p>A new project is in ‘draft’ mode until it is turned ‘live’. While in draft, the Selecoms can be viewed by the project owner and other Selecoms users on the same account, and no analytics views are recorded. Once switched to live, the Selecoms can be viewed by anyone with the URL and analytics data will be recorded.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>ARE MY Selecoms STILL SECURE WITHOUT ACCESS TO 'SECURITY OPTIONS'?</h4>
        					<p>Yes, all Selecoms URLs are secret links that contain randomly and automatically generated strings — technology that Dropbox, Google and others use to ensure that only those with whom the secret links are shared can view your Selecomss. Selecoms also secures access to your Selecomss throughout its own infrastructure. The ‘security options’ available on some of paid plans offer additional security through specific tools, like passwords, time limits and view limits.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>HELLO, I'M A STUDENT / WORK FOR A NON-PROFIT</h4>
        					<p>That's awesome! We’d love for you to use Selecoms! Simply let us know and we’ll help you out.</p>
        				</div>
        			</div>
        			<div class="col-md-4">
        				<div class="what_selecoms">
        					<h4>I HAVE MORE QUESTIONS</h4>
        					<p>Cool. Check out our help center or let us know.<!-- <a href="">Selecoms</a>’. --></p>
        				</div>
        			</div>
        			
        		</div>
        	</div>
        </section>
        <section class="newsletter">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-6">
        				<div class="join_over">
        					<p>Join over 50,000 businesses enjoying a simpler life</p>
        				</div>
        			</div>
        			<div class="col-md-6">
        				<div class="join_over1">
        					<input type="text" placeholder="Work Email"/>
        					<a href="">GET STARTED <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        				</div>
        			</div>
        		</div>
        		<div class="row">
        			<row>
                    <div class="our-newsletter">
                        <ul>
                            <li class="item"><img class="img-responsive" src="images/free-logo-11.1.png" ></li>
                            <li class="item"><img class="img-responsive" src="images/free-logo-12.1.png" ></li>
                            <li class="item"><img class="img-responsive" src="images/free-logo-13.1.png" ></li>
                            <li class="item"><img class="img-responsive" src="images/free-logo-14.1.png" ></li>
                            <li class="item"><img class="img-responsive" src="images/free-logo-15.1.png" ></li>
                            <!-- <div class="item"><img class="img-responsive" src="images/free-logo-16.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-17.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-18.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-19.png" ></div>
                            <div class="item"><img class="img-responsive" src="images/free-logo-110.png" ></div> -->
                        </ul>
                    </div>
                </row>
        		</div>
        	</div>
        </section>
       <footer>
            <div class="salecoms-footer">
                <div class="container">
                    <row>
                        <div class="ft-sec">

                            
                                <div class="cmpy-links">
                                    <h4> <!-- <img src="images/logo-black.png" alt=""> --> Product</h4>
                                    <ul>
                                        <li><a href="#"> <!-- Home --> For individuals </a></li>
                                        <li><a href="#"> <!-- Opportunities for sales Agent --> For teams  </a></li>
                                        <li><a href="#"> <!-- How we help compsnies --> For organizations</a></li>
                                        <li><a href="#"> <!-- How we help Buyers --> Integrations </a></li>
                                        <li><a href="#"> <!-- Plans / Pricing --> Pricing </a></li>  
                                        <li><a href="#"> <!-- Plans / Pricing --> Help </a></li>
                                    </ul>
                                </div>
                           

                            
                                <div class="cmpy-links">
                                    <h4> Top Templates </h4>
                                    <ul>
                                        <li><a href="#"> <!-- Refer and Earn --> Sponsorship Proposal </a></li>
                                        <li><a href="#"> <!-- Loyalty Program --> Construction Proposal</a></li>
                                        <li><a href="#"> <!-- Sales Agents Training --> Event Planning Proposal </a></li>
                                        <li><a href="#"> <!-- Ebooks --> Marketing Proposal</a></li>
                                         <li><a href="#"> <!-- Ebooks --> Website Proposal </a></li>
                                    </ul>
                                </div>
                            

                            
                                <div class="cmpy-links">
                                    <h4> <!-- Who We Are --> Resources</h4>
                                    <ul>
                                        <li><a href="#"> <!-- About us --> Our Guides </a></li>
                                        <li><a href="#"><!--  Our Team --> Ad Spend Calculator </a></li>
                                        <li><a href="#"> <!-- Careers --> Resume Builder </a></li>
                                        <li><a href="#"> <!-- Press --> Webinars </a></li>
                                        <!-- <li><a href="#"> Blog </a></li>  -->
                                    </ul>
                                </div>
                           
                                <div class="cmpy-links">
                                    <h4> <!-- Support --> Company</h4>
                                    <ul>
                                        <li><a href="#"> <!-- Help Center --> About Us </a></li>
                                        <li><a href="#"> <!-- Contact us --> Selecoms in the Media </a></li>
                                        <li><a href="#"> <!-- FAQ's --> Jobs </a></li>
                                        <li><a href="#"> <!-- Privacy & Terms --> Privacy </a></li>
                                        <li><a href="#"> <!-- Privacy & Terms --> Terms of Service  </a></li>
                                    </ul>
                                </div>
                                <div class="cmpy-links">
                                    <h4> Contact </h4>
                                    <ul>
                                        <li><a href="#"> Book a Demo  </a></li>
                                        <li><a href="#"> Contact Help  </a></li>
                                        <li><a href="#"> Follow us <span> <i class="fa fa-twitter" aria-hidden="true"></i> </span></a></li>
                                        <!-- <li><a href="#"> Privacy & Terms </a></li> -->
                                    </ul>
                                </div>


                            
                        </div>
                    </row>
                    <div class="bottom_footer">
	                    <div class="row">
			                                <div class="col-md-4">
			                                	<div class="logo_bottom">
			                                		<a href="">
			                                			<img src="images/logo-white.png"/>
			                                		</a>
			                                	</div>
			                                </div>
			                                 <div class="col-md-4">
			                                 	<div class="logo_bottom1">
			                                 		<a href="">Made with in India</a>
			                                 	</div>
			                                </div>
			                                 <div class="col-md-4">
			                                 	<div class="social_icon">
			                                 		<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
			                                 		<a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
			                                 		<a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			                                 	</div>
			                                </div>
	                    </div>
                	</div>
                </div>
            </div>

           
        </footer> 
        
        <!-- <footer>
          <div class="contact_footer">
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <div class="contact_address">
                      <h3>Contact Us</h3>
                      <div class="contact_person">
                        <p><span>Toll-Free : </span>123-456-7890</p>
                        <p><span>Phone : </span>123-456-7890</p>
                        <p><span>Email : </span> office@selecome.com</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="contact_address">
                      <h3>Corporate Headquarters</h3>
                      <div class="contact_person">
                        <p>1985 Cedar Bridge Ave., Suite 3 Lakewood, NJ 08701</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="social_links">
                    <div class="">
                      <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                      <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                      <a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="footer_bottom">
                        <div class="links_nav">
                          <ul>
                            <li><a href="">Careers</a></li>
                            <li><a href="">Contact Us</a></li>
                            <li><a href="">Site Map</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Terms of Use</a></li>
                          </ul>
                        </div>
                        <p>Copyright © 2018 Selecome | All rights reserved</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer> -->


           <section class="earn-point">
            <a href="#">
                <img src="{{ URL::asset('/img/earn-point-img.png') }}">
                  <!-- <p class="ref_para"><span>Refer</span> <br/>a company or <br/> buyer and earn <br/>up to N100,000 <br/><span>($300)</span> per referral.</p> -->
            </a>
        </section>
        
        <script type="text/javascript" src="{{ URL::asset('/js/side-navbar.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/bootstrap-select.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-ui.min.js')}}"></script> 
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-ui.tabs.neighbors.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/input-bx.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery.magicsearch.js')}}"></script>
        @if(request()->route('email_token') != '')
         <script>
         $(document).ready(function () {
            $('#loginmodel').modal('show');
         });
         document.getElementById("resetPassword").click();
         </script>
         @endif
        <script>
        

        //Ajax Login Setup
            $("#loginForm").submit(function(stay){
                $('#signinerror_email').html('');
                $('#signinerror_password').html('');
                $('#messageBody').html('');
               // $('#userNotApplicableMsg').html('');
                //$('#userInvalid').html('');
               // $('#loginMsg').html('');

            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/login",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#signinerror_'+key).html(val);
                    });
                }
                

                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-success">'+data.loginMsg+'</div>');
                    //$('#loginMsg').html(data.loginMsg);
                    window.location.href = data.loginurl;
                }
                if(data.status == 2){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.userNotApplicableMsg+'</div>');
                    //$('#userNotApplicableMsg').html(data.userNotApplicableMsg);
                }
                if(data.status == 3){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.userInvalid+'</div>');
                    //$('#userInvalid').html(data.userInvalid);
                }
            },
            });
            stay.preventDefault(); 
            });




         //Ajax Registeration Setup
         $("#registerForm").submit(function(stay){
            $('#signuperror_first_name').html('');
            $('#signuperror_last_name').html('');
            $('#signuperror_phone').html('');
            $('#signuperror_email').html('');
            $('#signuperror_password').html('');
            $('#signuperror_role_id').html('');
             $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/register",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#signuperror_'+key).html(val);
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-success">OTP Sent to your email Please Verify..</div>');
                    document.getElementById("registerForm").style.display = "none";
                    document.getElementById("otpForm").style.display = "block";
                    document.getElementById("user_email").value = data.otpMsg;
                   // $('#otpMsg').html(data.otpMsg);
                    console.log(data.otpMsg);
                    //$('#loginMsg').html(data.loginMsg);
                    //window.location.href = data.loginurl;
                }

            },
            });
            stay.preventDefault(); 
            });    


//Ajax Registeration Setup
$("#otpForm").submit(function(stay){
            $('#otperror_otp_post').html('');
            $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/otp-verification",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#otperror_'+key).html(val);
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.otpPost+'</div>');
                     // $('#otpMsg').html(data.otpMsg);
                    console.log(data.otpPost);
                    //$('#loginotpMsg').html(data.loginotpMsg);
                    //window.location.href = data.loginotpurl;
                }
                if(data.status == 2){
                    $("#messageBody").append('<div class="alert alert-success">'+data.otpPost+'</div>');
                    // $('#otpMsg').html(data.otpMsg);
                    console.log(data.otpPost);
                    //$('#loginotpMsg').html(data.loginotpMsg);
                    window.location.href = data.otpRedirect;
                }

            },
            });
            stay.preventDefault(); 
            });    


             //Ajax Forgot Password Setup
         $("#forgotpasswordForm").submit(function(stay){
            $('#forgotpassworderror_email').html('');
            $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/forgot-password",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#forgotpassworderror_'+key).html(val);
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.usernotvalid+'</div>');
                 }
                if(data.status == 2){
                    $("#messageBody").append('<div class="alert alert-success">'+data.successForgotPass+'</div>');
                 }

                
        
            },
            });
            stay.preventDefault(); 
            }); 


               //Ajax Reset Password Setup
         $("#resetpasswordForm").submit(function(stay){
            $('#errorpassworderror_password').html('');
            $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company/reset-password/'.$email_token) }}",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    console.log($('#errorpassworderror_'+key).html(val));
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-success">'+data.successPasswordReset+'</div>');
                 }
               

                
        
            },
            });
            stay.preventDefault(); 
            });     
            

         $(document).ready(function () {
            //Script to generate OTP
            var x = Math.floor((Math.random() * 9999) + 1000);
            document.getElementById("otp").value = x;
         });
        </script>
            <script>
                function selectBox(){
                    $('.selectpicker').selectpicker();
                }
            </script>
            <script>
                $(document).ready(function(){
                  $('.dropdown-submenu a.test').on("click", function(e){
                    $(this).next('ul').toggle();
                    e.stopPropagation();
                    e.preventDefault();
                  });
                });
            </script>
            <script type="text/javascript">
                    $('.owl-carousel').owlCarousel({
                    loop:true,
                    margin:10,
                    nav:true,
                    autoplay: 100,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:3
                        },
                        1000:{
                            items:6
                        }
                    }
                })
        </script>

    </body>
</html>
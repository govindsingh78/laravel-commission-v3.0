 


                  

       <div class="loginmodel">
                <div class="modal fade" id="loginmodel" role="dialog">
                    <div class="modal-dialog modal-sm">
                    
                      <!-- Modal content-->
                          <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <div class="login_signup">
                                        <a class="signmo" href="javascript:void(0)" onClick="loginmodel(event, 'login_body')" id="defaultOpen1">Login </a>
                                        <a class="signmo" href="javascript:void(0)" onClick="loginmodel(event, 'signup_body')" id="defaultOpen2"> Signup</a>
                                        
                                      </div>
                                    </div>
                                    <div class="modal-body">

                                          <div id="messageBody"></div>
                                               

                                        <div class="loginmo" id="login_body" >

                                                <!-- @if(session('signin-error'))
                                                <div class="alert alert-danger">
                                                {!! session('signin-error') !!}
                                                </div>
                                                @endif
                                                @if (session('signin-success'))
                                                <div class="alert alert-success">
                                                {! session('signin-success') !}
                                                </div>
                                                @endif -->


                                                 
                                                


                                               
                                                 {!! Form::open(array('url' =>  URL::to('/company/login'), 'name' => 'form-login', 'id' => 'loginForm')) !!}
                                                 {!! csrf_field() !!}
                                                 <div class="field-group">
                                                  <label class="full-width login-field">
                                                    {!! Form::text('email',null,['class'=>'form-control login-field__input','placeholder'=>'Enter email']) !!}
                                                    <span class="login-field__text">Email Address or Mobile Number</span>
                                                  </label>
                                                 
                                                </div>

                                                <div class="field-group">
                                                <div id="signinerror_email" class="error">{{ $errors->first('email') }}</div>
                                                </div>

                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                   {!! Form::password('password',['class'=>'form-control login-field__input','placeholder'=>'Password']) !!}
                                                    <span class="login-field__text">Password</span>
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="signinerror_password" class="error">{{ $errors->first('password') }}</div>
                                                </div>
 
                                               
                                                  <div class="margin-top-s">
                                                    <div class="secondary-links flt-right">
                                                      
                                                      <a class="txt-brand-tertiary" href="javascript:void(0)" onClick="loginmodel(event, 'forgot')">Forgot password?
                                                      </a>
                                                      <a class="txt-brand-tertiary" href="javascript:void(0)" onClick="loginmodel(event, 'resetpass')" style="display: none" id="resetPassword">Reset password?
                                                      </a>
                                                    </div>
                                                  </div>
                                                
                                               
                                                <div class="field-group form-group login_btn">
                                                 {!! Form::submit('Login',['class'=>'btn btn--primary btn-block'])!!}
                                                </div>
                                                 {!! Form::close() !!} 
 
                                        </div>

                                        <!-- qwqwqwq -->
                                        <div class="loginmo resetpass" id="resetpass"  style="display:none;">

                                         
                                        
                                         

                                        {!! Form::open(array('url' =>  URL::to('/company/reset-password/'.$email_token), 'name' => 'form-forgot-password', 'id' => 'resetpasswordForm')) !!}
                                        
                                        {!! csrf_field() !!}
                                              
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::password('password',['class'=>'form-control login-field__input','placeholder'=>'Enter Password', 'autocomplete'=>'off']) !!}
                                                    
                                                    <span class="login-field__text">Password </span>
                                                   </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="errorpassworderror_password" class="error">{{ $errors->first('password') }}</div>
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::password('password_confirmation',['class'=>'form-control login-field__input','placeholder'=>'Confirm Password', 'autocomplete'=>'off']) !!}
                                                    
                                                    <span class="login-field__text">Confirm Password</span>
                                                   </label>
                                                </div>
                                                
                                                <div class="field-group form-group member_pricing">
                                                  <!-- <a href=""> < Back</a> -->
                                                   
                                                  {!! Form::submit('Update Password',['class'=>'btn btn-primary btn-cont'])!!}
                                                </div>

                                           {!! Form::close() !!} 
                                        </div>
                                          <!-- wqwqwqwqw -->

                                        <div class="loginmo forgot" id="forgot"  style="display:none;">
                                        {!! Form::open(array('url' =>  URL::to('/company/forgot-password'), 'name' => 'form-forgot-password', 'id' => 'forgotpasswordForm')) !!}
                                        <!-- @if(session('forgotpass-error'))
                                                <div class="alert alert-danger">
                                                {!! session('forgotpass-error') !!}
                                                </div>
                                                @endif
                                                @if (session('forgotpass-success'))
                                                <div class="alert alert-success">
                                                {!! session('forgotpass-success') !!}
                                                </div>
                                                @endif -->
                                        
                                        {!! csrf_field() !!}
                                                <!-- <div class="field-group">
                                                 <label class="full-width login-field para_forgot">
                                                    <div class="login-field__text">A One-Time Password (OTP) that was sent to your registered Email ID / Mobile Number</div>
                                                 </label>
                                                </div> -->
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::text('email',null,['class'=>'form-control login-field__input','placeholder'=>'Enter email']) !!}
                                                    
                                                    <span class="login-field__text">email address or mobile number</span>
                                                   </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="forgotpassworderror_email" class="error">{{ $errors->first('email') }}</div>
                                                </div>
                                                <div class="field-group form-group member_pricing">
                                                  <a href=""> < Back</a>
                                                   
                                                  {!! Form::submit('SET PASSWORD',['class'=>'btn btn-primary btn-cont'])!!}
                                                </div>

                                           {!! Form::close() !!} 
                                        </div>
                                        <div class="loginmo" id="signup_body"  style="display:none;">
                                          <div id="otpMsg"></div>
                                        {!! Form::open(array('url' =>  URL::to('/company/otp-verification'), 'name' => 'form-otp', 'id' => 'otpForm', 'style'=>'display:none')) !!}
                                        {!! csrf_field() !!}
                                        
                                        {!! Form::hidden('user_email',null,['class'=>'form-control login-field__input', 'id'=>'user_email']) !!}
                                                 
                                                <!-- <div class="field-group">
                                                 <label class="full-width login-field">
                                                    <div class="login-field__text">Enter the One-Time Password (OTP) that was sent to <span>0987654321</span></div>
                                                 </label>
                                                </div> -->

                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::text('otp_post',null,['class'=>'form-control login-field__input','placeholder'=>'Enter otp']) !!}
                                                  <span class="login-field__text">OTP</span>
                                                  <a href="" class="resend_code">Resend OTP ?</a> 
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="otperror_otp_post" class="error">{{ $errors->first('otp_post') }}</div>
                                                </div>
                                               
                                                <div class="field-group form-group member_pricing">
                                                  <a href=""> < Back</a>
                                                  {!! Form::submit('Continue',['class'=>'btn btn-primary btn-cont']) !!}
                                                </div>

                                          {!! Form::close() !!} 
                                        
           
                                        {!! Form::open(array('url' =>  URL::to('/company/register'), 'name' => 'form-register', 'id' => 'registerForm')) !!}
                                        <!-- @if(session('signup-error'))
                                                <div class="alert alert-danger">
                                                {!! session('signup-error') !!}
                                                </div>
                                                @endif
                                                @if (session('signup-success'))
                                                <div class="alert alert-success">
                                                {!! session('signup-success') !!}
                                                </div>
                                                @endif -->
                                                {!! Form::hidden('otp',null,['class'=>'form-control login-field__input', 'id'=>'otp']) !!}
                                                
                                        {!! csrf_field() !!}
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::text('first_name',null,['class'=>'form-control login-field__input','placeholder'=>'Enter first name']) !!}
                                                     <span class="login-field__text">First Name</span>
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="signuperror_first_name" class="error">{{ $errors->first('first_name') }}</div>
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::text('last_name',null,['class'=>'form-control login-field__input','placeholder'=>'Enter last name']) !!}
                                                     <span class="login-field__text">Last Name</span>
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="signuperror_last_name" class="error">{{ $errors->first('last_name') }}</div>
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::text('email',null,['class'=>'form-control login-field__input','placeholder'=>'Enter email']) !!}
                                                  <span class="login-field__text">Email</span>
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="signuperror_email" class="error">{{ $errors->first('email') }}</div>
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                  {!! Form::text('phone',null,['class'=>'form-control login-field__input','placeholder'=>'Enter mobile no.']) !!}
                                                    <span class="login-field__text">Phone</span>
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                <div id="signuperror_phone" class="error">{{ $errors->first('phone') }}</div>
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                   {!! Form::password('password',['class'=>'form-control login-field__input','placeholder'=>'Password']) !!}
                                                    <span class="login-field__text">Password</span>
                                                  </label>
                                                </div>
                                                
                                                <div class="field-group">
                                                <div id="signuperror_password" class="error">{{ $errors->first('password') }}</div>
                                                </div>

                                                 <div class="field-group">
                                                  <label class="full-width login-field">
                                                   {!! Form::password('password_confirmation',['class'=>'form-control login-field__input','placeholder'=>'Confirm Password']) !!}
                                                    <span class="login-field__text">Confirm Password</span>
                                                  </label>
                                                </div>
                                                 
                                                <div class="field-group">
                                                       <div class="dropdown selct_box">
                                                          <select name="role_id">
                                                              <option value="">SELECT CATEGORY</option>
                                                              <option value="2">Company: Looking For Commission-only Sales Agents</option>
                                                              <option value="3">Sales Agent: Looking For Commission-based Opportunities.</option>  
                                                          </select>
                                                      </div> 
                                                </div>
                                                <div class="field-group">
                                                <div id="signuperror_role_id" class="error">{{ $errors->first('role_id') }}</div>
                                                </div>
                                                
                                                
                                                  
                                                  <div class="field-group">
                                                  <label class="nb-checkbox margin-top-m sign_noti">
                                                        <input class="ng-valid ng-touched ng-dirty" type="checkbox" name="check_agree" required>
                                                        <div class="nb-checkbox__bg">
                                                          <div class="nb-checkbox__icon"></div>
                                                        </div>
                                                        <span class="sign_span">I accept the terms of services.</span>
                                                  </label>
                                              </div>
                                                
                                                
                                                 <div class="field-group form-group member_login">
                                                  <!-- <a href="pricing3.html" class="btn btn--primary btn-cont">Continue</a> -->
                                                  {!! Form::submit('Continue',['class'=>'btn btn-primary btn-cont'])!!}
                                                  <a href="" class="" href="javascript:void(0)" onClick="loginmodel(event, 'login_body')" >Already a member? 
                                                 <span class="font-size-xs txt-brand-primary">Login</span></a>
                                                </div> 
                                                {!! Form::close() !!} 
                                        </div>
                                    </div>

                                   <!--  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                    <script>
                                            function loginmodel(log, modelNum) {
                                              $('#messageBody').html('');
                                                var i, loginmo, signmo,forgot, resetpass;
                                                loginmo = document.getElementsByClassName("loginmo");
                                                for (i = 0; i < loginmo.length; i++) {
                                                    loginmo[i].style.display = "none";
                                                }
                                                signmo = document.getElementsByClassName("signmo");
                                                for (i = 0; i < signmo.length; i++) {
                                                    signmo[i].className = signmo[i].className.replace(" active", "");
                                                }
                                                forgot = document.getElementsByClassName("forgot");
                                                for (i = 0; i < forgot.length; i++) {
                                                    forgot[i].className = forgot[i].className.replace(" active", "");
                                                }
                                                resetpass = document.getElementsByClassName("resetpass");
                                                for (i = 0; i < resetpass.length; i++) {
                                                  resetpass[i].className = resetpass[i].className.replace(" active", "");
                                                }
                                                document.getElementById(modelNum).style.display = "block";
                                                log.currentTarget.className += " active";
                                            }
                                            // Get the element with id="defaultOpen" and click on it
                                            //document.getElementById("defaultOpen2").click();
                                              </script> 
                                            @if(session('signuptab'))
                                            <script>
                                            document.getElementById("defaultOpen2").click();
                                            </script> 
                                            @elseif(session('signintab'))
                                            <script>
                                            document.getElementById("defaultOpen1").click();
                                            </script> 
                                            @elseif(session('forgotpasstab'))
                                            <script>
                                            document.getElementById("forgot").style.display = "block";
                                            document.getElementById("login_body").style.display = "none";
                                            </script>
                                            @endif
                                            

                          </div>
                      
                    </div>
                </div>
       
        
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Selecoms :: Home </title>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/custom-style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/side-navbar.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap-select.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/font-awesome.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/normalize.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/normalize.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery.magicsearch.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/responsive.css')}}">
        <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script> -->
         
        
    
    </head>
    <body>
        <header>
            <div class="header_part">
                <div class="header_down">
                    <div class="container-fluid">
                        <div class="k-new-navbar-left">
                            <a class="k-new-navbar-logo" href="#">
                                <img src="{{ URL::asset('/img/logo.jpg') }}" alt="logo" class="img-responsive">
                            </a>
                        </div>

                        <div class="k-new-navbar " data-behavior="LoggedInNavbar ToggleClassWhenScrolling " data-login-details="#" data-scroll-class="k-new-navbar--scrolled" data-scroll-class-behavior="add" data-scroll-offset="5">
                            <div class="k-new-navbar-right" >
                                <div class="other_nav">
                                    <div class="k-new-navbar-item-container dropdown men">
                                        <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">
                                            Companies<span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="men"><a href="/pricing">Pricing</a></li>
                                            <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">xx</a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" -toggle="dropdown">XX.1</a>
                                                    </li><li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">XX.2</a>
                                                        <ul class="dropdown-menu">
                                                            <li class="men"><a href="">XX.2.1</a></li>
                                                            <li class="men"><a href="">XX.2.2</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </div> 
                                    <div class="k-new-navbar-item-container dropdown men">
                                        <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">Sales Agents
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="men"><a href="#">Opportunities</a></li>
                                            <li class="men"><a href="#">Training</a></li>
                                        </ul>
                                    </div>
                                    <div class="k-new-navbar-item-container dropdown men">
                                        <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">How it works
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li class="men"><a href="#">Companies</a></li>
                                            <li class="men"><a href="#">Sales Agents</a></li>
                                            <li class="men"><a href="#">Buy Easy</a></li>
                                            <li class="men"><a href="#">Refer and Earn</a></li>
                                            <li class="men"><a href="#">Loyalty Points</a></li>
                                        </ul>
                                    </div>
                                    <div class="k-new-navbar-item-container"><a class="k-new-navbar-item" href="#">Contact Us</a></div>
                                    <div class="k-new-navbar-item-container "><a class="k-new-navbar-item-login" data-logged-in="false" href="#">Login</a>
                                    </div>
                                    <!-- <div class="k-new-navbar-item-container k-new-navbar-item-container--hidden">
                                        <a class="k-new-navbar-item-login" data-logged-in="true" href="#">login</a>
                                    </div>  <div class="k-new-navbar-item-container">
                                        <a class="k-new-navbar-item-store" href="#">Sign Up</a>
                                    </div> -->

                                  
                            <?php
                            // @if(Auth::guard('company')->user()->role_id  == 2 || Auth::guard('company')->user()->role_id == 3)
                            ?>
                            @if (Auth::guard('company')->check())
                           
                            <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-login" href="{{ route('company.dashboard') }}">Welcome {{ Auth::guard('company')->user()->first_name }} </a>
                           </div>
                          
                           <div class="k-new-navbar-item-container">
                           <a class="k-new-navbar-item-login"  href="{{ route('company.logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                            </a>
                            <form id="logout-form" action="{{ route('company.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            </form>
                           
                           </div>
                           @elseif (Auth::guard('admin')->check())
                            <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-login" href="{{ route('admin.dashboard') }}">Welcome {{ Auth::guard('admin')->user()->first_name }} </a>
                           </div>
                           <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-login" href="{{ route('admin.logout') }}">Logout</a>
                            </div>
                            @else
                            <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-login" href="javascript:void(0)" data-toggle="modal" data-target="#loginmodel">Login</a>
                            </div>
                            <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-store" href="javascript:void(0)" data-toggle="modal" data-target="#loginmodel">Sign Up</a>
                            </div>
                            @endif 
                             
                           <? ?>

                            <!-- <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-login" href="javascript:void(0)" data-toggle="modal" data-target="#loginmodel">Login</a>
                            </div>
                            <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-store" href="javascript:void(0)" data-toggle="modal" data-target="#loginmodel">Sign Up</a>
                            </div> -->

                                   

                           


                                </div>
                                <div class="k-new-navbar-item-container k-new-navbar-item-container--last">
                                    <a class="k-new-navbar-item-menu" data-behavior="ExpandNewNavbar" href="#"></a>
                                    <div class="k-new-navbar-item-stack" data-behavior="ExpandNewNavbar">
                                        <div class="k-new-navbar-item-stack-top"></div>
                                        <div class="k-new-navbar-item-stack-middle"></div>
                                        <div class="k-new-navbar-item-stack-bottom"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="k-new-navbar-side-bar">
                                <div class="upper_nav">
                                    <div class="k-new-navbar-right">
                                        <div class="k-new-navbar-item-container dropdown men">
                                            <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">
                                                Companies<span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="men"><a href="">Pricing</a></li>
                                                <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">xx</a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-submenu men "><a href="" class="dropdown-toggle" -toggle="dropdown">XX.1</a>
                                                        </li><li class="dropdown-submenu men "><a href="" class="dropdown-toggle" data-toggle="dropdown">XX.2</a>
                                                            <ul class="dropdown-menu">
                                                                <li class="men"><a href="">XX.2.1</a></li>
                                                                <li class="men"><a href="">XX.2.2</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </div> 
                                        <div class="k-new-navbar-item-container dropdown men">
                                            <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">Sales Agents
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="men"><a href="#">Opportunities</a></li>
                                                <li class="men"><a href="#">Training</a></li>
                                            </ul>
                                        </div>
                                        <div class="k-new-navbar-item-container dropdown men">
                                            <a class="k-new-navbar-item dropdown-toggle" href="#" data-toggle="dropdown">How it works
                                                <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li class="men"><a href="#">Companies</a></li>
                                                <li class="men"><a href="#">Sales Agents</a></li>
                                                <li class="men"><a href="#">Buy Easy</a></li>
                                                <li class="men"><a href="#">Refer and Earn</a></li>
                                                <li class="men"><a href="#">Loyalty Points</a></li>
                                            </ul>
                                        </div>
                                        <div class="k-new-navbar-item-container"><a class="k-new-navbar-item" href="#">Contact Us</a></div>
                                        <div class="k-new-navbar-item-container "><a class="k-new-navbar-item-login" data-logged-in="false" href="#">Login</a>
                                        </div>
                          
                                        <div class="k-new-navbar-item-container">
                                            <a class="k-new-navbar-item-store" href="#">Sign Up</a>
                                        </div>

                                    </div>
                                </div>
                                <a class="k-new-navbar-side-bar-item" href="#">How It Works</a><a class="k-new-navbar-side-bar-item" href="#">Coverage</a>
                                <a class="k-new-navbar-side-bar-item" href="#">Blog</a>
                                <a class="k-new-navbar-side-bar-item" href="#">Get Started</a><a class="k-new-navbar-side-bar-item" href="#">About Us</a>
                                <a class="k-new-navbar-side-bar-item" href="#">Jobs</a>
                                <div class="k-new-navbar-item-container--hidden">
                                    <h3 class="k-header--xx-small k-spacing-top--x-large k-spacing-bottom--base k-color--blue">My Account</h3><a class="#" data-logged-in="true" href="#">Sign Out</a>
                                </div>
                                <h3 class="k-header--xx-small k-spacing-top--x-large k-spacing-bottom--base k-color--blue">Get Support</h3>
                                <a class="k-new-navbar-side-bar-item" href="#">Help Center</a>
                                <a class="k-new-navbar-side-bar-item" href="#">Contact Us</a>
                                <a class="k-new-navbar-side-bar-item" href="#">Return</a>
                                <a class="k-new-navbar-side-bar-item k-new-navbar-side-bar-item-legal" href="#">Privacy</a><a class="k-new-navbar-side-bar-item k-new-navbar-side-bar-item-legal" href="#">Terms</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </header>
        <?php 
        // $email_token=Route::currentRouteName();
        $email_token=request()->route('email_token');
        ?>
 @include('front.home.login')
 
        <div class="contact_banner">
            <img src="{{ URL::asset('/img/contact.jpg') }}" class="img-responsive">
            <div class="contact_banner_matter">
                <h4>Contact Us</h4>
                <p>We available and ready to answer your inquiries</p>
                <a href="#Reasion">
                    <img src="{{ URL::asset('/img/arrow.png') }}">
                </a>
            </div>
        </div>
        <div class="contact_us_matter" id="Reasion">
            <h3>Reason for Inquiry</h3>
            <p class="get_para">Please select the purpose for your inquiry.</p>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-md-4">
                    <div class="about_info">
                        <img src="{{ URL::asset('/img/customer.png') }}">
                        <h3>Request A Demo</h3>
                        <p>Request a demonstration of Selecoms award-winning, differentiated instruction solutions.</p>
                        <a href="" class="biliboard3 repeating">Request a Demo</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about_info">
                        <img src="{{ URL::asset('/img/customer1.png') }}">
                        <h3>Sales Inquiry</h3>
                        <p>Reach out to our Sales team directly for immediate assistance with all sales-related inquiries.</p>
                        <a href="" class="biliboard3 repeating">Sales Inquiry</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about_info">
                        <img src="{{ URL::asset('/img/customer2.png') }}">
                        <h3>Customer Support</h3>
                        <p>Get in touch with customer support for assistance with your Selecoms implementation.</p>
                        <a href="" class="biliboard3 repeating">Customer Support</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="map_section">
            <div class="col-md-4">
                <div class="address_info">
                    <h3>
                        <img src="{{ URL::asset('/img/map_pin.png') }}" class="img-responsive">
                        <span>
                            Locate us
                        </span>
                    </h3>
                    <p>1985 Cedar Bridge Ave., Suite 3
                        Lakewood, NJ 08701</p>
                    <a href="https://www.google.co.in/maps/dir//Embassy+of+the+United+States+of+America,+Shantipath,+Chanakyapuri,+Chanakyapuri,+New+Delhi,+Delhi+110021/@28.5972277,77.1876009,18z/data=!4m8!4m7!1m0!1m5!1m1!1s0x390d1d676f4487ad:0xad2e642d2072c910!2m2!1d77.1880475!2d28.5975663" target="_blank" class="biliboard4 repeating4" style="margin-top:10%;">Get Direction
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="right_map">
                <div class="location_image">
                    <!--<img src="{{ URL::asset('public/img/location.png') }}" class="img-responsive">-->
                    <iframe width="100%" height="553" src="https://maps.google.com/maps?width=100%&amp;height=553&amp;hl=en&amp;q=1985%20Cedar%20Bridge%20Ave.%2C%20Suite%203%20Lakewood%2C%20NJ%2008701+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                    </iframe>
                </div>
            </div>
        </div>
           <?php/* @auth("admin")
            You're an admin!
            @endauth

            @auth("company")
            You're a company!
            @endauth

            @auth("web")
            You're not logged in!
            @endauth
*/?>        <footer>
            <div class="contact_footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="contact_address">
                                <h3>Contact Us</h3>
                                <div class="contact_person">
                                    <p><span>Toll-Free : </span>123-456-7890</p>
                                    <p><span>Phone : </span>123-456-7890</p>
                                    <p><span>Email : </span> office@selecome.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="contact_address">
                                <h3>Corporate Headquarters</h3>
                                <div class="contact_person">
                                    <p>1985 Cedar Bridge Ave., Suite 3 Lakewood, NJ 08701</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="social_links">
                                <div class="">
                                    <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="footer_bottom">
                                <div class="links_nav">
                                    <ul>
                                        <li><a href="">Careers</a></li>
                                        <li><a href="">Contact Us</a></li>
                                        <li><a href="">Site Map</a></li>
                                        <li><a href="">Privacy Policy</a></li>
                                        <li><a href="">Terms of Use</a></li>
                                    </ul>
                                </div>
                                <p>Copyright © 2018 Selecome | All rights reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <section class="earn-point">
            <a href="#">
                <img src="{{ URL::asset('/img/earn-point-img.png') }}">
                  <!-- <p class="ref_para"><span>Refer</span> <br/>a company or <br/> buyer and earn <br/>up to N100,000 <br/><span>($300)</span> per referral.</p> -->
            </a>
        </section>
        
        <script type="text/javascript" src="{{ URL::asset('/js/side-navbar.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/bootstrap-select.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-ui.min.js')}}"></script> 
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-ui.tabs.neighbors.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/input-bx.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery.magicsearch.js')}}"></script>
         @if(request()->route('email_token') != '')
         <script>
         $(document).ready(function () {
            $('#loginmodel').modal('show');
         });
         document.getElementById("resetPassword").click();
         </script>
         @endif
        <script>
        

        //Ajax Login Setup
            $("#loginForm").submit(function(stay){
                $('#signinerror_email').html('');
                $('#signinerror_password').html('');
                $('#messageBody').html('');
               // $('#userNotApplicableMsg').html('');
                //$('#userInvalid').html('');
               // $('#loginMsg').html('');

            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/login",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#signinerror_'+key).html(val);
                    });
                }
                

                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-success">'+data.loginMsg+'</div>');
                    //$('#loginMsg').html(data.loginMsg);
                    window.location.href = data.loginurl;
                }
                if(data.status == 2){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.userNotApplicableMsg+'</div>');
                    //$('#userNotApplicableMsg').html(data.userNotApplicableMsg);
                }
                if(data.status == 3){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.userInvalid+'</div>');
                    //$('#userInvalid').html(data.userInvalid);
                }
            },
            });
            stay.preventDefault(); 
            });




         //Ajax Registeration Setup
         $("#registerForm").submit(function(stay){
            $('#signuperror_first_name').html('');
            $('#signuperror_last_name').html('');
            $('#signuperror_phone').html('');
            $('#signuperror_email').html('');
            $('#signuperror_password').html('');
            $('#signuperror_role_id').html('');
             $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/register",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#signuperror_'+key).html(val);
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-success">OTP Sent to your email Please Verify..</div>');
                    document.getElementById("registerForm").style.display = "none";
                    document.getElementById("otpForm").style.display = "block";
                    document.getElementById("user_email").value = data.otpMsg;
                   // $('#otpMsg').html(data.otpMsg);
                    console.log(data.otpMsg);
                    //$('#loginMsg').html(data.loginMsg);
                    //window.location.href = data.loginurl;
                }

            },
            });
            stay.preventDefault(); 
            });    


//Ajax Registeration Setup
$("#otpForm").submit(function(stay){
            $('#otperror_otp_post').html('');
            $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/otp-verification",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#otperror_'+key).html(val);
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.otpPost+'</div>');
                     // $('#otpMsg').html(data.otpMsg);
                    console.log(data.otpPost);
                    //$('#loginotpMsg').html(data.loginotpMsg);
                    //window.location.href = data.loginotpurl;
                }
                if(data.status == 2){
                    $("#messageBody").append('<div class="alert alert-success">'+data.otpPost+'</div>');
                    // $('#otpMsg').html(data.otpMsg);
                    console.log(data.otpPost);
                    //$('#loginotpMsg').html(data.loginotpMsg);
                    window.location.href = data.otpRedirect;
                }

            },
            });
            stay.preventDefault(); 
            });    


             //Ajax Forgot Password Setup
         $("#forgotpasswordForm").submit(function(stay){
            $('#forgotpassworderror_email').html('');
            $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company') }}/forgot-password",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    $('#forgotpassworderror_'+key).html(val);
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-danger">'+data.usernotvalid+'</div>');
                 }
                if(data.status == 2){
                    $("#messageBody").append('<div class="alert alert-success">'+data.successForgotPass+'</div>');
                 }

                
        
            },
            });
            stay.preventDefault(); 
            }); 


               //Ajax Reset Password Setup
         $("#resetpasswordForm").submit(function(stay){
            $('#errorpassworderror_password').html('');
            $('#messageBody').html('');
           
            var formdata = $(this).serialize(); // here $(this) refere to the form its submitting
            $.ajax({
            type: 'POST',
            url: "{{ url('/company/reset-password/'.$email_token) }}",
            data: formdata, // here $(this) refers to the ajax object not form
            success: function (data) {
            console.log(data.status);
            //here i will print the error messages
                if(data.status == 0){
                    $.each(data.errors, function(key, val) {
                    console.log($('#errorpassworderror_'+key).html(val));
                    });
                }
                if(data.status == 1){
                    $("#messageBody").append('<div class="alert alert-success">'+data.successPasswordReset+'</div>');
                 }
               

                
        
            },
            });
            stay.preventDefault(); 
            });     
            

         $(document).ready(function () {
            //Script to generate OTP
            var x = Math.floor((Math.random() * 9999) + 1000);
            document.getElementById("otp").value = x;
         });
        </script>
         
        <script>
            function selectBox() {
                $('.selectpicker').selectpicker();
            }
        </script>
        
        <script>
            $(document).ready(function () {
                
                $('.dropdown-submenu a.test').on("click", function (e) {
                    $(this).next('ul').toggle();
                    e.stopPropagation();
                    e.preventDefault();
                });
            });
        </script>
        <script type="text/javascript">
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 6
                    }
                }
            })
        </script>
    </body>
</html>
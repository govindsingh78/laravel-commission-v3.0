@extends('layouts.default')
@section('content')

<?php
    $settings = \App\Helpers\BasicFunction::getSettings();
?>

@if(!empty($sliders))
<section class="bannerpart">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner">
            @if(!empty($sliders))
                @foreach ($sliders as $key => $slider)
                    <div class="item banr_img_{{$key}}">
                        <img src="{{asset(SLIDER_IMAGES_URL . $slider->image)}}" alt="banner-img"/>
                    </div> 
                @endforeach
            @endif
        </div>

        <div class="banner-cng">
            <ul class="nav nav-pills nav-justified">
                @if(!empty($sliders))
                    @foreach ($sliders as $key => $slider)
                        <li data-target="#myCarousel" data-slide-to="{{$key}}" class="banner_list_{{$key}}"><a href="#">{{$slider->title}}</a></li>
                    @endforeach
                @endif
            </ul>
        </div>
        
        <div class="search-nav">
            <div class="container">
                <row>
                    <div class="bannerheading">
                        <h2 class="banner-h2"> {{$settings->site_slider_title}} </h2>
                        <h4 class="banner-h4"> {{$settings->site_slider_description}} </h4>
                    </div>
                </row>
                <row>
                    <div class="search-filter">
                        <form class="form-group">
                            <ul>                    
                                <li id="main_li">
                                    <select class="selectpicker" id="i_am_select" title="I am a...">
                                        <option value="Sales Agents">Sales Agents</option>
                                        <option value="Company">Company</option>
                                        <option value="Buyer">Buyer</option>
                                    </select>
                                </li>

                                <li class="div_looking">
                                    <select class="selectpicker" title="Looking For...">

                                    </select>
                                </li>

                                <li>
                                    <div class="">
                                        <input class="magicsearch" id="basic" placeholder="Select Industry...">
                                    </div>
                                </li>

                                <li>
                                    <div class="">
                                        <input class="magicsearch" id="productsearch" placeholder="Select Products / Services...">
                                    </div>
                                </li>

                                <li class="div_commision_level hide" style="margin-top: 10px;">
                                    <select class="selectpicker" title="Select Commission Level...">
                                        <option value="Standard">Standard</option>
                                        <option value="Premium">Premium</option>
                                        <option value="Premium Plus">Premium Plus</option>
                                    </select>
                                </li>

                                <li> 
                                    <button type="submit" name="Search" class="ripple snd-valu"> Go </button>
                                </li>

                            </ul>
                        </form>
                    </div>
                </row>
            </div>
        </div>
    </div>
</section>
@endif

@if(!empty($why))
<section class="why_us_sec">
    <div class="container">
        <row>
            <h3 class="h3-heading"> {{$why->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$why->description}}</p>
            </span>
        </row>

        <row>
            @if(!empty($why->children))
                @foreach ($why->children as $children)
                    @if($children->status === 1)
                        <div class="col-md-4">
                            <div class="why-me">
                                <div class="whyme-infograph">
                                    <img src="{{asset(CMS_IMAGES_URL . $children->cms_image)}}" alt="money bank"/>
                                </div>
                                <div class="why-me-dtl">
                                    <h4> {{$children->title}} </h4>
                                    <p>{{$children->description}}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </row>
    </div>
</section>
@endif

@if(!empty($howitworks))
<section class="howit_wrok_sec">
    <div class="container">
        <row>
            <h3 class="h3-heading"> {{$howitworks->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$howitworks->description}}</p>
            </span>
        </row>

        <row>
            <div class="howit_wrok">
                <div id="tabs">
                    @if(!empty($howitworks->children))
                        @foreach ($howitworks->children as $key => $children)
                            @if($children->status === 1)
                                <?php $key = $key + 1; ?>
                                <div id="tabs-{{$key}}" class="our-work">
                                    <div class="col-md-6">
                                        <img src="{{asset(CMS_IMAGES_URL . $children->cms_image)}}" alt="salaes-agent"/>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="tab-heading"> 
                                            {{$children->title}} 
                                        </span>
                                        <p>{{$children->description}}</p>
                                        <div class="it-work">
                                            <div class="raised it-work-btn-sec">
                                                <button class="ripple stated-btn"> Get Started </button>
                                            </div>

                                            <div class="raised it-work-btn-sec">
                                                <button class="ripple stated-btn"> Contact Us </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                    
<!--                    <div id="tabs-2" class="our-work">
                        <div class="col-md-6">
                            <img src="{{ URL::asset('public/img/sales-agent.png') }}" alt="salaes-agent">
                        </div>
                        <div class="col-md-6">
                            <span class="tab-heading"> Sales Agent </span>
                            <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Maecenas feugiat, tellus pellentesque pretium posuere.Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                            <div class="it-work">
                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Get Started </button>
                                </div>

                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Contact Us </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div id="tabs-3" class="our-work"> 
                        <div class="col-md-6">
                            <img src="{{ URL::asset('public/img/sales-agent.png') }}" alt="salaes-agent">
                        </div>
                        <div class="col-md-6">
                            <span class="tab-heading"> REFER & EARN </span>
                            <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Maecenas feugiat, tellus pellentesque pretium posuere.Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                            <div class="it-work">
                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Get Started </button>
                                </div>

                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Contact Us </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div id="tabs-4" class="our-work"> 
                        <div class="col-md-6">
                            <img src="{{ URL::asset('public/img/sales-agent.png') }}" alt="salaes-agent">
                        </div>
                        <div class="col-md-6">
                            <span class="tab-heading"> LOYALTY POINTS </span>
                            <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Maecenas feugiat, tellus pellentesque pretium posuere.Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                            <div class="it-work">
                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Get Started </button>
                                </div>

                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Contact Us </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div id="tabs-5" class="our-work"> 
                        <div class="col-md-6">
                            <img src="{{ URL::asset('public/img/sales-agent.png') }}" alt="salaes-agent">
                        </div>
                        <div class="col-md-6">
                            <span class="tab-heading"> BUY EASY </span>
                            <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Maecenas feugiat, tellus pellentesque pretium posuere.Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                            <div class="it-work">
                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Get Started </button>
                                </div>

                                <div class="raised it-work-btn-sec">
                                    <button class="ripple stated-btn"> Contact Us </button>
                                </div>

                            </div>
                        </div>
                    </div>-->

                    <ul>
                        @if(!empty($howitworks->children))
                            @foreach ($howitworks->children as $key => $children)
                                @if($children->status === 1)
                                    <?php $key = $key + 1; ?>
                                    <li class="tab{{$key}} ripple"><a href="#tabs-{{$key}}">{{$children->title}}</a></li>
                                @endif
                            @endforeach
                        @endif
<!--                        <li class="tab2 ripple"><a href="#tabs-2">Sales Agent</a></li>
                        <li class="tab3 ripple"><a href="#tabs-3">REFER & EARN</a></li>
                        <li class="tab4 ripple"><a href="#tabs-4">LOYALTY POINTS </a></li>
                        <li class="tab5 ripple"><a href="#tabs-5">BUY EASY</a></li>-->
                    </ul>
                    <button class="prev"><i class="fa fa-chevron-left"></i></button>
                    <button class="next"><i class="fa fa-chevron-right"></i></button>
                </div>
            </div>
        </row>
    </div>
</section>
@endif

<section class="pricing-sec">
    <div class="container">
        @if(!empty($pricing))
        <row>
            <h3 class="h3-heading"> {{$pricing->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$pricing->description}}</p>
            </span>
        </row>
        @endif

        <row>
            <div class="our-pricing">
                <div class="col-md-4">
                    <div class="price-tab mrg-top">
                        <div class="pricing-offer-hd">
                            Standard
                        </div>
                        <div class="pricing-offer-rat">
                            Free
                        </div>
                        <div class="pricing-offer-rat-dtl">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </div>
                        <div class="raised pricing-buy">
                            <button class="ripple pricing-buy-btn">Buy Now </button>
                        </div>
                    </div>
                    <div class="price-packeg-dtl">
                        <h3> Includes </h3>
                        <ul>
                            <li> Unlimited video emails up to 2500 contacts <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Gmail app & Chrome extension <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Mobile app for iOS & Android devices <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Tracking your opportunities with opens, clicks, plays <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Group coaching session <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="price-tab">
                        <div class="price-offer">
                            Most popular
                        </div>
                        <div class="pricing-offer-hd">
                            Premium
                        </div>
                        <div class="pricing-offer-rat">
                            <sup> $ </sup> 52 <sup> / month </sup>
                        </div>
                        <div class="pricing-offer-rat-dtl">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </div>
                        <div class="raised pricing-buy">
                            <button class="ripple pricing-buy-btn active">Buy Now </button>
                        </div>
                    </div>
                    <div class="price-packeg-dtl">
                        <h3> Includes </h3>
                        <ul>
                            <li> Premium – All standard features pluss <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Gmail app & Chrome extension <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Mobile app for iOS & Android devices <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Tracking your opportunities with opens, clicks, plays <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Group coaching session <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="price-tab mrg-top">
                        <div class="pricing-offer-hd">
                            Premium Plus
                        </div>
                        <div class="pricing-offer-rat">
                            <sup> $ </sup> 120 <sup> / month </sup>
                        </div>
                        <div class="pricing-offer-rat-dtl">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </div>
                        <div class="raised pricing-buy">
                            <button class="ripple pricing-buy-btn">Buy Now </button>
                        </div>
                    </div>
                    <div class="price-packeg-dtl">
                        <h3> Includes </h3>
                        <ul>
                            <li> Premium Plus – All Premium features 
                                plus <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Gmail app & Chrome extension <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Mobile app for iOS & Android devices <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Tracking your opportunities with opens, clicks, plays <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                            <li>Group coaching session <a href="#"><i class="fa fa-info-circle"></i></a> </li>
                        </ul>
                    </div>
                </div>
            </div>  
        </row>
    </div>
</section>

<section class="salecom-blg">
    <div class="container">
        @if(!empty($blog))
        <row>
            <h3 class="h3-heading">  {{$blog->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$blog->description}}</p>
            </span>
        </row>
        @endif

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sale-blg-sec">
                        <div id="Carousel" class="carousel slide">
                            <!-- Carousel items -->
                            <div class="carousel-inner">

                                <div class="item active">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-1.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-2.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-3.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/1 (1).png') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 

                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div><!--.row-->
                                </div><!--.item-->

                                <div class="item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-1.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-2.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-3.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/1 (1).png') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 

                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div><!--.row-->
                                </div><!--.item-->

                                <div class="item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-1.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-2.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/blog-3.jpg') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="cmp-blog">
                                                <a href="#" class="thumbnail">
                                                    <img src="{{ URL::asset('public/img/1 (1).png') }}" alt="Image" style="max-width:100%;">
                                                    <h4>Investment Strategy</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 

                                                    <div class="blog-date"><i class="fa fa-clock-o"></i> Sunday, 22 dec 2018 </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div><!--.row-->
                                </div><!--.item-->

                            </div><!--.carousel-inner-->
                            <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
                            <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
                        </div><!--.Carousel-->
                    </div>
                </div>
            </div>
        </div><!--.container-->

    </div>
</section>

<section class="buy-easy">
    @if(!empty($settings->site_advt_image))
        <img src="{{asset(SITE_IMAGES_URL . $settings->site_advt_image)}}" alt="logo">
    @else
        <img src="{{ URL::asset('public/img/BUY-EASY.jpg') }}" alt="buy-easy">
    @endif
</section>

<section class="premium-partners">
    <div class="container">
        @if(!empty($prem_part))
        <row>
            <h3 class="h3-heading"> {{$prem_part->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$prem_part->description}}</p>
            </span>
        </row>
        @endif

        <row>
            <div class="our-partners">
                <div class="carousel-wrap">
                    <div class="owl-carousel">

                        <div class="item">
                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-1.png') }}"> 
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="1">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat! <a class="read-more-hide hide" href="#" more-id="1">Read Less</a> </span> </p>
                            </div>

                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-6.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="2">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="2">Read Less</a> </span> </p>
                            </div>
                        </div> 

                        <div class="item">
                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-2.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="3">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="3">Read Less</a> </span> </p>
                            </div>

                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-7.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="4">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="4">Read Less</a> </span> </p>
                            </div>
                        </div> 

                        <div class="item">
                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-3.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="5">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!   <a class="read-more-hide hide" href="#" more-id="5">Read Less</a> </span> </p>
                            </div>

                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-8.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="6">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="6">Read Less</a> </span> </p>
                            </div>
                        </div> 

                        <div class="item">
                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-4.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="7">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="7">Read Less</a>  </span> </p>
                            </div>

                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-9.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="8">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="8">Read Less</a> </span> </p>
                            </div>
                        </div> 

                        <div class="item">
                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-5.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="9">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="9">Read Less</a> </span> </p>
                            </div>

                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-10.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="10">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="10">Read Less</a> </span> </p>
                            </div>
                        </div> 

                        <div class="item">
                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-4.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="11">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="11">Read Less</a> </span> </p>
                            </div>

                            <div class="permium-logo">
                                <img src="{{ URL::asset('public/img/free-logo-3.png') }}">
                                <strong> Company Name </strong>
                                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> <a class="read-more-show hide" href="#" id="12">Read More</a> <span class="read-more-content">Suspendisse! Rutrum cupidatat!  <a class="read-more-hide hide" href="#" more-id="12">Read Less</a> </span> </p>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </row>

    </div>
</section>

<section class="contact-form">
    <div class="cloud-effect">
        <div id="clouds">
            <div class="cloud x1"></div>
            <!-- Time for multiple clouds to dance around -->
            <div class="cloud x2"></div>
            <div class="cloud x3"></div>
            <div class="cloud x4"></div>
            <div class="cloud x5"></div>
        </div>
    </div>
    <div class="container">
        @if(!empty($contactus))
        <row>
            <h3 class="h3-heading"> {{$contactus->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$contactus->description}}</p>
            </span>
        </row>
        @endif

        <row>
            <div class="our-contactus">
                <div class="col-md-7">
                    <h3 class="h3">Need More Information?</h3>
                    <h4>GIVE US A CALL or SEND US A MESSAGE</h4>
                    <span><i class="fa fa-phone"></i> +91 789-456-1230</span>
                    <span><i class="fa fa-envelope-o"></i> demo_Selecoms@mailid.com</span>
                    <div class="form-dtl">
                        <form class="form-group">
                            <ul>
                                <li>
                                    <div class="group">
                                        <input type="text" class="form-control"><span class="highlight"></span><span class="bar"></span>
                                        <label>Name</label>
                                    </div>
                                </li>

                                <li>
                                    <div class="group">
                                        <input type="email" class="form-control"><span class="highlight"></span><span class="bar"></span>
                                        <label>Email Id</label>
                                    </div>
                                </li>

                                <li>
                                    <div class="group">
                                        <input type="email" class="form-control"><span class="highlight"></span><span class="bar"></span>
                                        <label>Phone Number</label>
                                    </div>
                                </li>

                                <li>
                                    <div class="group">

                                        <select title="Select Reason..."> 
                                            <option>Value 1</option>
                                            <option>Value 2</option>
                                        </select>

                                    </div>
                                </li>

                                <li>
                                <li>
                                    <div class="group">
                                        <input type="email" class="form-control"><span class="highlight"></span><span class="bar"></span>
                                        <label>Comment</label>
                                    </div>
                                </li>
                                </li>

                            </ul>
                            <div class="aform-btn">
                                <div class="button raised clickable submit">
                                    <input class="toggle" type="checkbox"/>
                                    <div class="anim"></div><span>Submit</span>
                                </div>

                                <div class="button raised clickable joinus">
                                    <input class="toggle" type="checkbox"/>
                                    <div class="anim"></div><span>Join us</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6030.418742494061!2d-111.34563870463673!3d26.01036670629853!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1471908546569" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div
                    ></div>
            </div>
        </row>            
    </div>
</section>

@if(!empty($aboutcompany))
<section class="about-salecoms">
    <div class="container">
        <row>
            <h3 class="h3-heading"> {{$aboutcompany->title}} </h3> 
            <span class="h3-subheading">
                <p>{{$aboutcompany->description}}</p>
            </span>
        </row>

        <row>
            @if(!empty($aboutcompany->children))
                @foreach($aboutcompany->children as $children)
                    @if($children->status === 1)
                        <div class="col-md-4 about-me">
                            <a href="#">
                                <div class="about-me-pic">
                                    <img src="{{asset(CMS_IMAGES_URL . $children->cms_image)}}" alt="company"> 
                                </div> 
                                <div class="about-me-dtl">
                                    <h2>{{$children->title}}</h2>
                                    <p>{{$children->description}}</p>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            @endif
            
<!--            <div class="col-md-4 about-me">
                <a href="#">
                    <div class="about-me-pic">
                        <img src="{{ URL::asset('public/img/sales-agent-2.png') }}" alt="company"> 
                    </div>
                    <div class="about-me-dtl">
                        <h2>I'm a sales agent</h2>
                        <p>Looking for opportunities</p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 about-me">
                <a href="#">
                    <div class="about-me-pic">
                        <img src="{{ URL::asset('public/img/buyer.png') }}" alt="company"> 
                    </div>
                    <div class="about-me-dtl">
                        <h2>Are you a buyer?</h2>
                        <p>Looking for where to buy a product or service?</p>
                    </div>
                </a>
            </div>-->
        </row>
    </div>
</section>
@endif
@stop
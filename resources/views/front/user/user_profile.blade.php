<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.front.head')


</head>

<body>
  <div id="page-wraper">
    @include('includes.front.header')
    <div class="header-landing">
}
<div class="clearfix"></div>
<div class="chating_cover">
  <div class="container">
  <div class="row">
  <div class="col-md-2">
  <div class="profile_pick"><img src="{{$user_data['profile_image']}}" alt=""/></div></div>
  <div class="col-md-10">
  <div class="user_details"><h1>{{$user_data['first_name']}}</h1>
  <h3><i class="fa fa-map-marker" aria-hidden="true"></i>{{$user_data['address']}}</h3>
  <h3><i class="fa fa-envelope" aria-hidden="true"></i>{{$user_data['email']}}</h3>
  <h2><i class="fa fa-phone" aria-hidden="true"></i>+91-{{$user_data['phone']}}</h2>
  </div>
  @if($user_data['id']!=$user['id'])
    @if($isfriend['status']==1)
        <div class="dropdown">
        <button onclick="myFunction()" class="dropbtn"><i class="fa fa-check" aria-hidden="true"></i> Friend</button>
          <div id="myDropdown" class="dropdown-content">
            <a href="#home">Get notification</a>
            <a href="#about">Unfriend</a>
            <a href="#contact">block</a>
          </div>
        </div>
        <div class="icon_button"><i class="fa fa-envelope" aria-hidden="true"></i>Message</div>
    @elseif($isfriend['status']==2)
        @if($isfriend['from_id']==$user['id'] && $isfriend['status']==2)
            <div class="icon_button"><i class="fa fa-user" aria-hidden="true"></i>Request Sent</div>
            <div class="icon_button"><i class="fa fa-envelope" aria-hidden="true"></i>Message</div>
            <div class="icon_button blockUser"><i class="fa fa-ban" aria-hidden="true"></i>Block</div>
        @else
            <div class="icon_button acceptFriendship" request_id="{{$user_data['isfriend']['id']}}"><i class="fa fa-user" aria-hidden="true"></i>Accept Request</div>
            <div class="icon_button rejectFriendship" request_id="{{$user_data['isfriend']['id']}}"><i class="fa fa-times" aria-hidden="true"></i>Reject Request</div>
            <div class="icon_button"><i class="fa fa-envelope" aria-hidden="true"></i>Message</div>
            <div class="icon_button blockUser"><i class="fa fa-ban" aria-hidden="true"></i>Block</div>
        @endif
    @else
        <div class="icon_button addFriend" friend_id="{{$user_data['id']}}"><i class="fa fa-plus" aria-hidden="true"></i>Add a Friend</div>
        <div class="icon_button"><i class="fa fa-envelope" aria-hidden="true"></i>Message</div>
        <div class="icon_button blockUser"><i class="fa fa-ban" aria-hidden="true"></i>Block</div>
    @endif
  @else
      <div class="icon_button"><i class="fa fa-envelope" aria-hidden="true"></i>Message (16)</div>
  @endif

  
  </div>
  </div>
  </div>
  <div class="clearfix"></div>
  </div>
  <div class="container">
  <div class="row">
<div class="col-lg-3 ">
<div class="friends_chat"><div class="heding-de">Friends ({{$friends_count}})
</div>
@foreach($friends as $friend)
<div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i><img src="{{$friend['friends']['profile_image']}}" alt=""/></i>
{{$friend['friends']['first_name']}}&nbsp; {{$friend['friends']['last_name']}}<div class="online"></div></a>
                        </h4>
                    </div>

            </div>
          
            </div>

         @endforeach               
            
            
            
            
            </div></div>


       @include('includes.front.user_timeline')
      @include('includes.front.latest_products')
    </div>
 
 </div>
</div>
</div>
@include('includes.front.footer_scripts')
</body>
</html>
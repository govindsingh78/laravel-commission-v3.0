<!DOCTYPE html>
<html >
<head>
	@include('includes.front.head')
</head>
<body>
	<div id="page-wraper1">
		@include('includes.front.header')
		<div class="container">
			<div class="page-content page-content-popup">
			    <div class="page-fixed-main-content"> 
      <div class="row">
        <div class="col-md-12 no_pad"> 
          <div class="profile-sidebar"> 
            <div class="portlet light profile-sidebar-portlet"> 
              <div class="profile-userpic"> <img src="{{$user['profile_image']}}" class="img-responsive" alt=""> </div>
              <div class="profile-usertitle">
                <div class="profile-usertitle-name">{{$user['first_name']}}&nbsp;{{$user['last_name']}} </div>
                <div class="profile-usertitle-job"><i class="fa fa-map-marker" aria-hidden="true"></i>{{$user['address']}}</div>
              </div>
            </div>
            <div class="portlet light"> 
              <div class="row list-separated profile-stat">
                <div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="uppercase profile-stat-title"> 256</div>
                  <div class="uppercase profile-stat-text"> Friends </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="uppercase profile-stat-title">  2.6k  </div>
                  <div class="uppercase profile-stat-text">Follower  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="uppercase profile-stat-title"> 1.2k  </div>
                  <div class="uppercase profile-stat-text"> Following </div>
                </div>
              </div>
              <div class="pad_div_2 bor-top">
                <h4 class="profile-desc-title">About {{$user['first_name']}}&nbsp;{{$user['last_name']}} </h4>
                <span class="profile-desc-text"> {{$user['about_user']}} </span>
                <div class="margin-top-20 profile-desc-link"> <i class="fa fa-globe"></i> <a href="#">{{$user['email']}}</a> </div>
                <div class="margin-top-20 profile-desc-link"> <i class="fa fa-twitter"></i> <a href="#">@kathryn</a> </div>
                <div class="margin-top-20 profile-desc-link"> <i class="fa fa-facebook"></i> <a target="_blank" href="{{$user['facebook_url']}}">{{$user['first_name']}}</a> </div>
              </div>
            </div>
          </div>
          <div class="profile-content">
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light">
                  @if(Session::has('passwordMessage'))
                    <div class="successMessage">
                          {{Session::get('passwordMessageError')}}
                    </div>
                  @endif
                  @if(Session::has('passwordMessage'))
                    <div class="errorMessage">
                          {{Session::get('passwordMessageError')}}
                    </div>
                  @endif
                  <div class="portlet-title tabbable-line">
               
                    <ul class="nav nav-tabs">
                      <li class="active"> <a href="#tab_1_1" data-toggle="tab">Personal Info</a> </li>
                      <li> <a href="#tab_1_2" data-toggle="tab">Change Avatar</a> </li>
                      <li> <a href="#tab_1_3" data-toggle="tab">Change Password</a> </li>
                      <li> <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a> </li>
                    </ul>
                  </div>
                  <div class="portlet-body">
                    <div class="tab-content"> 
                      <div class="tab-pane active" id="tab_1_1">
                        {{Form::open(array('url'=>'updateprofile','method'=>'post'))}}
                          <div class="form-group">
                            <label class="control-label">First Name</label>
                            <input placeholder="Kathryn" value="{{$user['first_name']}}" name="first_name" class="form-control" type="text">
                          </div>
                          <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input placeholder="Rodriguez" value="{{$user['last_name']}}" name="last_name" class="form-control" type="text">
                          </div>
                          <div class="form-group">
                            <label class="control-label">Mobile Number</label>
                            <input placeholder="+91-9876543210" name="phone" value="{{$user['phone']}}" class="form-control" type="text">
                          </div>
                          <div class="form-group">
                            <label class="control-label">Interests</label>
                            <input placeholder="Lorem Ipsum" class="form-control" value="{{$user['user_interests']}}" name='user_interests' type="text">
                          </div>
                          <div class="form-group">
                            <label class="control-label">Occupation</label>
                            <input placeholder="Model" class="form-control" value="{{$user['occupation']}}" name='occupation' type="text">
                          </div>
                          <div class="form-group">
                            <label class="control-label">About you</label>
                            <textarea class="form-control" rows="3" name="about_user" placeholder="Something about you!">{{$user['about_user']}}</textarea>
                          </div>
                          <div class="form-group">
                            <label class="control-label">Website Url</label>
                            <input placeholder="http://www.demo.com" value="{{$user['company_url']}}" name="company_url" class="form-control" type="text">
                          </div>
                          <div class="margiv-top-10"> <button type="submit" class="btn save_button"> Save Changes </button> 
                          <button type="reset" class="btn cancel_buttion"> Cancel </button> </div>
                        {{Form::close()}}
                      </div>
                      <div class="tab-pane" id="tab_1_2">
                        {{Form::open(array('url'=>'updateprofilepicture','method'=>'post','enctype'=>'multipart/form-data'))}}
                          <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-new thumbnail" style="width: 130px; height: 130px;"> <img src="{{$user['profile_image']}}" alt="" id='profile_image_preview'> </div>
                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                              </div>
                              <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select image </span> <span class="fileinput-exists"> Change </span>
                                {{Form::file('profile_image',null)}}
                                </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                            </div>
                          </div>

                          <div class="margin-top-10"> {{Form::button('Submit',array('type'=>'submit','class'=>'btn save_button'))}}
                           <button type="reset" class="btn cancel_buttion"> Cancel </button></div>
                        {{Form::close()}}
                      </div>
                      <div class="tab-pane" id="tab_1_3">
                        {{Form::open(array('url'=>'changepassword','method'=>'post'))}}
                          <div class="form-group">
                            <label class="control-label">Current Password</label>
                            <input class="form-control" type="password" name="current_password">
                            <input type="hidden" name='id' value="{{$user['id']}}">
                          </div>
                          <div class="form-group">
                            <label class="control-label">New Password</label>
                            <input class="form-control" type="password" name="password">
                          </div>
                          <div class="form-group">
                            <label class="control-label">Re-type New Password</label>
                            <input class="form-control" type="password" name="password_confirmation">
                          </div>
                          <div class="margin-top-10"> <button type="save" class="btn save_button"> Change Password </button> 
                          <button type="reset" class="btn cancel_buttion"> Cancel </button> </div>
                       {{Form::close()}}
                      </div>
                      <div class="tab-pane" id="tab_1_4">
                        {{Form::open(array('url'=>'updatepreferences','method'=>'post'))}}
                          <table class="table table-light">
                            <tbody>
                              <tr>
                                <td> Who can find you? </td>
                                <td>
                                  <select id="year" name="can_find_user">
                                     @if($preferences[0]->can_find_user=='Public')
    <option value="Public" selected="selected"><i class="fa fa-globe" aria-hidden="true"></i>
Public</option>
@else
<option value="Public"><i class="fa fa-globe" aria-hidden="true"></i>
Public</option>
@endif
 @if($preferences[0]->can_find_user=='Friends')
    <option value="Friends" selected="selected" ><i class="fa fa-globe" aria-hidden="true"></i>
Friends </option>
@else
  <option value="Friends"><i class="fa fa-globe" aria-hidden="true"></i>
Friends </option>
@endif
@if($preferences[0]->can_find_user=='Only Me')
    <option value="Only me" selected="selected"><i class="fa fa-globe" aria-hidden="true"></i>
Only me </option>
@else
  <option value="Only me"><i class="fa fa-globe" aria-hidden="true"></i>
Only me </option>
@endif
    </select>
                                </td>
                              </tr>
                              <tr>
                                <td> Who Can View Your Posts?</td>
                                <td> 
                                <select id="mounth" name="can_view_profile">
@if($preferences[0]->can_view_profile=='Public')
    <option value="Public" selected="selected">Public<i class="fa fa-globe" aria-hidden="true"></i></option>
@else
    <option value="Public">Public<i class="fa fa-globe" aria-hidden="true"></i></option>
@endif
@if($preferences[0]->can_view_profile=='Friends')
    <option value="Friends" selected="selected"><i class="fa fa-globe" aria-hidden="true"></i>
Friends </option>
@else
  <option value="Friends"><i class="fa fa-globe" aria-hidden="true"></i>
Friends </option>
@endif
@if($preferences[0]->can_view_profile=='Only me')    
    <option value="Only me" selected="selected"><i class="fa fa-globe" aria-hidden="true"></i>
Only me </option>
@else
    <option value="Only me"><i class="fa fa-globe" aria-hidden="true"></i>
Only me </option>
@endif
</select></td>
                              </tr>
                              <tr>
                              <td>Notifications Sound<p>A sound will be played each time you receive a new activity notification</p></td>
                               <td>  <div class="toggle-group">
    @if($preferences[0]->notification_sound==1)
    <input type="checkbox" id="on-off-switch4" checked="checked" name="notification_sound" tabindex="1">
    @else
    <input type="checkbox" id="on-off-switch4" checked="" name="notification_sound" tabindex="1">
    @endif
    <label for="on-off-switch4">
    </label>
    <div class="onoffswitch pull-right" aria-hidden="true">
        <div class="onoffswitch-label">
            <div class="onoffswitch-inner" id="notify_on"></div>
            <div class="onoffswitch-switch"></div>
        </div>
    </div>
</div></td>
                              </tr>
                              <tr>
                              <td>Notifications Email<p>We’ll send you an email to your account each time you receive a new 
activity notification</p></td>

                               <td>  <div class="toggle-group">
                                @if($preferences[0]->notification_email==1)  
    <input type="checkbox" id="on-off-switch3" checked="checked" name="notification_email" tabindex="1">
     @else
          <input type="checkbox" id="on-off-switch3" checked="checked" name="notification_email" tabindex="1">
    @endif
    <label for="on-off-switch3">
    </label>
    <div class="onoffswitch pull-right" aria-hidden="true">
        <div class="onoffswitch-label">
            <div class="onoffswitch-inner"></div>
            <div class="onoffswitch-switch"></div>
        </div>
    </div>
</div></td>
                              </tr>
                               <tr>
                              <td>Friend’s Birthdays<p>Choose wheather or not receive notifications about your friend’s 
birthdays on your newsfeed</p></td>

                                <td>  <div class="toggle-group">
                                  @if($preferences[0]->birthday_notify==1)  
    <input type="checkbox" id="on-off-switch2" checked="checked" name="birthday_notify" tabindex="1">
    @else
        <input type="checkbox" id="on-off-switch2" checked="" name="birthday_notify" tabindex="1">
    @endif
    <label for="on-off-switch2">
    </label>
    <div class="onoffswitch pull-right" aria-hidden="true">
        <div class="onoffswitch-label">
            <div class="onoffswitch-inner"></div>
            <div class="onoffswitch-switch"></div>
        </div>
    </div>
</div></td>
                              </tr>
                               <tr>
                              <td>Chat Message Sound<p>A sound will be played each time you receive a new message on 
an inactive chat window</p></td>

                                 <td>  <div class="toggle-group">
  @if($preferences[0]->chat_sound==1)                                  
    <input type="checkbox" checked="checked"  id="on-off-switch1" name="chat_sound" tabindex="1">
    @else
    <input type="checkbox" checked=""  id="on-off-switch1" name="chat_sound" tabindex="1">
    @endif
    <label for="on-off-switch1">
    </label>
    <div class="onoffswitch pull-right" aria-hidden="true">
        <div class="onoffswitch-label">
            <div class="onoffswitch-inner"></div>
            <div class="onoffswitch-switch"></div>
        </div>
    </div>
    
</div></td>
                              </tr>
                               <tr>
                             
                              </tr>
                            </tbody>
                          </table>
                          <div class="margin-top-10"> <button type="submit" class="btn save_button"> Save Changes </button>
                                                      <button type="reset" class="btn cancel_buttion"> Cancel </button> </div>
                        {{Form::close()}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
 
</div></div>
@include('includes.front.footer_scripts')
  @if($preferences[0]->chat_sound==0)
<script type="text/javascript">
    $("#on-off-switch1").trigger('click');
</script>
@endif
@if($preferences[0]->birthday_notify==0)
<script type="text/javascript">
    $("#on-off-switch2").trigger('click');
</script>
@endif
@if($preferences[0]->notification_email==0)
<script type="text/javascript">
    $("#on-off-switch3").trigger('click');
</script>
@endif
@if($preferences[0]->notification_sound==0)
<script type="text/javascript">
    $("#on-off-switch4").trigger('click');
</script>
@endif
</body>
</html>
@extends('layouts.default')
@section('content')
<div class="container">
    <div class="row">
  <div class="col-lg-12">
  <div class="serch_product">
  <form class="form-inline top-bar-search">
            <div class="deals"><input class="form-control" placeholder="Search By Deals/Coupon " type="text"></div>
            <div class="location"><input class="form-control fa-location-arrow" placeholder="Jaipur,Rajasthan " type="text"><i class="fa fa-map-marker" aria-hidden="true"></i>
</div>
            <div class="company"><input class="form-control fa-location-arrow" placeholder="Search By Company Name" type="text"></div>
<button class="search-btn" type="submit" value="/f002"><span class="fa fa-search"></span></button>
<!--<button class="search-btn hidden-sm-up" type="submit" value="/f002"><span>Search</span></button>
-->
          </form></div>
</div>
<div class="col-lg-3 ">
<div class="border_ro"><div class="heding-de">Categories

</div>
<div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-chain-broken" aria-hidden="true"></i>
All Retailers</a>
                        </h4>
                    </div>
                    <!--<div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="http://www.jquery2dotnet.com">All Retailers</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="http://www.jquery2dotnet.com">News</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-file text-info"></span><a href="http://www.jquery2dotnet.com">Newsletters</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-comment text-success"></span><a href="http://www.jquery2dotnet.com">Comments</a>
                                        <span class="badge">42</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>-->
                </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                      

                            <a href="#">  <i class="fa fa-star-o" aria-hidden="true"></i>Featured Gift Cards</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-child" aria-hidden="true"></i>
 Babies & Children</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-graduation-cap" aria-hidden="true"></i>
Back to school</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
Department Stores</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-gamepad" aria-hidden="true"></i>
Electronics & Gaming</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i><img src="img/icn-category-10.png" alt="icon" /></i>Fashion Accessories</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-medkit" aria-hidden="true"></i>
Health & Beauty</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i><img src="img/icn-category-12.png" alt="icon" /></i>Home Goods</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default"><div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i><img src="img/t-shirt.png" alt="icon" /></i>Men's Apparel</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i><img src="img/icn-category-28.png" alt="icon" /></i>New</a>
                        </h4>
                    </div>
                    
                </div>
<div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-cutlery" aria-hidden="true"></i>
Restaurants & Beverages</a>
                        </h4>
                    </div>
                    
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-futbol-o" aria-hidden="true"></i>
Sports & Outdoors</a>
                        </h4>
                    </div>
                    
                </div>    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-plane" aria-hidden="true"></i>
Travel</a>
                        </h4>
                    </div>
                    
                </div>  
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#"><i class="fa fa-female" aria-hidden="true"></i>
Women's Apparel</a>
                        </h4>
                    </div>
                    
                </div>          
                
 
          
            </div></div></div>
        <div class="col-lg-9"><aside>
          <div class="inner_product"><div class="product wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
            <div class="brand"> <img src="img/brand_1.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_2.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_3.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_4.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_5.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_6.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_1.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_2.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_3.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            <div class="brand"> <img src="img/brand_4.png" alt="image">
              <div class="clearfix"></div>
              <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Buy now</a> <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> </a> </div>
            
            <div class="clearfix"></div>
          </div></div>
        </aside></div>
    </div>
 
 </div>
@stop

@extends('layouts.default')
@section('content')
<div id="page-wraper1">
  <div class="clearfix"></div>
  <div class="container">
	<div class="card_2">
    <div class="card">
			<div class="container-fliud">
				<div class="wrapper row">
					<div class="preview col-md-4">
						
						<div class="preview-pic tab-content">
						  <div class="tab-pane active" id="pic-1"><img src="{{URL::asset('public/stuff/voucher_image')}}/{{$product['image']}}" alt="image"/></div>
                          </div>
						
					</div>
					<div class="details col-md-8">
						<h3 class="product-title">{{$product['title']}}</h3>
                        <h2><span>Category name:</span> {{$product['category']['name']}}</h2>
                       <div class="follow_type"> <h2><span>By</span> Acme Corporation </h2>
                        <div class="follow_button"><a href="#">follow<i class="fa fa-check" aria-hidden="true"></i></div></a></div>

							<div class="rating">
							<div class="stars">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
							<span class="review-no">41 reviews</span>
                            </div>
						<p class="product-description">{!!$product['description']!!} </p>
						<h4 class="price">current price: <span><i class="fa fa-inr"></i>&nbsp;{{$product['price']}}</span></h4>

						<!--<p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>-->
						<div class="action">
							<button class="add-to-cart btn btn-default addToCart" product_type="{{$product['entity_type']}}" product_id="{{$product['id']}}"  type="button">Buy for Yourself</button>
							<button class="add-to-cart btn btn-default" type="button">Send as a Gift</button>
							<button class="like btn btn-default addToWishlist" product_type="{{$product['entity_type']}}" product_id="{{$product['id']}}" type="button"><span class="fa fa-heart"></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="col-xs-12">
        <div class="card">
        <div class="select_design">
        <h4>1. Select a style for your gift card</h4>
<p><span>Design: </span>Happy Birthday (Cream Cake) </p></div>
        <ul class="preview-thumbnail nav nav-tabs">
						  <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="{{URL::asset('public/img/gift_4.png')}}" alt="image"/></a></li>
						  <li><a data-target="#pic-2" data-toggle="tab"><img src="{{URL::asset('public/img/gift_1.jpg')}}" alt="image"/></a></li>
						  <li><a data-target="#pic-3" data-toggle="tab"><img src="{{URL::asset('public/img/gift_2.jpg')}}" alt="image"/></a></li>
						  <li><a data-target="#pic-4" data-toggle="tab"><img src="{{URL::asset('public/img/gift_3.jpg')}}" alt="image"/></a></li>
						  <li><button class="view_more btn btn-default" type="button">view<br>
more</button>
</li>
						</ul>
                        <div class="select_design">
        <h4>2. Enter your details</h4>

        <div class="amount_tab">
        <ul>
        <li><p>Amount</p></li>
        <li><a>$500</a></li>
        <li><a>$1000</a></li>
        <li><a>$2000</a></li>
        <li><a>$5000</a></li>
        <li><a>$7500</a></li>
        <li><a>Other amount</a></li>

        </ul>
        <p><i class="fa fa-pencil" aria-hidden="true"></i>
The minimum amount $10</p>
        </div>
        <div class="amount_tab">
        <ul>
        <li><p>Quantity</p></li>
        <li> <input type="text" id="quantity" name="fname" placeholder="0"></li>
        </ul>
        </div><br>
        <button class="add-to-cart btn btn-default col-lg-2 addToCart" product_type="{{$product['entity_type']}}" product_id="{{$product['id']}}" type="button">Add to cart</button>
</div>
                        </div>
        
        </div>
        
        </div>
	</div>

@stop
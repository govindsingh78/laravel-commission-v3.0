@foreach($comments as $comment)
                                                    <div class="profile-n-post-info-name">
                                                        <h5><i class="fa fa-comment"></i>&nbsp; <a href="{{route('user_profile')}}" ><span class="p-p-name">{{$comment['users']['first_name']}}&nbsp;{{$comment['users']['last_name']}}</span> </a></h5>
                                                        <div class="profile-n-post-info-details">{{$comment['comment']}}</div>
                                                    </div>
                                                @endforeach
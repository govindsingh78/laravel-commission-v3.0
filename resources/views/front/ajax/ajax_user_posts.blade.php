                    @if(!empty($posts))
                    
                        @foreach($posts as $post)
                            @if($post['is_private']==1 && $post['user']['id']==$user['id'])
                            <div class="post-main wow fadeIn">
                                <div class="box">
                                    <div class="profile-n-post-info clearfix">
                                        @if($post['user']['profile_image']!=null)
                                        <div class="profile-n-post-info-profile-pic"> <img src="{{ $post['user']['profile_image'] }}" alt="image" /> </div>
                                        @endif
                                        @if($post['user']['profile_image']==null)
                                        <div class="profile-n-post-info-profile-pic"> <img src="{{ URL::asset('public/img/no-image.jpg')}}" alt="image" /> </div>
                                        @endif
                                        <div class="profile-n-post-info-name">
                                            <h5> <a href="{{route('user_profile')}}" ><span class="p-p-name">{{$post['user']['first_name']}}&nbsp;{{$post['user']['last_name']}}</span> </a></h5>

                                            <div class="profile-n-post-info-details"> <a href="javascript:void(0)">2 hrs <i class="fa fa-globe"></i></a> </div>


                                        </div>
                                        @if($user['id']==$post['user']['id'])
                                        <div class="profile-n-post-info-more">
                                            <div class="nav-item wow bounceIn" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: bounceIn;">
                                                <label class="dropdown">

                                                    <div class="dd-button">
                                                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                    </div>

                                                    <input class="dd-input" id="test" type="checkbox">

                                                    <ul class="dd-menu">
                                                        @if($post['is_private']==0)
                                                        <li><a href="javascript:void(0);" class="private-post" post-id="{{$post['id']}}" >Private</a>
                                                        </li>
                                                        @endif
                                                       @if($post['is_public']==0)
                                                        <li><a href="javascript:void(0);" class="public-post" post-id="{{$post['id']}}" >Public </a>
                                                        </li>
                                                        @endif
                                                        <li><a href="#">Anonymous</a>
                                                        </li>

                                                        <li class="divider"></li>
                                                    </ul>

                                                </label>
                                            </div>
                                            <a href="#"></a>
                                        </div>
                                        @endif
                                    </div>
                                    @if($post['image']!=null)
                                    <div class="post-media-main"> <img src="{{url('/public/post_images')}}/{{ $post['image']}}" alt="image" /> </div>
                                    @else
                                    <div class="post-media-main"> <img src="{{url('/public/img/no-image.jpg')}}" alt="image" /> </div>
                                    @endif
                                    <div class="pro_details">
                                        <h1>{{$post['title']}}</h1>
                                        <p>{!!$post['description']!!} </p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="like_comment_count">
                                        <ul>
                                            <li><a href="#" class="likes_{{$post['id']}}">{{$post['likes_count']}} Likes</a>
                                            </li>
                                            <li><a href="#" class="comment_{{$post['id']}}">{{$post['comments_count']}} Comments</a>
                                            </li>
                                            <li><a href="#">56 Share</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="like_comment">
                                        <ul>
                                            <li><a href="javascript:void(0);"><i class="fa fa-heart likePost" like="{{$post['id']}}" aria-hidden="true">&nbsp;Like</i></a>
                                            </li>
                                            <li><a href="javascript:void(0);"><i post_id="{{$post['id']}}" class="fa fa-comment" aria-hidden="true">&nbsp;Comment</i></a>
                                            <li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="post-react-main">
                                        <div class="commnet-box clearfix">
                                            <div class="commnet-box-profile-pic"> <img src="{{ $user['profile_image']}}" height="32" width="32" alt="profile-name" /> </div>
                                            <div class="commnet-box-commnt-write">
                                                <input type="text" class="user_comment" id="{{$post['id']}}" placeholder="Write a comment"/>
                                                <a href="javascript:void(0);" class="fa fa-smile-o cmnt-submit-btn"></a>
                                            </div>
                                            <div class="user-comments-box-toggle-{{$post['id']}} border-3-div" style="display:none;">
                                                @foreach($post['comments'] as $comment)
                                                    <div class="profile-n-post-info-name">
                                                        <h5><i class="fa fa-comment"></i>&nbsp; <a href="{{route('user_profile')}}" ><span class="p-p-name">{{$post['user']['first_name']}}&nbsp;{{$post['user']['last_name']}}</span> </a></h5>
                                                        <div class="profile-n-post-info-details">{{$comment['comment']}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($post['is_public']==1)
                            <div class="post-main wow fadeIn">
                                <div class="box">
                                    <div class="profile-n-post-info clearfix">
                                        @if($post['user']['profile_image']!=null)
                                        <div class="profile-n-post-info-profile-pic"> <img src="{{ $post['user']['profile_image'] }}" alt="image" /> </div>
                                        @endif
                                        @if($post['user']['profile_image']==null)
                                        <div class="profile-n-post-info-profile-pic"> <img src="{{ URL::asset('public/img/no-image.jpg')}}" alt="image" /> </div>
                                        @endif
                                        <div class="profile-n-post-info-name">
                                            <h5> <a href="{{route('user_profile')}}" ><span class="p-p-name">{{$post['user']['first_name']}}&nbsp;{{$post['user']['last_name']}}</span> </a></h5>

                                            <div class="profile-n-post-info-details"> <a href="javascript:void(0)">2 hrs <i class="fa fa-globe"></i></a> </div>


                                        </div>
                                        @if($user['id']==$post['user']['id'])
                                        <div class="profile-n-post-info-more">
                                            <div class="nav-item wow bounceIn" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: bounceIn;">
                                                <label class="dropdown">

                                                    <div class="dd-button">
                                                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                    </div>

                                                    <input class="dd-input" id="test" type="checkbox">

                                                    <ul class="dd-menu">
                                                        @if($post['is_private']==0)
                                                        <li><a href="javascript:void(0);" class="private-post" post-id="{{$post['id']}}" >Private</a>
                                                        </li>
                                                        @endif
                                                        @if($post['is_public']==0)
                                                        <li><a href="javascript:void(0);" class="public-post" post-id="{{$post['id']}}" >Public </a>
                                                        </li>
                                                        @endif
                                                        <li><a href="#">Anonymous</a>
                                                        </li>

                                                        <li class="divider"></li>
                                                    </ul>

                                                </label>
                                            </div>
                                            <a href="#"></a>
                                        </div>
                                        @endif
                                    </div>
                                    @if($post['image']!=null)
                                    <div class="post-media-main"> <img src="{{url('/public/post_images')}}/{{ $post['image']}}" alt="image" /> </div>
                                    @else
                                    <div class="post-media-main"> <img src="{{url('/public/img/no-image.jpg')}}" alt="image" /> </div>
                                    @endif
                                    <div class="pro_details">
                                        <h1>{{$post['title']}}</h1>
                                        <p>{!!$post['description']!!} </p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="like_comment_count">
                                        <ul>
                                            <li><a href="#" class="likes_{{$post['id']}}">{{$post['likes_count']}} Likes</a>
                                            </li>
                                            <li><a href="#" class="comment_{{$post['id']}}">{{$post['comments_count']}} Comments</a>
                                            </li>
                                            <li><a href="#">56 Share</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="like_comment">
                                        <ul>
                                            <li><a href="javascript:void(0);"><i class="fa fa-heart likePost" like="{{$post['id']}}" aria-hidden="true">&nbsp;Like</i></a>
                                            </li>
                                            <li><a href="javascript:void(0);"><i post_id="{{$post['id']}}" class="fa fa-comment" aria-hidden="true">&nbsp;Comment</i></a>
                                            <li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="post-react-main">
                                        <div class="commnet-box clearfix">
                                            <div class="commnet-box-profile-pic"> <img src="{{ $user['profile_image']}}" height="32" width="32" alt="profile-name" /> </div>
                                            <div class="commnet-box-commnt-write">
                                                <input type="text" class="user_comment" id="{{$post['id']}}" placeholder="Write a comment"/>
                                                <a href="javascript:void(0);" class="fa fa-smile-o cmnt-submit-btn"></a>
                                            </div>
                                            <div class="user-comments-box-toggle-{{$post['id']}} border-3-div" style="display:none;">
                                                @foreach($post['comments'] as $comment)
                                                    <div class="profile-n-post-info-name">
                                                        <h5><i class="fa fa-comment"></i>&nbsp; <a href="{{route('user_profile')}}" ><span class="p-p-name">{{$post['user']['first_name']}}&nbsp;{{$post['user']['last_name']}}</span> </a></h5>
                                                        <div class="profile-n-post-info-details">{{$comment['comment']}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endif
@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
<div id="page-content">
<div class="container">
<div class="panel">
    <div class="row">
        <div class="col-md-12">
            @if(session('error'))
              <div class="alert alert-danger">
                  {{ session('error') }}
              </div>
            @endif
              @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
        </div>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'post','route'=>['admin.updatepassword'])) !!}
          <div class="form-group clearfix col-sm-6">
              <div class="col-sm-12">
                  {!! Form::label(trans('admin.OLD_PASSWORD'),null, array('class' => 'required_label')) !!}
               {!! Form::password('old_password',['class'=>'form-control','placeholder'=>trans('admin.OLD_PASSWORD')]) !!}
                <div class="error">{{ $errors->first('old_password') }}</div> 
              </div>
          </div>
          <div class="form-group clearfix col-sm-6">
              <div class="col-sm-12">
                {!! Form::label(trans('admin.NEW_PASSWORD'),null,array('class' => 'required_label')) !!}
               {!! Form::password('new_password',['class'=>'form-control','placeholder'=>trans('admin.NEW_PASSWORD')]) !!}
                <div class="error">{{ $errors->first('new_password') }}</div>
              </div>
          </div>
          <div class="form-group clearfix col-sm-6">
              <div class="col-sm-12">
                  {!! Form::label(trans('admin.CONFIRM_PASSWORD') ,null,array('class' => 'required_label')) !!}
           {!! Form::password('confirm_password',['class'=>'form-control','placeholder'=>trans('admin.CONFIRM_PASSWORD')]) !!}
           <div class="error">{{ $errors->first('confirm_password') }}</div> 
              </div>
              
          </div>
          
          <div class="row">
              <div class="col-md-12 form-group form-actions">
                  <div class="col-md-8 col-md-offset-5">
          {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
          {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                  </div>
              </div>
          </div>
          
      {!! Form::close() !!}
    </div>
</div>
</div>
</div>
</div>

@endsection
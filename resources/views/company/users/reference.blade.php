@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">{{$user->first_name}} {{$user->last_name}} References - {{$user->role->name}} </h3>
                    <div class="example-box-wrapper">
                        
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">Name</th>
                                    <th width="20%">Email</th>
                                    <th width="20%">Phone</th>
                                    <th width="20%">Invited as</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!$users->isEmpty())
                                @php $i=1; @endphp
                                @foreach ($users as $user)
@php
$roles = array('1'=>'Admin', '2'=>'Company', '3'=>'Sales Agent', '4'=>'Buyer');
@endphp
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                                    <td>{{ $user->email}}</td>
                                    <td>{{ $user->phone}}</td>
                                    <td>{{$roles[$user->role_id]}}</td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">
                                        <div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>    
                        </table>
                        {!! $users->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
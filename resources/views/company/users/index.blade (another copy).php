@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">

        <div class="container">

            {!! Html::script(asset('public/admin/assets/widgets/datatable/datatable.js')) !!}
            {!! Html::script(asset('public/admin/assets/widgets/datatable/datatable-bootstrap.js')) !!}
            {!! Html::script(asset('public/admin/assets/widgets/datatable/datatable-responsive.js')) !!}

            <script type="text/javascript">

                $(document).ready(function () {
                    $('#datatable-responsive').DataTable({
                        responsive: true
                    });
                });
                $(document).ready(function () {
                    $('.dataTables_filter input').attr("placeholder", "Search...");
                });
            </script>
            
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">
                        User List
                    </h3>
                    <div class="example-box-wrapper">
                    <div class="row">    
                        <div class="size-md col-user-right">
                            {!! Form::open(['id'=>'search_form','route'=> 'admin.'.$controller.'.index']) !!}                         
                            <table class="search_table" align="right">
                              <tbody>
                                <tr>
                                  <td>{!! Form::label('Status',null,['class'=>'control-label no-padding-right']) !!}  &nbsp;&nbsp;</td>
                                  <td><?php 
                                    
                                     $status_list = array('' => 'All') + Config::get('global.status_list'); ?>
                                        {!! Form::select('status', $status_list, $form_data['status'], ['class' => 'form-control','id'=>'status']) !!}
                                  </td>
                                  <td style="padding-left: 5px !important;">{!! Form::label('Role ',null,['class'=>'control-label no-padding-right ']) !!}  &nbsp;&nbsp;</td>
                                  <td> {!! Form::select('role_id', array('' => 'All') + $allroles, $form_data['role_id'], ['class' => 'form-control','id'=>'roles']) !!}</td>
                                  
                                </tr>
                              </tbody>
                            </table>
                        {!! Form::close() !!}
                        </div>

                        <div class="size-md col-user-left">
                            {!!  Html::decode(Html::link(route('admin.users.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
                        </div>
                    </div>    
						<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">Name</th>
                                    <th width="12%">Email</th>
                                    <th width="15%">Role</th>
                                    <th width="15%">Refer By</th>
                                    <th width="25%">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if(!empty($users))
                                @foreach ($users as $key => $user)
                                <?php 
                                    $sr_no = $key + 1; 
                                    
                                    $role = '';
                                    if(isset($user->role)){
                                        $role = $user->role->name;
                                    }

                                    $referby = '';
                                    if(isset($user->refer)){
                                        $referby = $user->refer->first_name;
                                    }
                                ?>
                                <tr>
                                    <td>{{ $sr_no }}</td>
                                    <td>{{ ucfirst($user->first_name.' '.$user->last_name) }}</td>
                                    <td>{{ ucfirst($user->email) }}</td>
                                    <td>{{$role}}</td>
                                    <td>{{$referby}}</td>
                                    <td align="center">
                                        
                                    @if($user->name != 'Admin')
                                        
                                        @if($user->status == 1 )
                                        
                                        {!!  Html::decode(Html::link(route('admin.users.status_change',['id' => $user->id,'status'=>$user->status]),"<i class='glyphicon glyphicon-ok'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        
                                        @else
                                        
                                        {!!  Html::decode(Html::link(route('admin.users.status_change',['id' => $user->id,'status'=>$user->status]),"<i class='glyphicon glyphicon-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        
                                        @endif
                                        
                                        {!!  Html::decode(Html::link(route('admin.users.edit', $user->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                        {!!  Html::decode(Html::link(route('admin.users.destroy',['id' => $user->id]),"<i class='glyphicon glyphicon-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>'Delete', "data-alert"=>'Delete User'])) !!}

                                        {!!  Html::decode(Html::link(route('admin.dashboard',['r_id' => $user->role_id,'id'=>$user->id]),"<i class='glyphicon glyphicon-eye-open'></i>",['class'=>'btn btn-success','data-toggle'=>'tooltip','title'=>'View'])) !!}
                                        @endif
                                    
                                    </td>
                                    @endforeach
                                    @else
                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .example-box-wrapper .col-user-right{
        float: right;
        margin-right: 10px;
    }

    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }
</style>

<script type="text/javascript">
    $('#roles').on('change',function(){
        $('#search_form').submit();
    });

    $('#status').on('change',function(){
        $('#search_form').submit();
    });
</script>>
@endsection

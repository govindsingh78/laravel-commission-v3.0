@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">

        <div id="page-title">
		    <h2>Company Dashboard</h2>
		</div>

<div class="row">
    <div class="example-box-wrapper">
        <ul class="list-group row">
        	<li class="col-md-2">
                <a href="#" class="list-group-item active">
                    <i class="glyph-icon font-red icon-bullhorn"></i>
                    View Profile
                    <i class="glyph-icon font-green icon-chevron-right"></i>
                </a>
            </li>
            <li class="col-md-2">
                <a href="{{route('admin.user.history', $user->id)}}" class="list-group-item">
                    <i class="glyph-icon icon-clock-o"></i>
                    Last Records 
                    <i class="glyph-icon icon-chevron-right"></i>
                </a>
            </li>
            <li class="col-md-2">
                <a href="{{route('admin.user.reference', $user->id)}}" class="list-group-item">
                    <i class="glyph-icon font-blue-alt icon-user-md"></i>
                    Referral User
                    <i class="glyph-icon icon-chevron-right"></i>
                </a>
            </li>
            <li class="col-md-2">
                <a href="#" class="list-group-item">
                    <i class="glyph-icon font-primary icon-bar-chart"></i>
                    Statistics
                    <i class="glyph-icon icon-chevron-right"></i>
                </a>
            </li>
            <li class="col-md-2">
                <a href="#" class="list-group-item">
                    <i class="glyph-icon font-blue-alt icon-briefcase"></i>
                    Assets
                    <i class="glyph-icon icon-chevron-right"></i>
                </a>
            </li>
            <li class="col-md-2">
                <a href="#" class="list-group-item">
                    <i class="glyph-icon font-blue-alt icon-mobile"></i>
                    My Network
                    <i class="glyph-icon icon-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="tile-box tile-box-alt bg-blue-alt">
            <div class="tile-header">
                Total commission
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-dollar"></i>
                <div class="tile-content">
                    <span>$</span>
                    378
                </div>
            </div>
            <a href="#" class="tile-footer tooltip-button" data-placement="bottom" title="This is a link example!">
                view details
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tile-box tile-box-alt bg-green">
            <div class="tile-header">
                Commission earned 
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-dollar"></i>
                <div class="tile-content">
                    <span>$</span>
                    378
                </div>
            </div>
            <a href="#" class="tile-footer tooltip-button" data-placement="bottom" title="This is a link example!">
                view details
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tile-box tile-box-alt bg-primary">
            <div class="tile-header">
                Earned not collected
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-dollar"></i>
                <div class="tile-content">
                    <span>$</span>
                    378
                </div>
            </div>
            <a href="#" class="tile-footer tooltip-button" data-placement="bottom" title="This is a link example!">
                view details
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tile-box tile-box-alt bg-red">
            <div class="tile-header">
                Referral earned
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-dollar"></i>
                <div class="tile-content">
                    <span>$</span>
@php
    $referal_amt = BasicFunction::user_parent_earned($user->id);
    echo $referal_amt;
@endphp
                </div>
            </div>
            <a href="#" class="tile-footer tooltip-button" data-placement="bottom" title="This is a link example!">
                view details
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
</div>
@php $view_status = 'Inactive'; @endphp
@if($user->status == 1 )
    @php $view_status = 'Active'; @endphp
@endif;

<div class="row" style="padding-bottom: 2%;"></div>
<div class="row">
	<div class="row mailbox-wrapper">
		<div class="col-md-4">
			<div class="panel-layout">
		    	<div class="panel-box">
    				<div class="panel-content image-box">
		            	<div class="ribbon">
		                	<div class="bg-primary">{{$view_status}}</div>
		                </div>
		                <div class="image-content font-white">

		                    <div class="meta-box meta-box-bottom">
		                        <img src="{{WEBSITE_PUBLIC_URL}}/admin/assets/image-resources/default-user.png" alt="" class="meta-image img-bordered img-circle" width="60% !important">
		                        <h3 class="meta-heading">{{ucwords($user->first_name)}} {{ucwords($user->last_name)}}</h3>
		                        <h4 class="meta-subheading">Company</h4>
		                        <h4 class="bs-label label-blue-alt">Premium Plus</h4>
		                    </div>

		                </div>
		                <img src="{{WEBSITE_PUBLIC_URL}}/admin/assets/image-resources/blurred-bg/blurred-bg-13.jpg" alt="">

		            </div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="content-box">
                <h3 class="content-box-header bg-blue">
                    Profile Overview
                </h3>
                <div class="content-box-wrapper">
                    <table class="table">
                    	<tbody>
                    		<tr>
                    			<td style="color: black;">
                    				<b>First Name:</b>
                    			</td>	
                    			<td>
                    				{{ucwords($user->first_name)}}
                    			</td>
                    			<td style="color: black;">
                    				<b>Last Name:</b>
                    			</td>	
                    			<td>
                    				{{ucwords($user->last_name)}}
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black;">
                    				<b>Email:</b>
                    			</td>	
                    			<td>
                    				{{$user->email}}
                    			</td>
                    			<td style="color: black;">
                    				<b>Phone No:</b>
                    			</td>	
                    			<td>
                    				{{$user->phone}}
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black;">
                    				<b>Address:</b>
                    			</td>	
                    			<td colspan="3">
                    				{{$user->address}}
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black;">
                    				<b>City:</b>
                    			</td>	
                    			<td>
                    				Jaipur
                    			</td>
                    			<td style="color: black;">
                    				<b>State:</b>
                    			</td>	
                    			<td>
                    				Rajasthan
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black;">
                    				<b>User Type:</b>
                    			</td>	
                    			<td>
                    				Company
                    			</td>
                    			<td style="color: black;">
                    				<b>Plan Name:</b>
                    			</td>	
                    			<td>
                    				Premium Plus
                    			</td>
                    		</tr>
                    		
                    	</tbody>
                    </table>
                </div>
            </div>
		</div>
		<div class="col-md-12">
			<div class="content-box">
                <h3 class="content-box-header bg-blue">
                    About Your Company
                </h3>
                <div class="content-box-wrapper">
                    <table class="table">
                    	<tbody>
                    		<tr>
                    			<td style="color: black; width: 20% !important;">
                    				<b>Company Name:</b>
                    			</td>	
                    			<td>
                    				{{ucwords($user->first_name)}} {{ucwords($user->last_name)}}
                    			</td>
                    			<td style="color: black;">
                    				<b>Company Size:</b>
                    			</td>	
                    			<td>
                    				20
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black; width: 20% !important;">
                    				<b>Website URL:</b>
                    			</td>	
                    			<td>
                    				http://www.xyz.com
                    			</td>
                    			<td style="color: black;">
                    				<b>Skype:</b>
                    			</td>	
                    			<td>
                    				xyz2015
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black; width: 20% !important;">
                    				<b>Industry:</b>
                    			</td>	
                    			<td colspan="3">
                    				<span class="bs-label label-blue-alt">Accounting</span>&nbsp;<span class="bs-label label-blue-alt">Banking</span>&nbsp;<span class="bs-label label-blue-alt">Capital Markets</span>
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black; width: 20% !important;">
                    				<b>Target industries:</b>
                    			</td>	
                    			<td colspan="3">
                    				<span class="bs-label label-blue-alt">Banking</span>&nbsp;<span class="bs-label label-blue-alt">Capital Markets</span>
                    			</td>
                    		</tr>
                    		<tr>
                    			<td style="color: black; width: 20% !important;">
                    				<b>About The Company:</b>
                    			</td>	
                    			<td colspan="3">
                    				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    			</td>
                    		</tr>
                    	</tbody>
                    </table>
                </div>
            </div>
		</div>
		<div class="col-md-12">
			<div class="content-box">
                <h3 class="content-box-header bg-blue">
                    Product & Services
                </h3>
                <div class="example-box-wrapper">
					<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
						<thead>
							<tr>
							    <th>Sr No.</th>
							    <th>Industry Name</th>
							    <th>Product Name</th>
							    <th>Price</th>
							    <th>Description</th>
							    <th>Status</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
							    <th>Sr No.</th>
							    <th>Industry Name</th>
							    <th>Product Name</th>
							    <th>Price</th>
							    <th>Description</th>
							    <th>Status</th>
							</tr>
						</tfoot>
						<tbody>
							<tr>
							    <td>1</td>
							    <td>Banking</td>
							    <td>System Architect</td>
							    <td>2000</td>
							    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry....</td>
							    <td>Active</td>
							</tr>
							<tr>
							    <td>2</td>
							    <td>Accounting</td>
							    <td>Accountant</td>
							    <td>6000</td>
							    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry...</td>
							    <td>Active</td>
							</tr>
							<tr>
							    <td>3</td>
							    <td>Capital Markets</td>
							    <td>Broker</td>
							    <td>3000</td>
							    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry....</td>
							    <td>Active</td>
							</tr>
						</tbody>
					</table>
				</div>
            </div>
        </div>
	</div>
	
	
</div>


































        </div>
    </div>
</div>
@endsection

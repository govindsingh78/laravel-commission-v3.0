@extends('layouts.app')

@section('content')
    @foreach($posts as $post)
    <div class="col-md-4">    
        <div class="list-group">
            <div class="list-group-item" style="overflow: hidden;">
                <h4 class="list-group-item-heading">{{$post->title}}</h4>
                <p class="list-group-item-text">{{ $post->body }}</p>
                <p class="list-group-item-text"> Created at : {{$post->created_at}} </p>
                <p class="list-group-item-text"> Update On : {{$post->created_at}} </p>
                <p class="list-group-item-text"> <a href="/blog/{{$post->id}}" class="btn btn-primary pull-right">Read More..</a>
            </p>
            
        </div>
           
        </div>
    </div>
    @endforeach

    <div class="col-md-12">{{$posts->links()}}</div>
@endsection
@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <div class="panel">
                <div class="panel-body">
                {!! Form::open(['route'=> 'industry.store', 'id'=>'form_inds']) !!} 
                        <div class="form-group clearfix col-sm-6">
                            {!! Form::label('Name ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Name']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('name') }}</div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('Status',null,['class'=>'required_label']) !!}
                            <div class="col-sm-12">
                                <?php $status_list = Config::get('global.status_list'); ?>
                                {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                            </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('status') }}</div>
                        </div><!-- /.form-group -->

                        <div class="form-actions col-sm-12 center">
                            <div class="col-sm-12">
                            {{Form::button('<i class="fa fa-undo"></i> Reset', array('id' => 'btn_reset', 'type' => 'reset', 'class'=> 'btn btn-default btn-sm'))}}
                            {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(e){
    $('#btn_reset').click(function(){
        $('#form_inds')[0].reset();
    });            
});
</script>

@endsection
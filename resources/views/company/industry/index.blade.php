@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">Industry List</h3>
                    <div class="example-box-wrapper">
                        <div class="row"> 

                            <div class="size-md col-user-left">
                                {!!  Html::decode(Html::link(route('industry.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
                            </div>
                       
                            <div class="size-md col-user-right">
                            {!! Form::open(['method'=>'get','route'=>['industry.index']]) !!}                         
                                <table class="search_table">
                                  <tbody>
                                    <tr>
                                      <td>{!! Form::text('search', '', ['class'=>'form-control']) !!}</td>
                                      <td><button class="btn btn-primary btn-search" type="submit">Search</button></td>
                                    </tr>
                                  </tbody>
                                </table>
                                {!! Form::close() !!}
                            </div>
                        </div>    
                        
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">@sortablelink('name', 'Name')</th>
                                    <th width="15%">Updated At</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!$industries->isEmpty())
                                @php $i=1; @endphp
                                @foreach ($industries as $industry)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{ ucfirst($industry->name)}}</td>
                                    <td>
                                    {{date('m/d/Y', strtotime($industry->updated_at))}}
                                    </td>
                                    <td>
                @if($industry->status == 1 )
                    {!!  Html::decode(Html::link(route('admin.industry.status_change',['id' => $industry->id,'status'=>$industry->status]),"<i class='glyphicon glyphicon-ok'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                    @else
                    {!!  Html::decode(Html::link(route('admin.industry.status_change',['id' => $industry->id,'status'=>$industry->status]),"<i class='glyphicon glyphicon-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                @endif
                    {!!  Html::decode(Html::link(route('industry.edit', $industry->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                    {!!  Html::decode(Html::link(route('industry.destroy',['id' => $industry->id]),"<i class='glyphicon glyphicon-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>'Delete', "data-alert"=>'Delete Role'])) !!}
                                    </td> 
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>    
                        </table>
                        {!! $industries->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .example-box-wrapper .col-user-right{
        float: right;
        margin-right: 10px;
    }

    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }
    .btn-search{
        margin-bottom: 2px;
        margin-left: 4px;
    }
</style>
@endsection
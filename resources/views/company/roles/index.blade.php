@extends('layouts.admin')

@section('content') 
 <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

<!-- Data tables -->

<!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/datatable/datatable.css">-->
{!! Html::script(asset('public/admin/assets/widgets/datatable/datatable.js')) !!}
{!! Html::script(asset('public/admin/assets/widgets/datatable/datatable-bootstrap.js')) !!}
{!! Html::script(asset('public/admin/assets/widgets/datatable/datatable-responsive.js')) !!}

<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Search...");
    });

</script>

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    Role List
</h3>

<div class="example-box-wrapper">
<div class="size-md">
    {!!  Html::decode(Html::link(route('admin.roles.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
</div></br>
<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
<thead>
<tr>
    <th>Name</th>
    <th>Created At</th>
    <th>Updated At</th>
    <th>Action</th>
</tr>
</thead>

<tfoot>
<tr>
    <th>Name</th>
    <th>Created At</th>
    <th>Updated At</th>
    <th>Action</th>
</tr>
</tfoot>

<tbody>
  @if(!empty($roles))
        @foreach ($roles as $role)
        <tr>
            <td>{{ ucfirst($role->name) }}</td>
            <td>{{ date_val($role->created_at,DATE_FORMATE ) }}</td>
            <td>{{ date_val($role->updated_at,DATE_FORMATE ) }}</td>
            <td align="center">
            @if($role->name != 'Admin')
                @if($role->status == 1 )
                    {!!  Html::decode(Html::link(route('admin.roles.status_change',['id' => $role->id,'status'=>$role->status]),"<i class='glyphicon glyphicon-ok'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                    @else
                    {!!  Html::decode(Html::link(route('admin.roles.status_change',['id' => $role->id,'status'=>$role->status]),"<i class='glyphicon glyphicon-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                @endif
                {!!  Html::decode(Html::link(route('admin.roles.edit', $role->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                {!!  Html::decode(Html::link(route('admin.roles.destroy',['id' => $role->id]),"<i class='glyphicon glyphicon-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>'Delete', "data-alert"=>'Delete Role'])) !!}

            @endif

            </td>
            @endforeach
        @else
    <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>
    @endif

</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection

Dear {{$option->first_name}},
<br>
Your account has been created successfully. 
<br>
Please verify your account by putting the correct OTP.
<br>
Your OTP is : {{$option->otp}}
<br>
<strong>Regards</strong>
<br>
</p>
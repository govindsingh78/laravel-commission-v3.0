<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    @include('includes.front.head')
    <body>
        @include('includes.front.header')
        @include('includes.front.signon')
        @yield('content')
        @include('includes.front.footer')
    </body>
</html>
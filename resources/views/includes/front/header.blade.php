<?php
    $settings = \App\Helpers\BasicFunction::getSettings();
?>

<header>
    <div class="header_part">
        <div class="header_top">
            <div class="container">
                @if(!empty($settings->mobile))
                    <div class="col-md-6 col-sm-6">
                        <div class="contxt">
                            <img src="{{ URL::asset('public/img/call-icn.png') }}" alt="Contact Number">
                            {{$settings->mobile}}
                        </div>
                    </div>
                @endif
                
                @if(!empty($settings->email))
                    <div class="col-md-6 col-sm-6">
                        <div class="emiltxt">
                            <img src="{{ URL::asset('public/img/mail-icn.png') }}" alt="Contact Number">
                            {{$settings->email}}
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="header_down">
            <div class="container">
                <div class="k-new-navbar " data-behavior="LoggedInNavbar ToggleClassWhenScrolling " data-login-details="#" data-scroll-class="k-new-navbar--scrolled" data-scroll-class-behavior="add" data-scroll-offset="5">
                    <div class="k-new-navbar-left">
                        <a class="k-new-navbar-logo" href="#">
                            @if(!empty($settings->site_logo))
                                <img src="{{asset(SITE_IMAGES_URL . $settings->site_logo)}}" alt="logo">
                            @else
                                <img src="{{ URL::asset('public/img/logo.jpg') }}" alt="logo">
                            @endif
                        </a>
                    </div>
                    
                    <div class="k-new-navbar-right">
                        <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-login" data-logged-in="false" href="#">Login</a>
                        </div>
                        <div class="k-new-navbar-item-container k-new-navbar-item-container--hidden">
                            <a class="k-new-navbar-item-login" data-logged-in="true" href="#">login</a>
                        </div>
                        <div class="k-new-navbar-item-container">
                            <a class="k-new-navbar-item-store" href="#">Sign Up</a>
                        </div>
                        <div class="k-new-navbar-item-container k-new-navbar-item-container--last">
                            <a class="k-new-navbar-item-menu" data-behavior="ExpandNewNavbar" href="#">Menu</a>
                            <div class="k-new-navbar-item-stack" data-behavior="ExpandNewNavbar">
                                <div class="k-new-navbar-item-stack-top"></div>
                                <div class="k-new-navbar-item-stack-middle"></div>
                                <div class="k-new-navbar-item-stack-bottom"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="k-new-navbar-side-bar">
                        <a class="k-new-navbar-side-bar-item" href="#">How It Works</a>
                        <a class="k-new-navbar-side-bar-item" href="#">Coverage</a>
                        <a class="k-new-navbar-side-bar-item" href="#">Blog</a>
                        <a class="k-new-navbar-side-bar-item" href="#">Get Started</a>
                        <a class="k-new-navbar-side-bar-item" href="#">About Us</a>
                        <a class="k-new-navbar-side-bar-item" href="#">Jobs</a>
                        
                        <div class="k-new-navbar-item-container--hidden">
                            <h3 class="k-header--xx-small k-spacing-top--x-large k-spacing-bottom--base k-color--blue">My Account</h3>
                            <a class="#" data-logged-in="true" href="#">Sign Out</a>
                        </div>
                        
                        <h3 class="k-header--xx-small k-spacing-top--x-large k-spacing-bottom--base k-color--blue">Get Support</h3>
                        <a class="k-new-navbar-side-bar-item" href="#">Help Center</a>
                        <a class="k-new-navbar-side-bar-item" href="#">Contact Us</a>
                        <a class="k-new-navbar-side-bar-item" href="#">Return</a>
                        <a class="k-new-navbar-side-bar-item k-new-navbar-side-bar-item-legal" href="#">Privacy</a>
                        <a class="k-new-navbar-side-bar-item k-new-navbar-side-bar-item-legal" href="#">Terms</a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</header>
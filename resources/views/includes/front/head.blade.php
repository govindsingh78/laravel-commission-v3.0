<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Selecoms</title>
        
        <link href="{{ URL::asset('public/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/style.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/custom.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/side-navbar.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/bootstrap-select.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/font-awesome.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/jquery-ui.min.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/normalize.min.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/owl.carousel.min.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/jquery-ui.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/normalize.css')}}" rel="stylesheet" />
        <link href="{{ URL::asset('public/css/jquery.magicsearch.css')}}" rel="stylesheet" />
        
        <script src="{{ URL::asset('public/js/side-navbar.js')}}"></script>
        <script src="{{ URL::asset('public/js/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('public/js/popper.js')}}"></script>
        <script src="{{ URL::asset('public/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('public/js/bootstrap-select.js')}}"></script>
        <script src="{{ URL::asset('public/js/jquery-ui.min.js')}}"></script>
        <script src="{{ URL::asset('public/js/jquery-ui.tabs.neighbors.js')}}"></script>
        <script src="{{ URL::asset('public/js/owl.carousel.min.js')}}"></script>
        <script src="{{ URL::asset('public/js/input-bx.js')}}"></script>
        <script src="{{ URL::asset('public/js/jquery.magicsearch.js')}}"></script>
        <script src="{{ URL::asset('public/js/custom.js')}}"></script>
</head>


<?php
    $selecom_links = \App\Helpers\BasicFunction::getFooterLinks('selecoms');
    $userful_links = \App\Helpers\BasicFunction::getFooterLinks('useful-links');
    $who_we_are = \App\Helpers\BasicFunction::getFooterLinks('who-we-are');
    $support = \App\Helpers\BasicFunction::getFooterLinks('support');
    $settings = \App\Helpers\BasicFunction::getSettings();
?>

<footer>
    <div class="salecoms-footer">
        <div class="container">
            <row>
                <div class="ft-sec">
                    <div class="col-md-3">
                        <div class="cmpy-links">
                            <h4> <img src="{{ URL::asset('public/img/logo-black.png') }}" alt="{{$selecom_links->title}}"> </h4>
                            <ul>
                                @if(!empty($selecom_links->children))
                                    @foreach ($selecom_links->children as $key => $children)
                                        @if($children->status === 1)
                                            <li><a href="{{$children->slug}}"> {{$children->title}}  </a></li>
<!--                                            <li><a href="#"> Opportunities for sales Agent </a></li>
                                            <li><a href="#"> How we help compsnies </a></li>
                                            <li><a href="#"> How we help Buyers </a></li>
                                            <li><a href="#"> Plans / Pricing </a></li>-->
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="cmpy-links">
                            <h4> {{$userful_links->title}} </h4>
                            <ul>
                                @if(!empty($userful_links->children))
                                    @foreach ($userful_links->children as $key => $children)
                                        @if($children->status === 1)
                                            <li><a href="{{$children->slug}}"> {{$children->title}} </a></li>
<!--                                            <li><a href="#"> Loyalty Program </a></li>
                                            <li><a href="#"> Sales Agents Training </a></li>
                                            <li><a href="#"> Ebooks </a></li>-->
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="cmpy-links">
                            <h4> {{$who_we_are->title}} </h4>
                            <ul>
                                @if(!empty($who_we_are->children))
                                    @foreach ($who_we_are->children as $key => $children)
                                        @if($children->status === 1)
                                            <li><a href="{{$children->slug}}"> {{$children->title}} </a></li>
<!--                                            <li><a href="#"> Our Team </a></li>
                                            <li><a href="#"> Careers </a></li>
                                            <li><a href="#"> Press </a></li>
                                            <li><a href="#"> Blog </a></li> -->
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="cmpy-links">
                            <h4> {{$support->title}} </h4>
                            <ul>
                                @if(!empty($support->children))
                                    @foreach ($support->children as $key => $children)
                                        @if($children->status === 1)
                                            <li><a href="{{$children->slug}}"> {{$children->title}} </a></li>
<!--                                            <li><a href="#"> Contact us </a></li>
                                            <li><a href="#"> FAQ's </a></li>
                                            <li><a href="#"> Privacy & Terms </a></li>-->
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </row>
        </div>
    </div>

    <div class="ft-subline">
        <div class="container">
            <row>
                <div class="md-12">
                    <div class="ft-disclaimer">
                        <i class="fa fa-copyright"></i>{{$settings->copyright}}
                    </div>
                </div>
            </row>
        </div>
    </div>
</footer>

<section class="earn-point">
    <a href="#">
        @if(!empty($settings->footer_advt_img))
            <img src="{{asset(SITE_IMAGES_URL . $settings->footer_advt_img)}}" alt="point-img">
        @else
            <img src="{{ URL::asset('public/img/earn-point-img.png') }}" alt="point-img">
        @endif
    </a>
</section>

 

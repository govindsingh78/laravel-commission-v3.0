<div class="loginmodel">
                <div class="modal fade" id="loginmodel" role="dialog">
                    <div class="modal-dialog modal-sm">
                    
                      <!-- Modal content-->
                          <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <div class="login_signup">
                                        <a class="signmo" href="javascript:void(0)" onClick="loginmodel(event, 'login_body')" id="defaultOpen1">Login </a>
                                        <a class="signmo" href="javascript:void(0)" onClick="loginmodel(event, 'signup_body')"> Signup</a>
                                      </div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="loginmo" id="login_body" >
                                            <form class="login-popup">
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input" type="text" required>
                                                    <span class="login-field__text">Email Address or Mobile Number</span>
                                                  </label>
                                                 
                                                </div>

                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input type="password" required>
                                                    <span class="login-field__text">Password</span>
                                                  </label>
                                                </div>
                                               
                                                  <div class="margin-top-s">
                                                    <div class="secondary-links flt-right">
                                                      
                                                      <a class="txt-brand-tertiary" href="javascript:void(0)" onClick="loginmodel(event, 'forgot')">Forgot password?
                                                      </a>
                                                    </div>
                                                  </div>
                                                
                                               
                                                <div class="field-group form-group login_btn">
                                                  <button class="btn btn--primary btn-block">Login</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="loginmo forgot" id="forgot"  style="display:none;">
                                           <form class="login-popup">
                                                <div class="field-group">
                                                 <label class="full-width login-field para_forgot">
                                                    <div class="login-field__text">A One-Time Password (OTP) that was sent to your registered Email ID / Mobile Number</div>
                                                 </label>
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input" type="text" required>
                                                    <span class="login-field__text">email address or mobile number</span>
                                                    <!-- <a href="" class="resend_code">Resend OTP ?</a> --> 
                                                  </label>
                                                </div>
                                                <div class="field-group form-group member_pricing">
                                                  <a href=""> < Back</a>
                                                  <button class="btn btn--primary btn-cont">SET PASSWORD</button>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="loginmo" id="signup_body"  style="display:none;">
                                            <form class="login-popup">
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input" type="text" required>
                                                    <span class="login-field__text">Name</span>
                                                  </label>
                                                 
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input" type="email" required>
                                                    <span class="login-field__text">Email</span>
                                                  </label>
                                                 
                                                </div>
                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input" type="text" required>
                                                    <span class="login-field__text">Mobile Number</span>
                                                  </label>
                                                </div>

                                                <div class="field-group">
                                                  <label class="full-width login-field">
                                                    <input class="form-control login-field__input type="password" required>
                                                    <span class="login-field__text">Password</span>
                                                  </label>
                                                </div>
                                                <div class="field-group">
                                                       <div class="dropdown selct_box">
                                                          <select>
                                                              <option>SELECT CATEGORY</option>
                                                              <option>Company: Looking For Commission-only Sales Agents</option>
                                                              <option>Sales Agent: Looking For Commission-based Opportunities.</option>  
                                                          </select>
                                                      </div> 
                                                </div>
                                                
                                                
                                                  
                                                  <div class="field-group">
                                                  <label class="nb-checkbox margin-top-m sign_noti">
                                                        <input class="ng-valid ng-touched ng-dirty" type="checkbox">
                                                        <div class="nb-checkbox__bg">
                                                          <div class="nb-checkbox__icon"></div>
                                                        </div>
                                                        <span class="sign_span">I accept the terms of services.</span>
                                                  </label>
                                              </div>
                                                
                                                
                                                 <div class="field-group form-group member_login">
                                                  <!-- <a href="pricing3.html" class="btn btn--primary btn-cont">Continue</a> -->
                                                  <button href="" class="btn btn--primary btn-cont">Continue</button>
                                                  <a href="" class="" href="javascript:void(0)" onClick="loginmodel(event, 'login_body')" >Already a member? 
                                                 <span class="font-size-xs txt-brand-primary">Login</span></a>
                                                </div> 
                                            </form>
                                        </div>
                                    </div>

                                   <!--  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                    <script>
                                            function loginmodel(log, modelNum) {
                                                var i, loginmo, signmo,forgot;
                                                loginmo = document.getElementsByClassName("loginmo");
                                                for (i = 0; i < loginmo.length; i++) {
                                                    loginmo[i].style.display = "none";
                                                }
                                                signmo = document.getElementsByClassName("signmo");
                                                for (i = 0; i < signmo.length; i++) {
                                                    signmo[i].className = signmo[i].className.replace(" active", "");
                                                }
                                                forgot = document.getElementsByClassName("forgot");
                                                for (i = 0; i < forgot.length; i++) {
                                                    forgot[i].className = forgot[i].className.replace(" active", "");
                                                }
                                                document.getElementById(modelNum).style.display = "block";
                                                log.currentTarget.className += " active";
                                            }
                                            // Get the element with id="defaultOpen" and click on it
                                            document.getElementById("defaultOpen1").click();
                                    </script> 
                          </div>
                      
                    </div>
                </div>
        </div>
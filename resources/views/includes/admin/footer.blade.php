</div>


    <!-- WIDGETS -->

{!! Html::script(asset('/admin/assets/bootstrap/js/bootstrap.js')) !!}

<!-- Bootstrap Dropdown -->

<!-- {!! Html::script(asset('public/admin/assets/widgets/dropdown/dropdown.js')) !!} -->

<!-- Bootstrap Tooltip -->

<!-- {!! Html::script(asset('public/admin/assets/widgets/tooltip/tooltip.js')) !!} -->

<!-- Bootstrap Popover -->

<!-- {!! Html::script(asset('public/admin/assets/widgets/popover/popover.js')) !!} -->

<!-- Bootstrap Progress Bar -->

{!! Html::script(asset('/admin/assets/widgets/progressbar/progressbar.js')) !!}

<!-- Bootstrap Buttons -->

<!-- {!! Html::script(asset('public/admin/assets/widgets/button/button.js')) !!} -->

<!-- Bootstrap Collapse -->

<!-- {!! Html::script(asset('public/admin/assets/widgets/collapse/collapse.js')) !!} -->

<!-- Superclick -->

{!! Html::script(asset('/admin/assets/widgets/superclick/superclick.js')) !!}

<!-- Input switch alternate -->

{!! Html::script(asset('/admin/assets/widgets/input-switch/inputswitch-alt.js')) !!}

<!-- Slim scroll -->

{!! Html::script(asset('/admin/assets/widgets/slimscroll/slimscroll.js')) !!}

<!-- Slidebars -->

{!! Html::script(asset('/admin/assets/widgets/slidebars/slidebars.js')) !!}
{!! Html::script(asset('/admin/assets/widgets/slidebars/slidebars-demo.js')) !!}

<!-- PieGage -->

{!! Html::script(asset('/admin/assets/widgets/charts/piegage/piegage.js')) !!}
{!! Html::script(asset('/admin/assets/widgets/charts/piegage/piegage-demo.js')) !!}

<!-- Screenfull -->

{!! Html::script(asset('/admin/assets/widgets/screenfull/screenfull.js')) !!}

<!-- Content box -->

{!! Html::script(asset('/admin/assets/widgets/content-box/contentbox.js')) !!}

<!-- Overlay -->

{!! Html::script(asset('/admin/assets/widgets/overlay/overlay.js')) !!}

<!-- Widgets init for demo -->

{!! Html::script(asset('/admin/assets/js-init/widgets-init.js')) !!}

<!-- Theme layout -->

{!! Html::script(asset('/admin/assets/themes/admin/layout.js')) !!}

<!-- Theme switcher -->

{!! Html::script(asset('/admin/assets/widgets/theme-switcher/themeswitcher.js')) !!}

</div>

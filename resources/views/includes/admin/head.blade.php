<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>  Admin </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('/admin/assets/images/icons/apple-touch-icon-144-precomposed.png')}}">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('/admin/assets/images/icons/apple-touch-icon-114-precomposed.png')}}">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('/admin/assets/images/icons/apple-touch-icon-72-precomposed.png')}}">
<link rel="apple-touch-icon-precomposed" href="{{asset('/admin/assets/images/icons/apple-touch-icon-57-precomposed.png')}}">
<link rel="shortcut icon" href="{{asset('/admin/assets/images/icons/favicon.png')}}">


{!! Html::style( URL::asset('/admin/assets/bootstrap/css/bootstrap.css')) !!}


<!-- HELPERS -->

{!! Html::style( asset('/admin/assets/helpers/animate.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/backgrounds.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/boilerplate.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/border-radius.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/grid.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/page-transitions.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/spacing.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/typography.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/utils.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/colors.css')) !!}

<!-- ELEMENTS -->

{!! Html::style( asset('/admin/assets/elements/badges.css')) !!}
{!! Html::style( asset('/admin/assets/elements/buttons.css')) !!}
{!! Html::style( asset('/admin/assets/elements/content-box.css')) !!}
{!! Html::style( asset('/admin/assets/elements/dashboard-box.css')) !!}
{!! Html::style( asset('/admin/assets/elements/forms.css')) !!}
{!! Html::style( asset('/admin/assets/elements/images.css')) !!}
{!! Html::style( asset('/admin/assets/elements/info-box.css')) !!}
{!! Html::style( asset('/admin/assets/elements/invoice.css')) !!}
{!! Html::style( asset('/admin/assets/elements/loading-indicators.css')) !!}
{!! Html::style( asset('/admin/assets/elements/menus.css')) !!}
{!! Html::style( asset('/admin/assets/elements/panel-box.css')) !!}
{!! Html::style( asset('/admin/assets/elements/response-messages.css')) !!}
{!! Html::style( asset('/admin/assets/elements/responsive-tables.css')) !!}
{!! Html::style( asset('/admin/assets/elements/ribbon.css')) !!}
{!! Html::style( asset('/admin/assets/elements/social-box.css')) !!}
{!! Html::style( asset('/admin/assets/elements/tables.css')) !!}
{!! Html::style( asset('/admin/assets/elements/tile-box.css')) !!}
{!! Html::style( asset('/admin/assets/elements/timeline.css')) !!}



<!-- ICONS -->

{!! Html::style( asset('/admin/assets/icons/fontawesome/fontawesome.css')) !!}
{!! Html::style( asset('/admin/assets/icons/linecons/linecons.css')) !!}
{!! Html::style( asset('/admin/assets/icons/spinnericon/spinnericon.css')) !!}


<!-- WIDGETS -->

{!! Html::style( asset('/admin/assets/widgets/accordion-ui/accordion.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/calendar/calendar.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/carousel/carousel.css')) !!}

{!! Html::style( asset('/admin/assets/widgets/charts/justgage/justgage.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/charts/morris/morris.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/charts/piegage/piegage.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/charts/xcharts/xcharts.css')) !!}

{!! Html::style( asset('/admin/assets/widgets/chosen/chosen.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/colorpicker/colorpicker.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/datatable/datatable.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/datepicker/datepicker.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/datepicker-ui/datepicker.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/daterangepicker/daterangepicker.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/dialog/dialog.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/dropdown/dropdown.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/dropzone/dropzone.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/file-input/fileinput.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/input-switch/inputswitch.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/input-switch/inputswitch-alt.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/ionrangeslider/ionrangeslider.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/jcrop/jcrop.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/jgrowl-notifications/jgrowl.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/loading-bar/loadingbar.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/maps/vector-maps/vectormaps.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/markdown/markdown.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/modal/modal.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/multi-select/multiselect.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/multi-upload/fileupload.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/nestable/nestable.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/noty-notifications/noty.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/popover/popover.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/pretty-photo/prettyphoto.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/progressbar/progressbar.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/range-slider/rangeslider.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/slidebars/slidebars.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/slider-ui/slider.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/tabs-ui/tabs.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/theme-switcher/themeswitcher.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/timepicker/timepicker.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/tocify/tocify.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/tooltip/tooltip.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/touchspin/touchspin.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/uniform/uniform.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/wizard/wizard.css')) !!}
{!! Html::style( asset('/admin/assets/widgets/xeditable/xeditable.css')) !!}

<!-- SNIPPETS -->

{!! Html::style( asset('/admin/assets/snippets/chat.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/files-box.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/login-box.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/notification-box.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/progress-box.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/todo.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/user-profile.css')) !!}
{!! Html::style( asset('/admin/assets/snippets/mobile-navigation.css')) !!}

<!-- APPLICATIONS -->

{!! Html::style( asset('/admin/assets/applications/mailbox.css')) !!}

<!-- Admin theme -->

{!! Html::style( asset('/admin/assets/themes/admin/layout.css')) !!}
{!! Html::style( asset('/admin/assets/themes/admin/color-schemes/default.css')) !!}

<!-- Components theme -->

{!! Html::style( asset('/admin/assets/themes/components/default.css')) !!}
{!! Html::style( asset('/admin/assets/themes/components/border-radius.css')) !!}

<!-- Admin responsive -->

{!! Html::style( asset('/admin/assets/helpers/responsive-elements.css')) !!}
{!! Html::style( asset('/admin/assets/helpers/admin-responsive.css')) !!}



{!! Html::style( asset('/admin/plugins/datepicker/datepicker3.css')) !!}
  <!-- Daterange picker -->
{!! Html::style( asset('/admin/plugins/daterangepicker/daterangepicker-bs3.css')) !!}
{!! Html::style( asset('/admin/css/jquery.noty.css')) !!}
{!! Html::style( asset('/admin/css/noty_theme_default.css')) !!}


    <!-- JS Core -->
<script type="text/javascript">
    var date_format = "{{ DATE_FORMATE_JS }}";
</script>
    {!! Html::script(asset('/admin/assets/js-core/jquery-core.js')) !!}
    {!! Html::script(asset('/admin/assets/js-core/jquery-ui-core.js')) !!}
    {!! Html::script(asset('/admin/assets/js-core/jquery-ui-widget.js')) !!}
    {!! Html::script(asset('/admin/assets/js-core/jquery-ui-mouse.js')) !!}
    {!! Html::script(asset('/admin/assets/js-core/jquery-ui-position.js')) !!}
    <!--{!! Html::script(asset('/admin/assets/js-core/transition.js')) !!}-->
    {!! Html::script(asset('/admin/assets/js-core/modernizr.js')) !!}
    {!! Html::script(asset('/admin/assets/js-core/jquery-cookie.js')) !!}
    {!! Html::script( asset('/admin/plugins/daterangepicker/daterangepicker.js')) !!}
    <!-- datepicker -->
    {!! Html::script( asset('/admin/plugins/datepicker/bootstrap-datepicker.js')) !!}
    {!! Html::script(asset('/admin/js/global.js')) !!}
    {!! Html::script(asset('/admin/js/jquery.noty.js')) !!}
    {!! Html::script(asset('/admin/js/jquery.validate.min.js')) !!}

    <script type="text/javascript">

        var csrf_token          = "{{ csrf_token()}}";
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>





</head>
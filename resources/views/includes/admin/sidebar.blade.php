 <!--<div id="page-sidebar" class="bg-gradient-1 font-inverse">-->
 <div id="page-sidebar" class="">
    <div class="scroll-sidebar">
        <ul id="sidebar-menu">
            <li class="header"><span>Overview</span></li>
            <li>
                <a href="{{route('admin.dashboard')}}" title="Admin Dashboard">
                    <i class="glyphicon glyphicon-th-large"></i>
                    <span>Admin dashboard</span>
                </a>
            </li>
            <li class="divider"></li>
            
            <li class="no-menu">
                <a href="{{route('admin.settings')}}" title="Site Configuration">
                    <i class="glyphicon glyphicon-cog"></i>
                    <span>Site Configuration</span>
                </a>
            </li>
            <li class="divider"></li>

            <li class="no-menu">
                <a href="{{route('admin.users')}}" title="Users">
                    <i class="glyphicon glyphicon-user"></i>
                    <span>Users</span>
                </a>
            </li>
            <li class="divider"></li>
            
            <li class="no-menu">
                <a href="{{route('admin.sliders')}}" title="Slider Images">
                    <i class="glyphicon glyphicon-picture"></i>
                    <span>Slider Images</span>
                </a>
            </li>
            <li class="divider"></li>

            <!-- <li class="no-menu">
                <a href="{{route('admin.emailtemplates')}}" title="Email Templates">
                    <i class="glyph-icon glyphicon-cog"></i>
                    <span>Email Templates </span>
                </a>
            </li> 
            <li class="divider"></li>-->

            <li class="no-menu">
                <a href="{{route('admin.cms')}}" title="CMS">
                    <i class="glyphicon glyphicon-th-list"></i>
                    <span>Cms</span>
                </a>
            </li>
            <li class="divider"></li>

            <li class="no-menu">
                <a href="{{route('industry.index')}}" title="History">
                    <i class="glyphicon glyphicon-info-sign"></i>
                    <span>Industry</span>
                </a>
            </li>
            <li class="divider"></li>

            <li class="no-menu">
                <a href="{{route('product.index')}}" title="History">
                    <i class="glyphicon glyphicon-th-list"></i>
                    <span>Product</span>
                </a>
            </li>
            <li class="divider"></li>

            <li class="no-menu">
                <a href="{{route('admin.history')}}" title="History">
                    <i class="glyphicon glyphicon-header"></i>
                    <span>Login History</span>
                </a>
            </li>
            <li class="divider"></li>

            <li class="no-menu">
                <a href="{{route('admin.user.references')}}" title="Top References">
                    <i class="glyphicon glyphicon-exclamation-sign"></i>
                    <span>Top References</span>
                </a>
            </li>
            <li class="divider"></li>

             <li class="no-menu">
                <a href="{{route('admin.plann')}}" title="Featured Reference">
                    <i class="glyphicon glyphicon-list-alt"></i>
                    <span>Plann Feature</span>
                </a>
            </li>
            <li class="divider"></li>
            
<!--            <li>
                <a href="#" title="Elements">
                    <i class="glyph-icon icon-linecons-diamond"></i>
                    <span>Elements</span>
                </a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="buttons.html" title="Buttons"><span>Buttons</span></a></li>
                        <li><a href="labels-badges.html" title="Labels &amp; Badges"><span>Labels &amp; Badges</span></a></li>
                        <li><a href="content-boxes.html" title="Content boxes"><span>Content boxes</span></a></li>
                        <li><a href="icons.html" title="Icons"><span>Icons</span></a></li>
                        <li><a href="nav-menus.html" title="Navigation menus"><span>Navigation menus</span></a></li>
                        <li><a href="response-messages.html" title="Response messages"><span>Response messages</span></a></li>
                        <li><a href="images.html" title="Images"><span>Images</span></a></li>
                    </ul>

                </div> .sidebar-submenu 
            </li>
            <li class="divider"></li>-->
            
        </ul><!-- #sidebar-menu -->
    </div>
</div>
<!--<div id="page-header" class="bg-green font-inverse">-->
<div id="page-header" class="bg-gradient-9 font-inverse">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
            <a href="index.html" class="logo-content-small" title="MonarchUI"></a>
        </div>
        <div id="header-logo" class="logo-bg">


            <a href="{{route('comapany.dashboard')}}" class="logo-content-big" title="Commission Website">
                Commission Website
            </a>
            <a href="{{route('comapany.dashboard')}}" class="logo-content-small" title="Commission Website">
                Commission Website
            </a>
            <a id="close-sidebar" href="#" title="Close sidebar">
                <i class="glyph-icon icon-angle-left"></i>
            </a>
        </div>
        <div id="header-nav-left">
            <div class="user-account-btn dropdown">
                <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                    <img width="28" src="{{asset('/comapany/assets/image-resources/gravatar.jpg')}}" alt="Profile image">
                    <span>Welcome</span>
                    <i class="glyph-icon icon-angle-down"></i>
                </a>
                <div class="dropdown-menu float-left">
                    <div class="box-sm">
                        <div class="login-box clearfix">
                            <div class="user-img">
                                <a href="#" title="" class="change-img">Change photo</a>
                                <img src="{{asset('/comapany/assets/image-resources/gravatar.jpg')}}" alt="">
                            </div>
                            <div class="user-info" style="color: #000; display: grid;">
                                <span>
<?php 
                                $admin = adminUser();
                                ucfirst($admin->first_name.' '.$admin->last_name)    
?>
                                </span>
                                
                                {!!  Html::decode(Html::link(route('comapany.users.edit', $admin->id),"Edit profile",['title'=>'Edit profile'])) !!}

                                {!!  Html::decode(Html::link(route('comapany.changepassword'),"Change Password",['title'=>'Change Password'])) !!}
                                
                                
                            </div>
                        </div>
                        <div class="divider"></div>
                       
                        <div class="pad5A button-pane button-pane-alt text-center">
                            <a href="{{ route('comapany.logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn display-block font-normal btn-danger">
                                <i class="glyph-icon icon-power-off"></i>
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('comapany.logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
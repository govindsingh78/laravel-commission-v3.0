@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">Price List</h3>
                    <div class="example-box-wrapper">
                        <div class="row"> 
                            <div class="size-md col-user-left">
                                {!!  Html::decode(Html::link(route('admin.price.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
                            </div>
                        </div>    
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="15%">Per Month</th>
                                    <th width="15%">Per Year</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!$prices->isEmpty())
                                @php $i=1; @endphp
                                @foreach ($prices as $price)
                                @php 
                                    $type = 'Free';
                                    if($price->type==1)
                                        $type = 'Premium';
                                @endphp
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$price->name}}</td>
                                    <td>{{$type}}</td>
                                    <td>{{$price->per_month}}</td>
                                    <td>{{$price->per_year}}</td>
                                    <td>
                        {!!  Html::decode(Html::link(route('admin.price.edit', $price->id),"<i class='glyphicon glyphicon-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                        {!!  Html::decode(Html::link(route('admin.price.destroy',['id' => $price->id]),"<i class='glyphicon glyphicon-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>'Delete', "data-alert"=>'Delete Role'])) !!}

                        <a href="javascript:void(0);" id="btn_view_{{$price->id}}" class="btn_view btn btn-primary" itemid="{{$price->id}}">View tags</a>



                                    </td> 
                                </tr>
                                <tr id="tr_tag_{{$price->id}}">
                                    
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">
                                        <div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>    
                        </table>
                        {!! $prices->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .example-box-wrapper .col-user-right{
        float: right;
        margin-right: 10px;
    }

    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }
    .btn-search{
        margin-bottom: 2px;
        margin-left: 4px;
    }
</style>

<script type="text/javascript">
$(function(){
    $(document).on('click', '.btn_view', function(){
    
        var itemid = $(this).attr('itemid');
        
        var btnText = $('#btn_view_'+itemid).text();
        if(btnText !='' && btnText=='View tags'){
            $('#btn_view_'+itemid).text('Hide tags');
            $('#tr_tag_'+itemid).slideDown();
        }else{
            $('#btn_view_'+itemid).text('View tags');
            $('#tr_tag_'+itemid).slideUp();
            return false;
        }
        
        $.ajax({
          type: "GET",
          headers: {'X-CSRF-TOKEN': csrf_token},
          url:"{{url('admin/price-tags')}}",
          data:{price_id:itemid},
          success: function(response) {
            if(response.status == 1){
                $('#tr_tag_'+itemid).html(response.data);
            }
          }
        }); 
    });
});
</script>


<script type="text/javascript">

$(document).ready(function(e){

    $(document).on('click', '.btn_view', function(){
        var itemid = $(this).attr('itemid');
        if(itemid !=''){

        }
    });
});
</script>

@endsection
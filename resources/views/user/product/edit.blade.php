
@extends('layouts.admin')

@section('content')
{!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!}
{!! Html::script(asset('public/admin/assets/widgets/multi-select/multiselect.js')) !!} 
<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <section class="content-header with-border">
                @include('includes.admin.breadcrumb')
            </section>
            <div class="panel">
                <div class="panel-body">
{!! Form::model($product,['method'=>'patch','route'=>['product.update',$product->id], 'files' => true]) !!} 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                {!! Form::label('Product Name ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Name']) !!}    
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('name') }}</div>
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('Product Price ',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('price',null,['class'=>'form-control','placeholder'=>'Price']) !!}    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">    
                            <div class="form-group col-md-6">
                                {!! Form::label('Product Title ',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Title']) !!}    
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="form-group col-md-12">
                            {!! Form::label('Product Description',null,['class'=>'col-sm-4']) !!}
                            <div class="col-sm-12">
                                {!! Form::textarea('description',null,['class'=>'form-control ckeditor','placeholder'=>'Description']) !!}
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('description') }}</div>
                        </div><!-- /.form-group -->
                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">
                            {!! Form::label('Product Image (Max upload 3)',null,['class'=>'col-sm-6']) !!}
                            <div class="col-sm-12">    
                                <div class="table-responsive">  
                                    <table class="table" id="tbl_add_img">  
                                        <tr>  
                                            <td>
                                                <input id="photos" type="file" class="form-control" name="photos[]" multiple maxlength="3">
                                            </td>  
                                        </tr>
                                        <div class="col-sm-12 text-danger">{{ $errors->first('photos') }}</div>  
                                    </table>
<?php
    $images = '';
    if(isset($product->photos)){
        foreach ($product->photos as $photo) {
            $images .= '<div id="img_'.$photo->id.'" class="img-container"><img src="'.asset('storage/app/'.$photo->product_img).'" height="100" width="100"><div class="btn_del_img"><i class="glyphicon glyphicon-remove btn_close" itemid="'.$photo->id.'"></i></div></div>';
        }
    }
?>
                                    <div id="dvPreview1">
                                        {!!$images!!}
                                        <input type="hidden" name="img_deleted" id="img_deleted" value="">
                                    </div>
                                    <div id="dvPreview"></div>
                                </div>
                            </div>    
                        </div>

                        <div class="col-md-6">

                            <div class="form-group col-sm-12">
{!! Form::label('Product Industries',null,['class'=>'col-sm-4']) !!}
                                <div class="col-sm-12">
{{ Form::select('industries[]', $industries, null, ['id'=>'inds', 'class' => 'multi-select', 'multiple'=>true]) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions col-sm-12 center">
                        <div class="col-sm-12">
                        {{Form::button('<i class="fa fa-undo"></i> Reset', array('type' => 'reset', 'class'=> 'btn btn-default btn-sm'))}}
                        {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

{!! Html::style(asset('public/admin/assets/widgets/multi-select/multiselect.css')) !!}
<style type="text/css">
    #tbl_add_img tr td {
        border: none;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){ 

        $(document).on('click', '.btn_close', function(){
            var itemid = $(this).attr('itemid');
            var del_img = $('#img_deleted').val();
            
            if(del_img !=''){
                del_img += ','+itemid;     
            }else{
                del_img = itemid;
            }
            $('#img_deleted').val(del_img);
            $('#img_'+itemid).fadeOut('slow');
        });

        $("#inds").multiSelect();
        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');

        var fileUpload = document.getElementById("photos");
        
        fileUpload.onchange = function () {
            if(typeof(FileReader) != "undefined") {
                var dvPreview = document.getElementById("dvPreview");
                dvPreview.innerHTML = "";
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                
                for (var i = 0; i < fileUpload.files.length; i++) {
                    var file = fileUpload.files[i];
                    if (regex.test(file.name.toLowerCase())) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var img = document.createElement("IMG");
                            img.height = "100";
                            img.width = "100";
                            img.src = e.target.result;
                            dvPreview.appendChild(img);
                        }
                        reader.readAsDataURL(file);
                    } else {
                        alert(file.name + " is not a valid image file.");
                        dvPreview.innerHTML = "";
                        return false;
                    }
                }    
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        }

    });  
</script>

<style type="text/css">
    .img-container {
        position: relative;
        width: 21%;
        display: inline-block;
        margin-right: 25px;
        margin-bottom: 10px;
    }

    .btn_del_img{
        position: absolute;
        top: -5px;
        right: -10px;
        background-color: #ddd;
        border-radius: 3px;
        width: 20px;
        height: 20px;
        text-align: center;
    }

    .btn_del_img > i {
        font-size: 10px;
        cursor: pointer;
    }
</style>

@endsection
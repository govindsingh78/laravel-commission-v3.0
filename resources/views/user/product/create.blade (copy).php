
@extends('layouts.admin')

@section('content')
{!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!} 
<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
            <div class="panel">
                <div class="panel-body">
                {!! Form::open(['route'=> 'product.store']) !!} 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                {!! Form::label('Name ',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Name']) !!}    
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('name') }}</div>
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('Price ',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('price',null,['class'=>'form-control','placeholder'=>'Price']) !!}    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">    
                            <div class="form-group col-md-6">
                                {!! Form::label('Title ',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                                <div class="col-sm-12">
                                    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Title']) !!}    
                                </div>
                            </div>
                        </div>
                    </div>    

                    <div class="row">
                        <div class="col-md-12">    

                            <div class="form-group col-md-12">
                                {!! Form::label('Description',null,['class'=>'col-sm-4']) !!}
                                <div class="col-sm-12">
                                    {!! Form::textarea('description',null,['class'=>'form-control ckeditor','placeholder'=>'Description']) !!}
                                </div>
                                <div class="col-sm-12 text-danger">{{ $errors->first('description') }}</div>
                            </div><!-- /.form-group -->
                        </div>
                    </div>

                    <div class="form-actions col-sm-12 center">
                        <div class="col-sm-12">
                        {{Form::button('<i class="fa fa-undo"></i> Reset', array('type' => 'reset', 'class'=> 'btn btn-default btn-sm'))}}
                        {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">

        <div class="container">
<section class="content-header with-border">
    @include('includes.admin.breadcrumb')
</section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">
                        User List
                    </h3>
                    <div class="example-box-wrapper">
                    <div class="row">    
                        <div class="size-md col-user-right">
{!! Form::open(['id'=>'search_form', $form_data['search'],'route'=> 'admin.'.$controller.'.index']) !!}                         
                            <table class="search_table" align="right">
                              <tbody>
                                <tr>
<td>{!! Form::text('search', '', ['class'=>'form-control', 'placeholder'=>'Search']) !!}</td>
<td>&nbsp;</td>                                  
                                <td>
<?php 
$status_list = array('' => 'Select status') + Config::get('global.status_list'); ?>

{!! Form::select('status', $status_list, $form_data['status'], ['class' => 'form-control','id'=>'status']) !!}
                                </td><td>&nbsp;</td>

                                <td>
                                {!! Form::select('role_id', array('' => 'Select role') + $allroles, $form_data['role_id'], ['class' => 'form-control','id'=>'roles']) !!}</td>
                                <td>
                                    <button class="btn btn-primary btn-search" type="submit">Search</button>
                                </td>
                            </tr>
                              </tbody>
                            </table>
            {!! Form::close() !!}
                        </div>

                        <div class="size-md col-user-left">
                            {!!  Html::decode(Html::link(route('admin.users.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
                        </div>
                    </div>    
						<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">S.No.</th>
                                    <th width="15%">Name</th>
                                    <th width="12%">Email</th>
                                    <th width="15%">Role</th>
                                    <th width="15%">Refer By</th>
                                    <th width="25%">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if(!empty($users))
                                @foreach ($users as $key => $user)
                                <?php 
                                    $sr_no = $key + 1; 
                                    
                                    $role = '';
                                    if(isset($user->role)){
                                        $role = $user->role->name;
                                    }

                                    $referby = '';
                                    if(isset($user->refer)){
                                        $referby = $user->refer->first_name;
                                    }
                                ?>
                                <tr>
                                    <td>{{ $sr_no }}</td>
                                    <td>{{ ucfirst($user->first_name.' '.$user->last_name) }}</td>
                                    <td>{{ ucfirst($user->email) }}</td>
                                    <td>{{$role}}</td>
                                    <td>{{$referby}}</td>
                                    <td align="center">
                                        
                                        @if($user->status == 1 )
                                            
                                            {!!  Html::decode(Html::link(route('admin.users.status_change',['id' => $user->id,'status'=>$user->status]),"<i class='glyphicon glyphicon-ok'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                            
                                        @else
                                        
                                            {!!  Html::decode(Html::link(route('admin.users.status_change',['id' => $user->id,'status'=>$user->status]),"<i class='glyphicon glyphicon-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        
                                        @endif
                                        
                                        {!!  Html::decode(Html::link(route('admin.users.edit', $user->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                        {!!  Html::decode(Html::link(route('admin.users.destroy',['id' => $user->id]),"<i class='glyphicon glyphicon-trash'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>'Delete', "data-alert"=>'Delete User'])) !!}

                                        {!!  Html::decode(Html::link(route('admin.dashboard',['r_id' => $user->role_id,'id'=>$user->id]),"<i class='glyphicon glyphicon-eye-open'></i>",['class'=>'btn btn-success','data-toggle'=>'tooltip','title'=>'View'])) !!}
                                    
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">
<div class="data_not_found"> Data Not Found </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                        </table>
                        {!! $users->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .example-box-wrapper .col-user-right{
        float: right;
        margin-right: 10px;
    }

    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }
    .btn-search{
        margin-bottom: 2px;
        margin-left: 4px;
    }
</style>
@endsection

    
@extends('layouts.blank')

@section('content')  

<style type="text/css">
    html,body {
        height: 100%;
        background: #fff;
        overflow: hidden;
    }
</style>

{!! Html::script(asset('public/admin/assets/widgets/wow/wow.js')) !!}
<script type="text/javascript">
    wow = new WOW({animateClass: 'animated',offset: 100});
    wow.init();
</script>

<img src="{{asset('public/admin/assets/image-resources/blurred-bg/blurred-bg-3.jpg')}}" class="login-img wow fadeIn" alt="">

<div class="center-vertical">
    <div class="center-content row">
        <div class="col-md-3 center-margin">
            <div class="content-box wow bounceInDown modal-content">
                <h3 class="content-box-header content-box-header-alt bg-default">
                    <span class="icon-separator">
                        <i class="glyph-icon icon-cog"></i>
                    </span>
                    <span class="header-wrapper">
                    Password Reminder
                    <small>You need to provide your registered email here</small>
                    </span>
                </h3>
                
                <div class="row">
                    <div class="col-md-12">
                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::open(array('route' =>  'admin.forgot_password', 'id' => 'form-reminder', 'class'=>'form-horizontal')) !!}
                    <div class="content-box-wrapper">
                        
                        <div class="form-group">
                            <div class="input-group">
                                
                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email','id'=>'email']) !!}
                        <div class="error" class="server-side help-block animation-slideUp">{{ $errors->first('email') }}</div>
                               
                            </div>
                        </div>
                        
                        <div class="form-group form-actions">
                            <div class="col-xs-6">
                                {!! Html::link(route('admin.login'), 'Cancel', array('class' => 'btn btn-default   btn-flat pull-left')) !!}
                            </div>
                            <div class="col-xs-6 text-right">
                                {!! Form::submit('Remind Password',['class'=>'btn btn-effect-ripple btn-sm btn-primary'])!!}
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop

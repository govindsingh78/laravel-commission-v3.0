@extends('layouts.admin')

@section('content') 
<div id="page-content-wrapper">
    <div id="page-content">

        <div class="container">


            <!-- Ckeditor -->

            {!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!}

            <script>

                // This code is generally not necessary, but it is here to demonstrate
                // how to customize specific editor instances on the fly. This fits well
                // this demo because we have editable elements (like headers) that
                // require less features.

                // The "instanceCreated" event is fired for every editor instance created.
                CKEDITOR.on('instanceCreated', function (event) {
                    var editor = event.editor,
                            element = editor.element;

                    // Customize editors for headers and tag list.
                    // These editors don't need features like smileys, templates, iframes etc.
                    if (element.is('h1', 'h2', 'h3') || element.getAttribute('id') == 'taglist') {
                        // Customize the editor configurations on "configLoaded" event,
                        // which is fired after the configuration file loading and
                        // execution. This makes it possible to change the
                        // configurations before the editor initialization takes place.
                        editor.on('configLoaded', function () {

                            // Remove unnecessary plugins to make the editor simpler.
                            editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                                    'forms,iframe,image,newpage,removeformat,' +
                                    'smiley,specialchar,stylescombo,templates';

                            // Rearrange the layout of the toolbar.
                            editor.config.toolbarGroups = [
                                {name: 'editing', groups: ['basicstyles', 'links']},
                                {name: 'undo'},
                                {name: 'clipboard', groups: ['selection', 'clipboard']},
                                {name: 'about'}
                            ];
                        });
                    }
                });

            </script>

            <style>

                *[contenteditable="true"]
                {
                    padding: 10px;
                }

                #container
                {
                    width: 960px;
                    margin: 30px auto 0;
                }

                #header
                {
                    overflow: hidden;
                    padding: 0 0 30px;
                    border-bottom: 5px solid #05B2D2;
                    position: relative;
                }

                #headerLeft,
                #headerRight
                {
                    width: 49%;
                    overflow: hidden;
                }

                #headerLeft
                {
                    float: left;
                    padding: 10px 1px 1px;
                }

                #headerLeft h2,
                #headerLeft h3
                {
                    text-align: right;
                    margin: 0;
                    overflow: hidden;
                    font-weight: normal;
                }

                #headerLeft h2
                {
                    font-family: "Arial",sans-serif;
                    font-size: 4.6em;
                    line-height: 1.1;
                    text-transform: uppercase;
                }

                #headerLeft h3
                {
                    font-size: 2.3em;
                    line-height: 1.1;
                    margin: .2em 0 0;
                    color: #666;
                }

                #headerRight
                {
                    float: right;
                    padding: 1px;
                }

                #headerRight p
                {
                    line-height: 1.8;
                    text-align: justify;
                    margin: 0;
                }

                #headerRight p + p
                {
                    margin-top: 20px;
                }

                #headerRight > div
                {
                    padding: 20px;
                    margin: 0 0 0 30px;
                    font-size: 1.4em;
                    color: #666;
                }

                #columns
                {
                    color: #333;
                    overflow: hidden;
                    padding: 20px 0;
                }

                #columns > div
                {
                    float: left;
                    width: 33.3%;
                }

                #columns #column1 > div
                {
                    margin-left: 1px;
                }

                #columns #column3 > div
                {
                    margin-right: 1px;
                }

                #columns > div > div
                {
                    margin: 0px 10px;
                    padding: 10px 20px;
                }

                #columns blockquote
                {
                    margin-left: 15px;
                }

                #tagLine
                {
                    border-top: 5px solid #05B2D2;
                    padding-top: 20px;
                }

                #taglist {
                    display: inline-block;
                    margin-left: 20px;
                    font-weight: bold;
                    margin: 0 0 0 20px;
                }

            </style>


            <div id="page-title">
                <h2>{{$pageTitle}}</h2>
                <p>{{$title}}</p>
            </div>

            <div class="panel">
                <div class="panel-body">
                    {!!Form::model($setting, ['route' => ['admin.settings.update', $setting->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH',  'files' => true  , 'id' => 'edit-settings']) !!}
                    <div class="form-group clearfix col-sm-6">
                        {!! Form::label(trans('admin.SITE_TITLE'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('site_title',null,['class'=>'form-control','placeholder'=>trans('admin.SITE_TITLE')]) !!}   
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('site_title') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.COPYRIGHT'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('copyright',null,['class'=>'form-control','placeholder'=>trans('admin.COPYRIGHT')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('copyright') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.SITE_LOGO'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::file('site_logo',['class'=>'form-control', 'id' => 'prev_img']) !!}
                        </div>
                        <?php 
                            $min_width = Config::get('global.logo_min_width');
                            $min_height = Config::get('global.logo_min_height');
                            $max_width = Config::get('global.logo_max_width');
                            $max_height = Config::get('global.logo_max_height');
                        ?>
                        <div class="col-sm-12 text-notify" style="color: #d69f07;">Note : Please upload an image of ({{$min_width}} * {{$min_height}} to {{$max_width}} * {{$max_height}}) dimentions.</div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('site_logo') }}</div>
                        <img id="show_img" rel="{{$setting['site_logo']}}" src="{{asset(SITE_IMAGES_URL . $setting['site_logo'])}}" alt="your image" style="width: 200px; margin: 15px 0 0 10px;"/>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.SITE_FOOTER_REFER_IMAGE'),null,['class'=>'col-sm-6  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::file('footer_advt_img',['class'=>'form-control', 'id' => 'prev_img_1']) !!}
                        </div>
                        <?php 
                            $min_width = Config::get('global.earn_ad_min_width');
                            $min_height = Config::get('global.earn_ad_min_height');
                            $max_width = Config::get('global.earn_ad_max_width');
                            $max_height = Config::get('global.earn_ad_max_height');
                        ?>
                        <div class="col-sm-12 text-notify" style="color: #d69f07;">Note : Please upload an image of ({{$min_width}} * {{$min_height}} to {{$max_width}} * {{$max_height}}) dimentions.</div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('footer_advt_img') }}</div>
                        <img id="show_img_1" rel="{{$setting['footer_advt_img']}}" src="{{asset(SITE_IMAGES_URL . $setting['footer_advt_img'])}}" alt="your image" style="width: 100px; margin: 15px 0 0 10px;"/>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group clearfix col-sm-6">
                        {!! Form::label(trans('admin.SITE_SLIDER_TITLE'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('site_slider_title',null,['class'=>'form-control','placeholder'=>trans('admin.SITE_SLIDER_TITLE')]) !!}   
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('site_title') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group clearfix col-sm-6">
                        {!! Form::label(trans('admin.SITE_SLIDER_DESCRIPTION'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('site_slider_description',null,['class'=>'form-control','placeholder'=>trans('admin.SITE_SLIDER_DESCRIPTION')]) !!}   
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('site_title') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.ADMIN_MOBILE_NO'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('mobile',null,['class'=>'form-control','placeholder'=>trans('admin.PHONE_NUMBER')]) !!}
                            <div class="col-sm-12 text-danger">{{ $errors->first('mobile') }}</div>
                        </div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.ADMIN_EMAIL'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL')]) !!}
                            <div class="col-sm-12 text-danger">{{ $errors->first('email') }}</div>
                        </div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.ADMIN_PAGE_LIMIT'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('page_limit',null,['class'=>'form-control','placeholder'=>trans('admin.ADMIN_PAGE_LIMIT')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('page_limit') }}</div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.FRONT_PAGE_LIMIT'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('front_page_limit',null,['class'=>'form-control','placeholder'=>trans('admin.FRONT_PAGE_LIMIT')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('front_page_limit') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.FROM_NAME'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('from_name',null,['class'=>'form-control','placeholder'=>trans('admin.FROM_NAME')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('from_name') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.FROM_EMAIL'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('from_email',null,['class'=>'form-control','placeholder'=>trans('admin.FROM_EMAIL')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('from_email') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.REPLY_TO_EMAIL'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('reply_to_email',null,['class'=>'form-control','placeholder'=>trans('admin.REPLY_TO_EMAIL')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('reply_to_email') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.SITE_EMERGENCY_NOTE'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('site_emergency_note',null,['class'=>'form-control','placeholder'=>trans('admin.SITE_EMERGENCY_NOTE')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('site_emergency_note') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.META_TITLE'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('meta_title',null,['class'=>'form-control','placeholder'=>trans('admin.META_TITLE')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('meta_title') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.META_KEYWORDS'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('meta_keywords',null,['class'=>'form-control','placeholder'=>trans('admin.META_KEYWORDS')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('meta_keywords') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.META_DESCRIPTION'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::textarea('meta_description',null,['class'=>'form-control','placeholder'=>trans('admin.META_DESCRIPTION')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('meta_description') }}</div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.EMAIL_SIGNATURE'),null,['class'=>'col-sm-4  required_label    ']) !!}
                        <div class="col-sm-12">
                            {!! Form::textarea('email_signature',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL_SIGNATURE')]) !!}
                        </div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('email_signature') }}</div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.FOOTER_INFO'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::textarea('address',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS')]) !!}
                            <div class="col-sm-12 text-danger">{{ $errors->first('address') }}</div>
                        </div>
                    </div><!-- /.form-group -->
                    
                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.CONTACT_ADDRESS'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::textarea('contact_address',null,['class'=>'form-control','placeholder'=>trans('admin.CONTACT_ADDRESS')]) !!}
                            <div class="col-sm-12 text-danger">{{ $errors->first('contact_address') }}</div>
                        </div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.SITE_ADVT_IMAGE'),null,['class'=>'col-sm-6  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::file('site_advt_image',['class'=>'form-control', 'id' => 'prev_img_2']) !!}
                        </div>
                        <?php 
                            $min_width = Config::get('global.site_ad_min_width');
                            $min_height = Config::get('global.site_ad_min_height');
                            $max_width = Config::get('global.site_ad_max_width');
                            $max_height = Config::get('global.site_ad_max_height');
                        ?>
                        <div class="col-sm-12 text-notify" style="color: #d69f07;">Note : Please upload an image of ({{$min_width}} * {{$min_height}} to {{$max_width}} * {{$max_height}}) dimentions.</div>
                        <div class="col-sm-12 text-danger">{{ $errors->first('site_advt_image') }}</div>
                        <img id="show_img_2" rel="{{$setting['site_advt_image']}}" src="{{asset(SITE_IMAGES_URL . $setting['site_advt_image'])}}" alt="your image" style="width: 200px; height: 100px; margin: 15px 0 0 10px;"/>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        {!! Form::label(trans('admin.SOCIAL_ICON'),null,['class'=>'col-sm-4  required_label']) !!}
                        <div class="col-sm-12">
                            {!! Form::label(trans('admin.FACEBOOK_ICON'),null,['class'=>'col-sm-4  required_label']) !!} 
                            <div class='col-sm-8'>
                                {!! Form::text('facebook_icon',null,['class'=>'form-control','placeholder'=>trans('admin.FACEBOOK_ICON')]) !!}
                                <div class="col-sm-12 text-danger">{{ $errors->first('facebook_icon') }}</div>
                            </div>
                        </div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        <div class="col-sm-12">
                            {!! Form::label(trans('admin.TWITTER_ICON'),null,['class'=>'col-sm-4  required_label']) !!} 

                            <div class='col-sm-8'>
                                {!! Form::text('twitter_icon',null,['class'=>'form-control','placeholder'=>trans('admin.TWITTER_ICON')]) !!}
                                <div class="col-sm-12 text-danger">{{ $errors->first('twitter_icon') }}</div>
                            </div>
                        </div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        <div class="col-sm-12">
                            {!! Form::label(trans('admin.GOOGLE_PLUS_ICON'),null,['class'=>'col-sm-4  required_label']) !!} 

                            <div class='col-sm-8'>
                                {!! Form::text('google_plus_icon',null,['class'=>'form-control','placeholder'=>trans('admin.GOOGLE_PLUS_ICON')]) !!}
                                <div class="col-sm-12 text-danger">{{ $errors->first('google_plus_icon') }}</div>
                            </div>
                        </div>
                    </div><!-- /.form-group -->

                    <div class="form-group col-md-6">
                        <div class="col-sm-12">
                            {!! Form::label(trans('admin.LINKEDIN_ICON'),null,['class'=>'col-sm-4  required_label']) !!} 

                            <div class='col-sm-8'>
                                {!! Form::text('linkedin_icon',null,['class'=>'form-control','placeholder'=>trans('admin.LINKEDIN_ICON')]) !!}
                                <div class="col-sm-12 text-danger">{{ $errors->first('linkedin_icon') }}</div>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    
                    

                    <div class="form-actions col-sm-12 center">
                        <div class="col-sm-12">
                            {!! Html::link(route('admin.settings'), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-default pull-left']) !!}
                            {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    
<script>
    $(document).ready(function(){
        var src = $('#show_img').attr('rel');
        var src_1 = $('#show_img_1').attr('rel');
        var src_2 = $('#show_img_2').attr('rel');
        
        if(!src){
            $('#show_img').addClass('hide');
        }
        
        if(!src_1){
            $('#show_img_1').addClass('hide');
        }
        
        if(!src_2){
            $('#show_img_2').addClass('hide');
        }
    });
    
    $("#prev_img").change(function() {
        readURL(this);
        $('#show_img').removeClass('hide');
    });
    
    $("#prev_img_1").change(function() {
        readURLs(this);
        $('#show_img_1').removeClass('hide');
    });
    
    $("#prev_img_2").change(function() {
        readURL2(this);
        $('#show_img_2').removeClass('hide');
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#show_img').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };
    
    function readURLs(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#show_img_1').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };
    
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#show_img_2').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };
</script>
    
</div>
@endsection
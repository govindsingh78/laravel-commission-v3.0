@extends('layouts.admin')

@section('content') 
 <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
<section class="content-header with-border">
    @include('includes.admin.breadcrumb')
</section>
<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    CMS List
</h3>

<div class="example-box-wrapper">
    <div class="row">
        <div class="size-md col-user-left">
            {!!  Html::decode(Html::link(route('admin.cms.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
        </div>
    </div>
<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
<thead>
    <tr>
        <th width="10%">Sr. No.</th>
        <th width="30%">Title</th>
        <th width="20%">Created At</th>
        <th width="20%">Updated At</th>
        <th width="20%">Action</th>
    </tr>
</thead>

<tbody>
  @if(!empty($cmslist))
        @foreach ($cmslist as $key => $cms)
        <?php $sr_no = $key + 1; ?>
        <tr>
            <td>{{ $sr_no }}</td>
            <td>{{ ucfirst($cms->title) }}</td>
            <td>{{ date_val($cms->created_at,DATE_FORMATE ) }}</td>
            <td>{{ date_val($cms->updated_at,DATE_FORMATE ) }}</td>
            <td align="center">
                @if($cms->status == 1)
                {!!  Html::decode(Html::link(route('admin.cms.status_change',['id' => $cms->id,'status'=>$cms->status]),"<i class='glyphicon glyphicon-ok'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                @else
                {!!  Html::decode(Html::link(route('admin.cms.status_change',['id' => $cms->id,'status'=>$cms->status]),"<i class='glyphicon glyphicon-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                @endif
                {!!  Html::decode(Html::link(route('admin.cms.edit', $cms->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

            </td>
            @endforeach
        @else
    <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>
    @endif

</tbody>
</table>
{!! $cmslist->appends(Input::all('page'))->render() !!}
</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }
</style>
@endsection

@extends('layouts.admin')
@section('content') 

<div id="page-content-wrapper">
    <div id="page-content">
        <div class="container">
<section class="content-header with-border">
    @include('includes.admin.breadcrumb')
</section>
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">
                        SLIDER IMAGES
                    </h3>

                    <div class="example-box-wrapper">
                        <div class="row">
                            <div class="size-md col-user-left">
                                {!!  Html::decode(Html::link(route('admin.sliders.create'),"<i class='glyphicon glyphicon-plus'></i> Add New",['class'=>'btn btn-primary btn-md toggle-vis'])) !!}
                            </div>
                        </div>    
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">Sr. No.</th>
                                    <th width="15%">Title</th>
                                    <th width="20%">Image</th>
                                    <th width="15%">Created At</th>
                                    <th width="15%">Updated At</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if(!empty($sliderlist))
                                    @foreach ($sliderlist as $key => $slider)
                                        <?php $sr_no = $key + 1; ?>
                                        <tr>
                                            <td>{{ $sr_no }}</td>
                                            <td>{{ ucfirst($slider->title) }}</td>
                                            <td><img src="{{asset(SLIDER_IMAGES_URL . $slider->image)}}" style="width: 200px; height: 100px;"/></td>
                                            <td>{{ date_val($slider->created_at, DATE_FORMATE ) }}</td>
                                            <td>{{ date_val($slider->updated_at, DATE_FORMATE ) }}</td>
                                            <td align="center">
                                                @if($slider->status == 1)
                                                    {!!  Html::decode(Html::link(route('admin.sliders.status_change',['id' => $slider->id,'status'=>$slider->status]),"<i class='glyphicon glyphicon-ok'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                                @else
                                                    {!!  Html::decode(Html::link(route('admin.sliders.status_change',['id' => $slider->id,'status'=>$slider->status]),"<i class='glyphicon glyphicon-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                                @endif
                                                {!!  Html::decode(Html::link(route('admin.sliders.edit', $slider->id),"<i class='glyphicon glyphicon-pencil'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">
                                            <div class="data_not_found"> Data Not Found </div>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        {!! $sliderlist->appends(Input::all('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .example-box-wrapper .col-user-left{
        float: left;
        margin-left: 10px;
    }
</style>
@endsection
@extends('layouts.admin')

@section('content') 
 <div id="page-content-wrapper">
            <div id="page-content">
                
                    <div class="container">
                    

<!-- Ckeditor -->

{!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!}

<script>

    // This code is generally not necessary, but it is here to demonstrate
    // how to customize specific editor instances on the fly. This fits well
    // this demo because we have editable elements (like headers) that
    // require less features.

    // The "instanceCreated" event is fired for every editor instance created.
    CKEDITOR.on( 'instanceCreated', function( event ) {
        var editor = event.editor,
                element = editor.element;

        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                'forms,iframe,image,newpage,removeformat,' +
                'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }
    });

</script>

<div class="panel">
    <div class="panel-body">
      
        {!! Form::model($emailtemplates,['method'=>'patch','route'=>['admin.'.$controller.'.update',$emailtemplates->id]]) !!} 
                                <div class="form-group clearfix col-sm-6">
                                    {!! Form::label(trans('admin.NAME'),null,array('class' => 'col-sm-6 control-label no-padding-right required_label')) !!}
                                    <div class="col-sm-12">
                                       {!! Form::text('name',$emailtemplates->name,['class'=>'form-control','placeholder'=>'Name']) !!}   
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('name') }}</div>
                                </div>
                                <div class="form-group clearfix col-sm-6">
                                    {!! Form::label(trans('admin.SUBJECT'),null,array('class' => 'col-sm-6 control-label no-padding-right')) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('subject',$emailtemplates->subject,['class'=>'form-control','placeholder'=>'Subject']) !!} 
                                    </div>
                                    <div class="col-sm-12 text-danger">{{ $errors->first('description') }}</div>
                                </div>      

                                <div class="form-group clearfix col-sm-6">
                                     {!! Form::label(trans('admin.INSERT_CONSTRAINTS'),null,array('class' => 'col-sm-6 control-label no-padding-right')) !!}
                                    <div class="col-md-8">
                                        <?php $constraints = explode(',',$emailtemplates->template_constants);
                                          $constraints_array  = array();
                                          $constraints_array['']  = '--';
                                          foreach ($constraints as $key => $value) {
                                            $constraints_array[$value]  = $value;
                                          }
                                         ?>
                                         {!! Form::select('template_constants', $constraints_array,null,['class'=>' autocomplete select2 form-control','id'=>'list_constant']) !!}
                                          </div>
                                         <div class="col-md-4">{!! Form::button('Insert',['class' => 'btn btn-info','id'=>'insert_constatnt'])!!}</div>
                                   
                                    <div class="col-sm-12 text-danger">{{ $errors->first('meta_keywords') }}</div>
                                </div>  

                                <div class="form-group clearfix col-sm-12">
                                    {!! Form::label('Body',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                                    <div class="col-sm-12">
                                       {!! Form::textarea('body',$emailtemplates->body,['class'=>'form-control ckeditor','placeholder'=>'Body', 'id'=>'ck_editor']) !!}  

                                    </div>
                                </div>  

                                <div class="form-actions col-sm-12 center">
                                    <div class="col-sm-12">
                                    {{Form::button('<i class="fa fa-undo"></i> Reset', array('type' => 'reset', 'class'=> 'btn btn-default btn-sm'))}}
                                    {{Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class'=> 'btn btn-success btn-sm' ))}}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                            {!! Form::close() !!}
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){ 
    $('#insert_constatnt').click(function()
    {
        var current_template_instance = "ck_editor";
        var current_text        = $("#list_constant").val();
        var oEditor           =   CKEDITOR.instances[current_template_instance] ;
        oEditor.insertHtml(current_text) ;
    });
});
</script>

@endsection
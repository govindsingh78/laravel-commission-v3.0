<?php
   /*
    |--------------------------------------------------------------------------
    | Validation Language Lines for admin 
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

return [
    'SITE_TITLE'         => 'Site Title',
    'ADMIN_PAGE_LIMIT'   =>'Admin Page Limit',
    'GLOBAL_SETTINGS'    =>'Global Settings',
    'SUBMIT'             =>'Submit',
    'CANCEL'             =>'Cancel',
    'FRONT_PAGE_LIMIT'=>'Front Page Limit',
    'FROM_NAME'=>'From Name',
    'NAME'=>'Name',
    'EMAIL_SIGNATURE'=>'Email Signature',
    'REPLY_TO_EMAIL'=>'Reply To Email',
    'META_TITLE'=>'Meta Title',
    'META_KEYWORDS'=>'Meta Keywords',
    'STAFF_MAIL'=>'Staff Mail',
    'META_DESCRIPTION'=>'Meta Description',
    'FROM_EMAIL'=>'From E-Mail',
    'COPYRIGHT'=>'Copyright',
    'CONFIG_MANAGEMENT'=>'Config Management',
    'DASHBOARD'=>'Dashboard',
    'RESET_PASSWORD'=>'Reset Password',
    'EMAIL_LOG'=>'EmailLog',
    'EMAIL_TYPE'=>'Email Type',
    'EMAIL_FROM'=>'Email From',
    'EMAIL_To'=>'Email To',
    'SENT_ON'=>'Email On',
    'ACTION'=>'Action',
    'DELETE'=>'Delete',
    'VIEW'=>'View',
    'DELETE_ALERT'=>'Are you sure you want to delete this records?',
    'EMAILLOG_DELETED_SUCCESSFULLY'=>'EmailLog deleted successfully.',
    'VIEW_EMAIL_LOG'=>'View EmailLog',
    'BACK'=>'Back',
    'TO'=>'To',
    'CMS_PAGES'=>'Cms Pages',
    'TITLE'=>'Title',
    'DESCRIPTION'=>'Description',
    'STATUS'=>'Status',
    'META_TITLE'=>'Meta Title',
    'META_KEYWORDS'=>'Meta Keywords',
    'META_DESCRIPTION'=>'Meta Description',
    'SAVE'=>'Save',
    'RESET'=>'Reset',
    'CREATED_AT'=>'Created At',
    'UPDATED_AT'=>'Updated At',
    'ADD_NEW'=>'Add New',

    'ADD_CMS_PAGES'=>'Add Cms Page',
    'CMSPAGES_ADD_SUCCESSFULLY'=>'Cms page added successfully',
    'ACTIVE'=>'Active',
    'ACTIVE_ALERT'=>'Are you sure you want to active this records?',
    'INACTIVE'=>'Inactive',
    'INACTIVE_ALERT'=>'Are you sure you want to inactive this records?',
    'EDIT'=>'Edit',
    'SHOW'=>'View',
    'CMSPAGES_CHANGE_STATUS_SUCCESSFULLY'=>'Cms page status changed successfully',
    'CMSPAGES_UPDATE_SUCCESSFULLY'=>'Cms page updated successfully',
    'CANCEL'=>'Cancel',
    'EDIT_CMS_PAGES'=>'Edit Cms Page',
    'INVALID_URL'=>'Requested URL is invlaid',
    'HELLO'=>'Hello',
    'MY_PROFILE'=>'My Profile',
    'FIRST_NAME'=>'First Name',
    'LAST_NAME'=>'Last Name',
    'PRICE'=>'Price',
    'price'=>'Price',
    'EMAIL'=>'Email',
    'BACK_TO_DASHBOARD'=>'Back To Dashboard',
    'CHANGE_PASSWORD'=>'Change Password',
    'OLD_PASSWORD'=>'Old Password',
    'NEW_PASSWORD'=>'New Password',
    'CONFIRM_PASSWORD'=>'Confirm Password',
    'PASSWORD_CHANGE_SUCCESS'=>'Your Account password changed successfully',
    'MY_PROFILE_UPDATE_SUCCESS'=>'Your Profile updated successfully',
    'LOGIN_EMAIL_PASSWORD_NOT_MATCH'=>'Incorrect combination of email address or password',
    'LOGIN_SUCCESSFULLY'=>'Welcome to :site_tite,&nbsp;  :Name',
    'LOGOUT_SUCCESSFULLY'=>'You have successfully logged out',
    'NOT_LOGIN'=>'You are not logged in',
    'NOT_AUTHORITY'=>'You have not authority',
    'EMAIL_TEMPLATES'=>'Email Templates',
    'EDIT_EMAIL_TEMPLATES'=>'Edit Email Templates',
    'NAME'=>'Name',
    'SUBJECT'=>'Subject',
    'INSERT_CONSTRAINTS'=>'Insert Constraints',
    'FOOTER_CONTACT_INFO'=>'Footer Contact Info',
    'FOOTER_INFO'=>'Footer Info',
    'ADMIN_EMAIL'=>'Admin Email',
    'ADMIN_MOBILE_NO'=>'Admin Mobile No.',
    'SITE_EMERGENCY_NOTE'=>'Site Emergency Note',
    'SITE_SLIDER_TITLE'=>'Site Slider Title',
    'SITE_SLIDER_DESCRIPTION'=>'Site Slider Description',
    'SOCIAL_ICON'=>'Social Icon',
    'FACEBOOK_ICON'=>'Facebook_Icon',
    'TWITTER_ICON'=>'Twitter_Icon',
    'GOOGLE_PLUS_ICON'=>'Google_Plus_Icon',
    'LINKEDIN_ICON'=>'Linkedin Icon',
    'PHONE_NUMBER'=>'Phone Number',
    'BLOG_CATEGORY'=>'Blog Categories',


    /******** User Management Start   */
    'USERS'=>'Users',
    'PHONE'=>'Phone Number',
    'SEND_CREDENTIALS'=>'Send Credential',
    'ADD_USER'=>'Add User',
    'EDIT_USER'=>'Edit User',
    'PASSWORD'=>'Password',
    'USER_ADD_SUCCESSFULLY'=>'User added successfully.',
    'USER_CHANGE_STATUS_SUCCESSFULLY'=>'User status changed successfully.',
    'PROFILE_IMAGE'=>'Profile Image',
    'USER_UPDATE_SUCCESSFULLY'=>'User updated successfully',
    'CREDENTIALS_SEND_SUCCESSFULLY'=>'Credentials send successfully',

    /******** User Management End   */

    /************CATEGORY management start **************/
    'CATEGORY'=>'Categories',
    'CATEGORY_TYPE'=>'Category Type',
    'ADD_CATEGORY'=>'Add Category',
    'EDIT_CATEGORY'=>'Edit Category',
    'MAIN_CATEGORY'=>'Mian Category',
    'MAIN_CATEGORY'=>'Mian Category',
    'SUB_CATEGORY'=>'Sub Category',
    'CATEGORY_ADD_SUCCESSFULLY'=>'Category add successfully',
    'CATEGORY_CHANGE_STATUS_SUCCESSFULLY'=>'Category status changed  successfully',
    'CATEGORY_UPDATE_SUCCESSFULLY'=>'Category updated successfully',
    'PLEASE_SELECT'=>'Please Select',
    'SUBCATEGORY'=>'Subcategory',
    'TAG_CATEGORY'=>'Tags',
    /************CATEGORY management end **************/

    'BLOG_CATEGORY_TYPE'=>'Blog Category Type',
    'ADD_BLOG_CATEGORY'=>'Add Blog Category',
    'EDIT_BLOG_CATEGORY'=>'Edit Blog Category',
    'MAIN_BLOG_CATEGORY'=>'Main Blog Category',
    'SUB_BLOG_CATEGORY'=>'Sub Blog Category',
    'BLOG_CATEGORY_ADD_SUCCESSFULLY'=>'Blog Category add successfully',
    'BLOG_CATEGORY_CHANGE_STATUS_SUCCESSFULLY'=>'Blog Category status changed  successfully',
    'BLOG_CATEGORY_UPDATE_SUCCESSFULLY'=>'Blog Category updated successfully',
    'BLOGS_ADD_SUCCESSFULLY'=>'Blog add successfully',
    'BLOGS_CHANGE_STATUS_SUCCESSFULLY'=>'Blog status changed  successfully',
    'BLOGS_UPDATE_SUCCESSFULLY'=>'Blog updated successfully',
    



    /* Faq Start*/
    
    'ADD_FAQ_CATEGORY'=>'Add Faq Category',

    'FAQCATEGORY'=>'Faq Categories',    



    'ALL_FAQ_CATEGORIES'=>'All Faq Category',
    'FAQ_CATEGORY_ADD_SUCCESSFULLY'=>'Faq category add successfully',
    'EDIT_FAQ_CATEGORY'=>'Edit Faq Category',
    'FAQ_CATEGORY_UPDATE_SUCCESSFULLY'=>'Faq Category Update Successfully',
    'FAQ_CATEGORY_DELETE_SUCCESSFULLY'=>'Faq Category Delete Successfully',
    'ALL_FAQ'=>'Faq',
    'ADD_FAQ'=>'Add Faq',
    'FAQ_ADD_SUCCESSFULLY'=>'Faq Add Successfully',
    'EDIT_FAQ'=>'Edit Faq',
    'FAQ_DELETE_SUCCESSFULLY'=>'Faq Delete Successfully',
    'FAQ_UPDATE_SUCCESSFULLY'=>'Faq Update Successfully',
    'FAQPAGES_CHANGE_STATUS_SUCCESSFULLY'=>'Faqpages Change Status Successsfully',
    'FAQ'=>'Faq',
    





    /*Newsletter Management System**/

    'NEWSLETTER_MANAGEMENT'=>'Newsletter Management',
    'NEWSLETTER'=>'Newsletter',
    'NEWSLETTER_TEMPLATE'=>'Newsletter Template',
    'ADD_NEWSLETTER_TEMPLATE'=>'Add Newsletter Templates',
    'EDIT_NEWSLETTER_TEMPLATE'=>'Edit Newsletter Templates',
    'BODY'=>'Body',
    'NEWSLETTER_TEMPLATE_ADD_SUCCESSFULLY'=>'Newsletter template added successfully',
    'NEWSLETTER_TEMPLATE_UPDATE_SUCCESSFULLY'=>'Newsletter template updated successfully',
    'NEWSLETTER_TEMPLATE_CHANGE_STATUS_SUCCESSFULLY'=>'Newsletter template status changed successfully',
    'NEWSLETTER_TEMPLATE_DELETE_SUCCESSFULLY'=>'Newsletter template deleted successfully',
    'MANAGE_SCHEDULE'=>'Manage Schedule',
    'ALL_SUBSCRIBERS'=>'All Subscribers',
    'ALL_EVENTPLANS'=>'All Event Plans',
    'NEWSLETTER_SUBSCRIBERS'=>'Newsletter Subscribers',
    'ADD_SUBSCRIBERS'=>'Add Subscribers',
    'EDIT_SUBSCRIBERS'=>'Edit Subscribers',
    'SUBSCRIBERS_ADD_SUCCESSFULLY'=>'Subscriber added successfully',
    'SUBSCRIBERS_CHANGE_STATUS_SUCCESSFULLY'=>'Subscriber status changed successfully',
    'SUBSCRIBERS_UPDATE_SUCCESSFULLY'=>'Subscriber updated successfully',
    'SUBSCRIBERS_DELETE_SUCCESSFULLY'=>'Subscriber deleted successfully',
    'USER_LIST'=>'User List',
    'DATE'=>'Date',
    'NEWSLETTER_SCHEDULED_SUCCESSFULLY'=>'Newsletter scheduled successfully',
    'NEWSLETTER_CAMPAIGN'=>'Newsletter Campaign',
    'SEND_MANUALLY'=>'Send Manually',
    'NEWSLETTER_CAMPAIGN_CHANGE_STATUS_SUCCESSFULLY'=>'Newsletter Campaign status changed successfully',
    'NEWSLETTER_SENDED_SUCCESSFULLY'=>'Newsletter Campaign deleted successfully',
    'NEWSLETTER_CAMPAIGN_DELETE_SUCCESSFULLY'=>'Newsletter send  successfully',
    'EXECUTED'=>'Executed',
    'VISITORS'=>'Visitors',
    'VISITOR_NAME'=>'Visitors Name',
    'IP_ADDRESS'=>'IP Address',
    'LOCATION'=>'Location',
    'BROWSER_DEVICE'=>'Browser Device',
    'BROWSER_NAME'=>'Browser Name',
    'BROWSER_VERSION'=>'Browser Version',
    'PAGE_COUNT'=>'Page count',
    'VIEW_DETAIL'=>'View Detail',
    'VISITOR_HISTORY'=>'Visitor History',
    'VISITOR_TYPE'=>'Visitor Type',
    'DATE'=>'Date',
    'VISIT_PAGES'=>'Visit Pages',
    'PAGE_URL'=>'Page URL',
    /***Newsletter Management System End             */

    /*  Blog Management   start */
    'BLOGS'=>'Blogs',
    'BLOG'=>'Blog',
    'ADD_BLOGS'=>'Add Blog',
    'EDIT_BLOG'=>'Edit Blog',
    'ADD_Blog' => 'Add Blog',
    'Blog' => 'Blog Pages',
    'IMAGE'=>'Image',
    'CATEGORY_ID'=>'Category',
    'BLOG_CATEGORY_ID'=>'Blogs',
    'TAG_ID'=>'Tags',
    'BLOG_ADD_SUCCESSFULLY'=>'Blog Added Successfully.',
    'BLOG_UPDATE_SUCCESSFULLY'=>'Blog Updated Successfully.',
    'BLOG_CHANGE_STATUS_SUCCESSFULLY'=>'Blog Status changed Successfully.',
    'BLOG_DELETE_SUCCESSFULLY'=>'Blog Deleted Successfully.',

    /*  Blog Management  end */

    /* Blog Category Start*/
    'ALL_BLOG_CATEGORIES'=>'All Blog Category',
    'ADD_BLOG_CATEGORY'=>'Add Blog Category',
    'BLOG_CATEGORY_ADD_SUCCESSFULLY'=>'Blog Category Add Successfully',
    'EDIT_BLOG_CATEGORY'=>'Edit Blog Category',
    'BLOG_CATEGORY_UPDATE_SUCCESSFULLY'=>'Blog Category Update Successfully',
    'BLOG_CATEGORY_DELETE_SUCCESSFULLY'=>'Blog Category Deleted Successfully',

    /* Blog Category End*/

    /*  Admin Menu Management   start */
        'ADMIN_MENUS'=>'Admin Menus',
        'ADD_MENU'=>'Add Menu',
        'EDIT_MENU'=>'Edit Menu',
        'SHOW_ON_DESHBOARD'=>'Show on dashboard',
        'ROUTE'=>'route',
        'ICON'=>'icon',
        'MENU_ORDER'=>'Menu Order',
        'ORDER'=>'Order',
        'ADMIN_MENUS_ADD_SUCCESSFULLY'=>'Admin Menu added successfully.',
        'YES'=>'Yes',
        'NO'=>'No',
        'CHILD_MENU'=>'Child Menu',
        'ADMIN_MENUS_UPDATE_SUCCESSFULLY'=>'Admin Menu updated successfully.',
        'ADMIN_MENUS_CHANGE_STATUS_SUCCESSFULLY'=>'Admin Menu status changed successfully.',
        'MENUS'=>'Menu',

    /* Admin Menu Management   end */

  /*  Menu Management   start */


    'DYNAMIC'=>'Dynamic',
    'TYPE'=>'Type',
    'PARAMETER'=>'Parameter',
    'POSITION'=>'Position',
    'PARAMETER_INFO'=>'Please Enter Parameter in JSON form Like:- {a:1,b:2,c:3,d:4} ',
    'MENUS_ADD_SUCCESSFULLY'=>'Menu added successfully.',
    'MENUS_UPDATE_SUCCESSFULLY'=>'Menu updated successfully.',
    'MENUS_CHANGE_STATUS_SUCCESSFULLY'=>'Menu status changed successfully.',
  /* Menu Management   end */

  /*  Event Management   start */
    'MAX_EVENT_ALLOWED'=>'Maximum Event Allowed',
    'VALID_DAYS'=>'Valid Days',
    'VISIBILITY'=>'Visibility',
    'ADD_EVENT_PLANS'=>'Add Event Plans',
    'EDIT_EVENT_PLANS'=>'Edit Event Plans',
    'ALL_EVENT_PLANS'=>'All Event Plan Types',
    'EVENT_PLANS_ADD_SUCCESSFULLY'=>'Events plan added successfully',
    'EVENT_PLANS_DELETE_SUCCESSFULLY'=>'Events plan delete successfully',
    'EVENT_UPDATE_SUCCESSFULLY'=>'Events plan updated successfully',
  /* Menu Management   end */

  /*  Orzanizors Management   start */
    'COMPANY_NAME'=>'Company Name',
    'NO_EVENT_DONE'=>'Number of Events Done',
    'INTREST'=>'Intrest',
    'ADD_ORGANIZERS'=>'Add Organizers',
    'EDIT_ORGANIZERS'=>'Edit Organizers',
    'ALL_ORGANIZERS'=>'All Organizers',
    'ABOUT_ME'=>'About Me',
    'CITY'=>'City',
    'STATE'=>'State',
    'COUNTRY'=>'Country',
    'INTREST'=>'Interest',
    'PINCODE'=>'Pincode',
    'ADDRESS'=>'Address',
    'ORGANIZERS_ADD_SUCCESSFULLY'=>'Organizers Added Successfully',
    'ORGANIZERS_UPDATED_SUCCESSFULLY'=>'Organizers Updated Successfully',
    'ORGANIZERS_DELETED_SUCCESSFULLY'=>'Organizers Deleted Successfully',
    'ORGANIZER_CHANGE_STATUS_SUCCESSFULLY'=>'Organizer Status Change Sucessfully',
    'SITE_URL'=>'Website Url',
    'INDUSTRY'=>'Industry',
  /* Orzanizors Management   end */

  /*  Event Category   start */
    'ADD_EVENT_CATEGORY'=>'Add Event Category',
    'EDIT_EVENT_CATEGORY'=>'Edit Event Category',
    'ALL_EVENT_CATEGORIES'=>'All Event Category',
    'EVENT_CATEGORY_ADD_SUCCESSFULLY'=>'Event category added successfully',
    'EVENT_CATEGORY_DELETE_SUCCESSFULLY'=>'Event category delete successfully',
    'EVENT_CATEGORY_UPDATE_SUCCESSFULLY'=>'Event category updated successfully',

/*Events Start*/
    
    'ADD_EVENT'=>'Add Event',
    'EDIT_EVENT'=>'Edit Event',
    'SHOW_EVENT'=>'Event',
    'ALL_EVENT'=>'All Events',
    'EVENT_ADD_SUCCESSFULLY'=>'Event added successfully',
    'EVENT_DELETE_SUCCESSFULLY'=>'Event delete successfully',
    'EVENT_UPDATE_SUCCESSFULLY'=>'Event updated successfully',
    'NUMBER_OF_GATHER'=>'Number Of Gathering',
    

/* Contacts Start*/
    'CONTACT_NUMBER'=>'Contact Number',
    'ENQUIRY_TYPE'=>'Enquiry Type',
    'CONTACT_DELETE_SUCCESSFULLY'=>'Contact delete successfully',
    'All_CONTACTS'=>'CONTACTS',



/*  Tags Start*/
    'ALL_TAGS'=>'TAGS',
    'ADD_TAGS'=>'Add Tags',
    'TAGS_ADD_SUCCESSFULLY'=>'Tags Add Successfully',
    'TAGS_UPDATED_SUCCESSFULLY'=>'Tags Updated Successfully',
    'TAGS_DELETED_SUCCESSFULLY'=>'Tags Deleted Successfully',
    'EDIT_TAGS'=>'Edit Tag',
    'POPULAR_TAG'=>'Popular',

/*   Comment Start*/
    'ADD_COMMENT'=>'Add Comment',
    'ALL_COMMENT'=>'All Comments',
    'APPROVED' => 'Approved',
    'DISABLED' => 'Disabled',
    'UNAUTHORISED' => 'Disapproved',
    'BLOGCOMMENT_CHANGE_APPROVEMENT_SUCCESSFULLY' => 'Blogcomment Change Approvement Successfully',
    'BLOGCOMMENT_CHANGE_STATUS_SUCCESSFULLY' => 'Blogcomment Change Status Successfully',
    'EDIT_BLOGCOMMENT' => 'Edit Blogcomment',
    'BLOGCOMMENT_UPDATE_SUCCESSFULLY' => 'Blogcomment Update Successfully',
    'BLOGCOMMENT_ADD_SUCCESSFULLY' => 'Blog Comment Add Successfully',
    'BLOG_COMMENT_DELETE_SUCCESSFULLY'=>'Blog Comment Delete Successfully',
    'BLOG_SUBCOMMENT_DELETE_SUCCESSFULLY'=>'Blog SubComment Delete Successfully',
    'FAQ_CATEGORY_CHANGE_STATUS_SUCCESSFULLY'=>'Faq Category Change Status Successfully',
    'TAG_CHANGE_STATUS_SUCCESSFULLY'=>'Tag Change Status Sucessfully',


    /*  Blog Management   start */
    'TEAM'=>'Team',
    'ADD_TEAM'=>'Add Team',
    'DESIGNATION'=>'Designation',
    'TEAM_ADD_SUCCESSFULLY'=>'Team member Added Successfully.',
    'TEAM_CHANGE_STATUS_SUCCESSFULLY'=>'Status changed Successfully.',
    'EDIT_TEAM'=>'Edit TEAM',
    
    
    
    'Blog' => 'Blog Pages',
    'IMAGE'=>'Image',
    'CATEGORY_ID'=>'Category',
    'BLOG_CATEGORY_ID'=>'Blogs',
    'TAG_ID'=>'Tags',
    'BLOG_UPDATE_SUCCESSFULLY'=>'Blog Updated Successfully.',
    
    'BLOG_DELETE_SUCCESSFULLY'=>'Blog Deleted Successfully.',

    /*  Blog Management  end */

    /* Speaker */
    'KEYSPEAKER'=>'Key Speaker',
    'SUMMARY'=>'Summary',
    'COMPANY'=>'Company',

     /* Exhibitors */
    'PRODUCTS'=>'Products',
    'Contact_Name'=>'Name of Contact Person',
    'Contact_Email'=>'Email ID',
    'Contact_Phone'=>'Contact Number',

     /* Sponsers */
    'SPONSER_NAME'=>'Sponser Name',
    'Contact_Person'=>'Contact Person(optional)',
    'Contact_Email'=>'Contact Email(optional)',
    'Contact_Phone'=>'Contact Phone(optional)',
    'SPONSERSHIP_LEVEL'=>'Select Sponsership Level',
    'LOGO'=>'Logo',

    /* Ticketing */
    'COLOR' => 'Color',
    'CURRENCY'=>'Currency',
    'AMOUNT'=>'Amount',
    'STOCK'=>'Stock',
    'EVENT_PASS_NAME'=>'Event Pass Name',
    'FREE'=>'Free',
    'PAID'=>'Paid',

    /* Why Us */
    'ALL_WHYUS' => 'Whyus',
    'ADD_WHYUS'=>'Add Whyus',
    'EDIT_WHYUS'=>'Edit Whyus',
    'WHYUS_CHANGE_STATUS_SUCCESSFULLY'=>'Why us Status changed Successfully.',
    'WHYUS_DELETED_SUCCESSFULLY'=>'Why us deleted Successfully.',
    'WHYUS_UPDATED_SUCCESSFULLY'=>'Why us Updated Successfully.',
    'WHYUS_ADD_SUCCESSFULLY'=>'Why us Add Successfully.',

    /* Why Us */
    'ALL_ADDRESS' => 'Address',
    'ADD_ADDRESS'=>'Add Address',
    'EDIT_ADDRESS'=>'Edit Address',
    'ADDRESS_CHANGE_STATUS_SUCCESSFULLY'=>'Address Status changed Successfully.',
    'ADDRESS_DELETED_SUCCESSFULLY'=>'Address deleted Successfully.',
    'ADDRESS_UPDATED_SUCCESSFULLY'=>'Address Updated Successfully.',
    'ADDRESS_ADD_SUCCESSFULLY'=>'Address Add Successfully.',

    /*  TESTIMONIAL   start */
    'TESTIMONIAL'=>'Testimonial',
    'ADD_TESTIMONIAL'=>'Add Testimonial',
    'TESTIMONIAL_ADD_SUCCESSFULLY'=>'Testimonial member Added Successfully.',
    'TESTIMONIAL_CHANGE_STATUS_SUCCESSFULLY'=>'Status changed Successfully.',
    'EDIT_TESTIMONIAL'=>'Edit TESTIMONIAL',
    'TESTIMONIAL_UPDATE_SUCCESSFULLY'=>'Testimonial Update Successfully',
    
    /*  FEATURES   start */
    'FEATURES'=>'Features',
    'ADD_FEATURES'=>'Add Features',
    'FEATURES_ADD_SUCCESSFULLY'=>'Features member Added Successfully.',
    'EDIT_FEATURES'=>'Edit Features',
    'FEATURES_UPDATE_SUCCESSFULLY'=>'Features Updated Successfully.',

    'LIST_TITLE'=>'Event Listing Title',
    'LIST_DESCRIPTION'=>'Event Listing Description',
    'LIST_IMAGE'=>'Event Listing Image',

    'PUBLICITY_TITLE'=>'Event Publicity Title',
    'PUBLICITY_DESCRIPTION'=>'Event Publicity Description',
    'PUBLICITY_IMAGE'=>'Event Publicity Image',

    'MANAGEMENT_TITLE'=>'Event Management Title',
    'MANAGEMENT_DESCRIPTION'=>'Event Management Description',
    'MANAGEMENT_IMAGE'=>'Event Management Image',

    'AID_TITLE'=>'Sponsor and Exhibitor Aids Title',
    'AID_DESCRIPTION'=>'Sponsor and Exhibitor Aids Description',
    'AID_IMAGE'=>'Sponsor and Exhibitor Aids Image',

    'ENGAGEMENT_TITLE'=>'Post Event Engagement Title',
    'ENGAGEMENT_DESCRIPTION'=>'Post Event Engagement Description',
    'ENGAGEMENT_IMAGE'=>'Post Event Engagement Image',

    'DELEGATES_TITLE'=>'Delegates  Title',
    'DELEGATES_DESCRIPTION'=>'Delegates  Description',
    'DELEGATES_IMAGE'=>'Delegates  Image',

    'ROLE'=>'Role',
    'ADD_ROLE'=>'Add Role',
    'ROLE_ADD_SUCCESSFULLY'=>'Role added successfully',
    'ROLE_CHANGE_STATUS_SUCCESSFULLY'=>'Role status changed successfully',
    'ROLE_UPDATE_SUCCESSFULLY'=>'Role updated successfully',
    'ROLE_DELETED_SUCCESSFULLY'=>'Role deleted successfully',
    'EDIT_ROLE'=>'Edit Role',

    'CONTACTUS'=>'Contactus',
    'ADD_CONTACTUS'=>'Add Contactus',
    'CONTACTUS_ADD_SUCCESSFULLY'=>'Contactus added successfully',
    'CONTACTUS_CHANGE_STATUS_SUCCESSFULLY'=>'Contactus status changed successfully',
    'CONTACTUS_UPDATE_SUCCESSFULLY'=>'Contactus updated successfully',
    'CONTACTUS_DELETED_SUCCESSFULLY'=>'Contactus deleted successfully',
    'EDIT_CONTACTUS'=>'Edit Contactus',

    'COUPONS'=>'Coupons',
    'ADD_COUPONS'=>'Add Coupon',
    'COUPONS_ADD_SUCCESSFULLY'=>'Coupon added successfully',
    'COUPONS_CHANGE_STATUS_SUCCESSFULLY'=>'Coupon status changed successfully',
    'COUPONS_UPDATE_SUCCESSFULLY'=>'Coupon updated successfully',
    'COUPONS_DELETED_SUCCESSFULLY'=>'Coupon deleted successfully',
    'EDIT_COUPONS'=>'Edit Coupon',

     'COUPONS'=>'Coupons',
    'ADD_COUPONS'=>'Add Coupon',
    'EDIT_COUPONS'=>'Edit Coupon',
    'COUPONS_CODE'=>'Coupon Code',
    'COUPONS_CODE_HELP'=>'Generate Randomly Click On Checkbox',
    'START_DATE'=>'Start Date',
    'END_DATE'=>'End Date',
    'DISCOUNT_TYPE'=>'Discount Type',
    'DISCOUNT'=>'Discount',
    'PERCENTAGE'=>'Percentage',
    'AMOUNT'=>'Amount',
    'REDEEM_COUNT'=>'Redeem Count',
    'COUPONS_ADD_SUCCESSFULLY'=>'Coupons added successfully.',
    'COUPONS_CHANGE_STATUS_SUCCESSFULLY'=>'Coupons status changed successfully.',
    'COUPONS_UPDATE_SUCCESSFULLY'=>'Coupons updated successfully.',

    'CASHBACK_TYPE'=>'Cashback Type',
    'MAX_CASHBACK_AMOUNT'=>'Max Cashback Amount',
    'MIN_BILL_AMOUNT'=>'Min Bill Amount',
    'COUPON_DESCRIPTION'=>'Coupon Description',
    'COUPON_CODE'=>'Coupon Code',
    'ENABLE_CASHBACK'=>'Enable Cashback',
    'DISCOUNT_ON'=>'Discount On',
    'MAX_PRODUCT_QUANTITY'=>'Max Product Quantity',
    'MAX_DISCOUNT'=>'max Discount',
    'MIN_TOTAL_VALUE'=>'Min Total Value',

    'TEMPLATECATEGORY'=>'Template Category',
    'ADD_TEMPLATECATEGORY'=>'Add Template Category',
    'TEMPLATECATEGORY_ADD_SUCCESSFULLY'=>'Template Category added successfully',
    'TEMPLATECATEGORY_CHANGE_STATUS_SUCCESSFULLY'=>'Template Category status changed successfully',
    'TEMPLATECATEGORY_UPDATE_SUCCESSFULLY'=>'Template Category updated successfully',
    'TEMPLATECATEGORY_DELETED_SUCCESSFULLY'=>'Template Category deleted successfully',
    'EDIT_TEMPLATECATEGORY'=>'Edit Template Category',

    'TEMPLATE'=>'Template',
    'ADD_TEMPLATE'=>'Add Template',
    'TEMPLATE_ADD_SUCCESSFULLY'=>'Template added successfully',
    'TEMPLATE_CHANGE_STATUS_SUCCESSFULLY'=>'Template status changed successfully',
    'TEMPLATE_UPDATE_SUCCESSFULLY'=>'Template updated successfully',
    'TEMPLATE_DELETED_SUCCESSFULLY'=>'Template deleted successfully',
    'EDIT_TEMPLATE'=>'Edit Template',

    'VOUCHERS'=>'Vouchers',
    'ADD_VOUCHERS'=>'Add Voucher',
    'VOUCHERS_ADD_SUCCESSFULLY'=>'Voucher added successfully',
    'VOUCHERS_CHANGE_STATUS_SUCCESSFULLY'=>'Voucher status changed successfully',
    'VOUCHERS_UPDATE_SUCCESSFULLY'=>'Voucher updated successfully',
    'VOUCHERS_DELETED_SUCCESSFULLY'=>'Voucher deleted successfully',
    'EDIT_VOUCHERS'=>'Edit Voucher',
    
    /*SITE CONFIG*/
    'SITE_LOGO'=>'Site Logo',
    'SITE_FOOTER_REFER_IMAGE'=>'Footer Advertisment Image',
    'SITE_ADVT_IMAGE'=>'Site Advertisment Image',
    'CONTACT_ADDRESS'=>'Contact Address',
    
    /*SLIDER IMAGE*/
    'SLIDER_IMAGES'=>'Slider Images',
    'ADD_SLIDER_IMAGE'=>'Add Slider Image',
    'EDIT_SLIDER_IMAGE'=>'Edit Slider Image',
    'SLIDER_IMAGES_ADD_SUCCESSFULLY'=>'Slider Image Added Successfully.',
    'SLIDER_IMAGES_UPDATE_SUCCESSFULLY'=>'Slider Image Updated Successfully',
    'SLIDER_IMAGES_CHANGE_STATUS_SUCCESSFULLY'=>'Status Changed Successfully.'
];

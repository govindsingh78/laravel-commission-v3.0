<?php 
return [
	'status_list' => array("1"=>"Active","0"=>"Deactive"),
	'visibility' => array("Inactive"=>"Inactive","Active"=>"Active","Not Visible"=>"Not Visible"),
	'role_id' => array("admin"=>1, "user"=>2, "organizer"=>3),
	'file_max_size' => 2048,// in kb
	'image_mime_type' => 'jpeg,gif,jpg,png',// image  extention
	'min_width' => '1500',
	'min_height' => '700',
	'max_width' => '1940',
	'max_height' => '740',
    
        'logo_min_width' => '160',
	'logo_min_height' => '50',
	'logo_max_width' => '170',
	'logo_max_height' => '60',
    
        'earn_ad_min_width' => '180',
	'earn_ad_min_height' => '180',
	'earn_ad_max_width' => '190',
	'earn_ad_max_height' => '190',
    
        'site_ad_min_width' => '1910',
	'site_ad_min_height' => '480',
	'site_ad_max_width' => '1930',
	'site_ad_max_height' => '510',
    
	'html_entity_decode_option'=>array('remove' => false,'charset' => 'UTF-8','quotes' => ENT_QUOTES,'double' => true),
	'category_type' => array("main_category"=>0,"sub_category"=>1),
	'menu_type' => array("dynamic"=>0,"cms_page"=>1),
	'menu_position' => array("1"=>'Header',"2"=>'Footer'),
	'approve_list' => array("1"=>"Yes","0"=>"No"),
	
];
<select class="selectpicker" data-live-search="true" multiple title="Select Industry...">
  <option value="Accounting">Accounting</option>
  <option value="Airlines/Aviation">Airlines/Aviation</option>
  <option value="Alternative Dispute Resolution">Alternative Dispute Resolution</option>
  <option value="Alternative Medicine">Alternative Medicine</option>
  <option value="Animation">Animation</option>
  <option value="Apparel & Fashion">Apparel & Fashion</option>
  <option value="Architecture & Planning">Architecture & Planning</option>
  <option value="Arts & Crafts">Arts & Crafts</option>
  <option value="Automotive">Automotive</option>
  <option value="Aviation & Aerospace">Aviation & Aerospace</option>
  <option value="Banking">Banking</option>
  <option value="Biotechnology">Biotechnology</option>
  <option value="Broadcast Media">Broadcast Media</option>
  <option value="Building Materials">Building Materials</option>
  <option value="Business Supplies & Equipment">Business Supplies & Equipment</option>
  <option value="Capital Markets">Capital Markets</option>
  <option value="Chemicals">Chemicals</option>
  <option value="Civic & Social Organization">Civic & Social Organization</option>
  <option value="Civil Engineering">Civil Engineering</option>
  <option value="Commercial Real Estate">Commercial Real Estate</option>
  <option value="Computer & Network Security">Computer & Network Security</option>
  <option value="Computer Games">Computer Games</option>
  <option value="Computer Hardware">Computer Hardware</option>
  <option value="Computer Networking">Computer Networking</option>
  <option value="Computer Software">Computer Software</option>
  <option value="Construction">Construction</option>
  <option value="Consumer Electronics">Consumer Electronics</option>
  <option value="Consumer Goods">Consumer Goods</option>
  <option value="Consumer Services">Consumer Services</option>
  <option value="Cosmetics">Cosmetics</option>
  <option value="Dairy">Dairy</option>
  <option value="Defense & Space">Defense & Space</option>
  <option value="Design">Design</option>
  <option value="E-learning">E-learning</option>
  <option value="Education Management">Education Management</option>
  <option value="Electrical & Electronic Manufacturing">Electrical & Electronic Manufacturing</option>
  <option value="Entertainment">Entertainment</option>
  <option value="Environmental Services">Environmental Services</option>
  <option value="Events Services">Events Services</option>
  <option value="Executive Office">Executive Office</option>
  <option value="Facilities & Services">Facilities & Services</option>
  <option value="Farming">Farming</option>
  <option value="Financial Services">Financial Services</option>
  <option value="Fine Art">Fine Art</option>
  <option value="Fishery">Fishery</option>
  <option value="Food & Beverages">Food & Beverages</option>
  <option value="Food Production">Food Production</option>
  <option value="Furniture">Furniture</option>
  <option value="Fundraising">Fundraising</option>
  <option value="Gambling & Casinos">Gambling & Casinos</option>
  <option value="Glass, Ceramics & Concrete">Glass, Ceramics & Concrete</option>
</select>

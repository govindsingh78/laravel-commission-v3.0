function selectBox(){
    $('.selectpicker').selectpicker();
}

$(document).ready(function () {
    $('.banner_list_0').addClass('active');
    $('.banr_img_0').addClass('active');
        
    $("#i_am_select").change(function () {
        $("option:selected", $(this)).each(function () {
            var i_am_val = $(this).val();
            if(i_am_val === 'Sales Agents'){
                //$('.div_commision_level').show();	
            }
            else{
                $('.div_commision_level').hide();
                $('.search-filter ul li:last-child').css('paddingTop', 0);
            }

            $.ajax({
                type: "POST",
                url: "showcat.php",
                data: "i_am_val=" + i_am_val,
                success: function (data) {
                    console.log(data);
                    $('.div_looking').html(data);
                    $('.div_looking').slideDown('slow');
                    selectBox();
                }
            });
        });
    });
});

function Getindustry(elem){
    var looking_val = $(elem).val();
    var i_am = $('#i_am_select').val();

    if(i_am === 'Sales Agents' && looking_val === 'Company'){
            $('.div_commision_level').show();
            $('.search-filter ul li:last-child').css('paddingTop', 10);
    }else{
            $('.div_commision_level').hide();
            $('.search-filter ul li:last-child').css('paddingTop', 0);
    }
}

$(document).ready(function(){
    $(".magicsearch-box").click(function () {
        var basic_selecte_val = $('.multi-item').text();
        //alert(basic_selecte_val);
        $.ajax({
            type: "POST",
            url: "showproduct.php",
            data: "industry_val=" + industry_val,
            success: function (data) {
                $('.div_product').html(data);
                $('.div_product').slideDown('slow');
                selectBox();
            }
        });
    });
});

//magicsearch
$(function() {
    var dataSource = [
        {id: 1, industryName: 'Accounting'},
        {id: 2, industryName: 'Airlines'},
        {id: 3, industryName: 'Animation'},
        {id: 4, industryName: 'Apparel & Fashion'},
        {id: 5, industryName: 'Banking'},
        {id: 6, industryName: 'Biotechnology'},
        {id: 7, industryName: 'Broadcast Media'},
        {id: 8, industryName: 'Capital Markets'},
        {id: 9, industryName: 'Chemicals'},
        {id: 10, industryName: 'Civic & Social Organization'},
        {id: 11, industryName: 'Civil Engineering'},
        {id: 12, industryName: 'Computer Software'},
        {id: 13, industryName: 'Dairy'},
        {id: 14, industryName: 'Design'},
        {id: 15, industryName: 'E-learning'}
    ];

    $('#basic').magicsearch({
        dataSource: dataSource,
        fields: ['industryName'],
        id: 'id',
        format: '%industryName%',
        multiple: true,
        multiField: 'industryName',
        dropdownBtn: true,
        noResult: 'No Keyword found',
        multiStyle: {
            space: 5,
            width: 80
        }
    });
});

//magicsearch
$(function () {
    var dataSource = [
        {id: 1, productName: 'Product 1'},
        {id: 2, productName: 'Product 2'},
        {id: 3, productName: 'Product 3'},
        {id: 4, productName: 'Product 4'},
        {id: 5, productName: 'Product 5'},
        {id: 6, productName: 'Product 6'},
        {id: 7, productName: 'Product 7'},
        {id: 8, productName: 'Product 8'},
        {id: 9, productName: 'Product 9'},
        {id: 10, productName: 'Product 10'},
    ];

    $('#productsearch').magicsearch({
        dataSource: dataSource,
        fields: ['productName'],
        id: 'id',
        format: '%productName%',
        multiple: true,
        multiField: 'productName',
        dropdownBtn: true,
        noResult: 'No Keyword found',
        multiStyle: {
            space: 5,
            width: 80
        }
    });
});

//input
$(window, document, undefined).ready(function () {
    $('input').blur(function () {
        var $this = $(this);
        if ($this.val())
            $this.addClass('used');
        else
            $this.removeClass('used');
    });

    var $ripples = $('.ripples');
    $ripples.on('click.Ripples', function (e) {
        var $this = $(this);
        var $offset = $this.parent().offset();
        var $circle = $this.find('.ripplesCircle');

        var x = e.pageX - $offset.left;
        var y = e.pageY - $offset.top;

        $circle.css({
            top: y + 'px',
            left: x + 'px'
        });

        $this.addClass('is-active');
    });

    $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function (e) {
        $(this).removeClass('is-active');
    });
});

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        navText: [
            ""
        ],
        autoplay: true,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
});

$(function () {
    $('#tabs').tabs({
        neighbors: {
            prev: $('button.prev'),
            next: $('button.next')
        }
    });
});

//nav bar
var $xBar = $(".x-bar"),
    $sideNav = $(".side-nav"),
    $menuIcon = $(".menu-icon");
$menuIcon.click(function () {
    $sideNav.css({
        left: 0
    });
    $menuIcon.fadeOut(5);
    $xBar.css({
        top: 30
    });
    if ($(window).width() < 767) {
        $sideNav.css({
            left: 0
        });
    }
    if ($(window).width() < 992) {
        $sideNav.css({
            width: 270
        });
    }
});

$xBar.click(function () {
    $sideNav.css({
        left: -307
    });
    $menuIcon.fadeIn(500);
    $xBar.css({
        top: -30
    });
    if ($(window).width() < 767) {
        $sideNav.css({
            left: -100 + "%"
        });
    }
    if ($(window).width() < 991 && $(window).width() > 1199) {
        $sideNav.css({
            left: -270
        });
    }
});

// Hide the extra content initially, using JS so that if JS is disabled, no problemo:
$('.read-more-content').addClass('hide')
$('.read-more-show, .read-more-hide').removeClass('hide')

// Set up the toggle effect:
$('.read-more-show').on('click', function (e) {
    $(this).next('.read-more-content').removeClass('hide');
    $(this).addClass('hide');
    e.preventDefault();
});

$('.read-more-hide').on('click', function (e) {
    $(this).parent('.read-more-content').addClass('hide');
    var moreid = $(this).attr("more-id");
    $('.read-more-show#' + moreid).removeClass('hide');
    e.preventDefault();
});

//material-btn
(function (window, $) {
    $(function () {
        $('.ripple').on('click', function (event) {
            event.preventDefault();

            var $div = $('<div/>'),
                btnOffset = $(this).offset(),
                xPos = event.pageX - btnOffset.left,
                yPos = event.pageY - btnOffset.top;

            $div.addClass('ripple-effect');
            var $ripple = $(".ripple-effect");

            $ripple.css("height", $(this).height());
            $ripple.css("width", $(this).height());
            $div.css({
                top: yPos - ($ripple.height() / 2),
                left: xPos - ($ripple.width() / 2),
                background: $(this).data("ripple-color")
            }).appendTo($(this));

            window.setTimeout(function () {
                $div.remove();
            }, 2000);
        });
    });
})(window, jQuery);

$(function () {
    $('#tabs').tabs({
        neighbors: {
            prev: $('button.prev'),
            next: $('button.next')
        }
    });
});

$(document).ready(function () {
    $('#Carouselblg').carousel({
        interval: 5000
    })
});

//par. logo
$(document).ready(function () {
    $('#myCarousel').carousel({
        interval: 4000
    });

    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function () {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function (e) {
        if (!clickEvent) {
            var count = $('.nav').children().length - 1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
});
﻿//*  UI Designer : Ravindra Singh Rathore
//*  Ways and Means Tech. PVT. LTD
//*  Project : NEEYAM 



// Enable tooltips everywhere

$(function () {
    $('[data-toggle="tooltip"]').tooltip()


})


// side menu toggle
$(".menu-collapse-toggle h3").click(function () {    
    $(this).next("ul").slideToggle();
});


// Post Tabs
// post box

var typeVal = '';
$('.content-write-text-input').focus(function () {
    $('.post-type-text-main-placehoder').remove();
    $('#new_post').addClass('focus');
    $('body').addClass('focus');
});
$('.content-write-text-input').blur(function () {
    checkVal()
    function checkVal() {
        typeVal = $('.content-write-text-input').text().trim();
        if (typeVal == '') {
            $('.content-write-text-input').append('\
                    <div class="post-type-text-main-placehoder"> Hi Name. Tell your friends about your day. ?</div>\
                    ');
        }
    }
});
$('.close-remove').click(function () {

    $('#new_post').removeClass('focus');
    $('body').removeClass('focus');
    $('.post-type-text-more-fields-tag').hide();

});
$('.hide-show-tag-frnds').click(function () {

    $('.post-type-text-more-fields-tag').slideToggle();

});

// Scroll animations 
wow = new WOW(
      {
          animateClass: 'animated',
          offset: 100,
          callback: function (box) {
              console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
          }
      });
wow.init();
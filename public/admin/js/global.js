function showSuccessMessageBottomRight(msg)
{
    noty({
        layout : 'bottomRight', 
        theme : 'noty_theme_default', 
        type : 'success',     
        text: msg ,     
        timeout : 10000,
        closeButton:true,
        animation : {
            easing: 'swing',
            speed: 150 // opening & closing animation speed
        }		
    });
}
function showErrorMessageBottomRight(msg){
    noty({
        layout : 'bottomRight', 
        theme : 'noty_theme_default', 
        type : 'error',     
        text: msg ,     
        timeout : 10000,
        closeButton:true,
        animation : {
            easing: 'swing',
            speed: 150 // opening & closing animation speed
        }		
    });
}


function showSuccessMessageTopCenter(msg)
{
    noty({
        layout : 'Top', 
        theme : 'noty_theme_default', 
        type : 'success',     
        text: msg ,     
        timeout : 10000,
        closeButton:true,
        animation : {
            easing: 'swing',
            speed: 150 // opening & closing animation speed
        }		
    });
}
function showErrorMessageTopCenter(msg){
    noty({
        layout : 'Top', 
        theme : 'noty_theme_default', 
        type : 'error',     
        text: msg ,     
        timeout : 10000,
        closeButton:true,
        animation : {
            easing: 'swing',
            speed: 150 // opening & closing animation speed
        }		
    });
}



function showSuccessMessageTopRight(msg){
    noty({
        layout : 'topRight', 
        theme : 'noty_theme_default', 
        type : 'success',     
        text: msg ,     
        timeout : 10000,
        closeButton:true,
        animation : {
            easing: 'swing',
            speed: 150 // opening & closing animation speed
        }		
    });
}